<?php

namespace MentorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="user_mentee")
 * @UniqueEntity(fields = "username", targetClass = "AppBundle\Entity\User", message="fos_user.username.already_used")
 * @UniqueEntity(fields = "email", targetClass = "AppBundle\Entity\User", message="fos_user.email.already_used")
 */
class UserMentee extends User
{
    protected $entityType = 'user_mentee';
    protected $redirectPath = 'list_mentee';
    
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    public $idParentUser;

    /**
     * @ORM\ManyToOne(targetEntity="MentorBundle\Entity\UserClient", inversedBy="mentees")
     * @ORM\JoinColumn(name="id_parent_user", referencedColumnName="id")
     */
    public $client;
    
    /**
     * @ORM\ManyToOne(targetEntity="MentorBundle\Entity\UserMenteeWatcher", inversedBy="mentees",cascade={"persist"}))
     * @ORM\JoinColumn(name="id_mentee_trainer", referencedColumnName="id")
     */
    public $menteeWatcher;
    
    /**
     * @ORM\ManyToOne(targetEntity="MentorBundle\Entity\Company", inversedBy="mentees")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    public $company;
    
    /**
     * @ORM\OneToMany(targetEntity="MentorBundle\Entity\MentorProcess",
     *  mappedBy="mentee",
     *  fetch="EXTRA_LAZY",
     *  orphanRemoval=true,
     *  cascade={"persist"})
     */
    protected $processes;
    
    public function __construct() {
        parent::__construct();
        $this->processes = new ArrayCollection();
    
    }
    
    public function getIdParentUser()
    {
        return $this->idParentUser;
    }
    
    public function getClient() {
        return $this->client;
    }

    public function getMenteeWatcher() {
        return $this->menteeWatcher;
    }
    
    public function getProcesses(){
        return $this->processes;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->addRole('ROLE_MENTEE');
    }
    
    public function getCompany() {
        return $this->company;
    }
    public function setCompany($company) {
        $this->company = $company;
        return $this;
    }
    
    public function setMenteeWatcher($user)
    {
        $this->menteeWatcher = $user;
        return $this;
    }
}