<?php

namespace MentorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MentorGoalCard
 *
 * @ORM\Table(name="mentor_goal_card")
 * @ORM\Entity(repositoryClass="MentorBundle\Repository\MentorGoalCardRepository")
 */
class MentorGoalCard
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_process", type="integer")
     */
    private $idProcess;

    /**
     * @var int
     *
     * @ORM\Column(name="accepted_mentee", type="integer")
     */
    private $acceptedMentee = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="accepted_mentor", type="integer")
     */
    private $acceptedMentor = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="first_goal", type="text", nullable=true)
     */
    private $firstGoal;

    /**
     * @var string
     *
     * @ORM\Column(name="first_kpi", type="text", nullable=true)
     */
    private $firstKpi;

    /**
     * @var string
     *
     * @ORM\Column(name="second_goal", type="text", nullable=true)
     */
    private $secondGoal;

    /**
     * @var string
     *
     * @ORM\Column(name="second_kpi", type="text", nullable=true)
     */
    private $secondKpi;

    /**
     * @var string
     *
     * @ORM\Column(name="third_goal", type="text", nullable=true)
     */
    private $thirdGoal;

    /**
     * @var string
     *
     * @ORM\Column(name="third_kpi", type="text", nullable=true)
     */
    private $thirdKpi;

    /**
     * @ORM\ManyToOne(targetEntity="MentorBundle\Entity\MentorProcess", inversedBy="goalCards")
     * @ORM\JoinColumn(name="id_process", referencedColumnName="id")
     */
    public $process;

    /**
     * @return the $process
     */
    public function getProcess() {
        return $this->process;
    }

	/**
     * @param field_type $process
     */
    public function setProcess($process) {
        $this->process = $process;
    }

	/**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idProcess
     *
     * @param integer $idProcess
     *
     * @return MentorGoalCard
     */
    public function setIdProcess($idProcess)
    {
        $this->idProcess = $idProcess;

        return $this;
    }

    /**
     * Get idProcess
     *
     * @return int
     */
    public function getIdProcess()
    {
        return $this->idProcess;
    }

    /**
     * Set acceptedMentee
     *
     * @param integer $acceptedMentee
     *
     * @return MentorGoalCard
     */
    public function setAcceptedMentee($acceptedMentee)
    {
        $this->acceptedMentee = $acceptedMentee;

        return $this;
    }

    /**
     * Get acceptedMentee
     *
     * @return int
     */
    public function getAcceptedMentee()
    {
        return $this->acceptedMentee;
    }

    /**
     * Set acceptedMentor
     *
     * @param integer $acceptedMentor
     *
     * @return MentorGoalCard
     */
    public function setAcceptedMentor($acceptedMentor)
    {
        $this->acceptedMentor = $acceptedMentor;

        return $this;
    }

    /**
     * Get acceptedMentor
     *
     * @return int
     */
    public function getAcceptedMentor()
    {
        return $this->acceptedMentor;
    }

    /**
     * Set firstGoal
     *
     * @param string $firstGoal
     *
     * @return MentorGoalCard
     */
    public function setFirstGoal($firstGoal)
    {
        $this->firstGoal = $firstGoal;

        return $this;
    }

    /**
     * Get firstGoal
     *
     * @return string
     */
    public function getFirstGoal()
    {
        return $this->firstGoal;
    }

    /**
     * Set firstKpi
     *
     * @param string $firstKpi
     *
     * @return MentorGoalCard
     */
    public function setFirstKpi($firstKpi)
    {
        $this->firstKpi = $firstKpi;

        return $this;
    }

    /**
     * Get firstKpi
     *
     * @return string
     */
    public function getFirstKpi()
    {
        return $this->firstKpi;
    }

    /**
     * Set secondGoal
     *
     * @param string $secondGoal
     *
     * @return MentorGoalCard
     */
    public function setSecondGoal($secondGoal)
    {
        $this->secondGoal = $secondGoal;

        return $this;
    }

    /**
     * Get secondGoal
     *
     * @return string
     */
    public function getSecondGoal()
    {
        return $this->secondGoal;
    }

    /**
     * Set secondKpi
     *
     * @param string $secondKpi
     *
     * @return MentorGoalCard
     */
    public function setSecondKpi($secondKpi)
    {
        $this->secondKpi = $secondKpi;

        return $this;
    }

    /**
     * Get secondKpi
     *
     * @return string
     */
    public function getSecondKpi()
    {
        return $this->secondKpi;
    }

    /**
     * Set thirdGoal
     *
     * @param string $thirdGoal
     *
     * @return MentorGoalCard
     */
    public function setThirdGoal($thirdGoal)
    {
        $this->thirdGoal = $thirdGoal;

        return $this;
    }

    /**
     * Get thirdGoal
     *
     * @return string
     */
    public function getThirdGoal()
    {
        return $this->thirdGoal;
    }

    /**
     * Set thirdKpi
     *
     * @param string $thirdKpi
     *
     * @return MentorGoalCard
     */
    public function setThirdKpi($thirdKpi)
    {
        $this->thirdKpi = $thirdKpi;

        return $this;
    }

    /**
     * Get thirdKpi
     *
     * @return string
     */
    public function getThirdKpi()
    {
        return $this->thirdKpi;
    }
}

