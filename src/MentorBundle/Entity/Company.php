<?php

namespace MentorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="MentorBundle\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="MentorBundle\Entity\UserClient", mappedBy="company")
     */
    protected $clients;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="MentorBundle\Entity\UserMenteeWatcher", mappedBy="company")
     */
    protected $menteeWatchers;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="MentorBundle\Entity\UserMentee", mappedBy="company")
     */
    protected $mentees;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=255, unique=true)
     */
    public $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="nip", type="string", length=20, nullable=true)
     */
    public $nip;

    /**
     * @var string
     *
     * @ORM\Column(name="address_postal_code", type="string", length=10, nullable=true)
     */
    public $addressPostalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="address_city", type="string", length=255, nullable=true)
     */
    public $addressCity;

    /**
     * @var string
     *
     * @ORM\Column(name="address_street", type="string", length=255, nullable=true)
     */
    public $addressStreet;

    /**
     * @var string
     *
     * @ORM\Column(name="address_building", type="string", length=15, nullable=true)
     */
    public $addressBuilding;

    /**
     * @var string
     *
     * @ORM\Column(name="address_local", type="string", length=5, nullable=true)
     */
    public $addressLocal;

    /**
     * @var string
     *
     * @ORM\Column(name="correspondence_postal_code", type="string", length=10, nullable=true)
     */
    public $correspondencePostalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="correspondence_city", type="string", length=255, nullable=true)
     */
    public $correspondenceCity;

    /**
     * @var string
     *
     * @ORM\Column(name="correspondence_street", type="string", length=255, nullable=true)
     */
    public $correspondenceStreet;

    /**
     * @var string
     *
     * @ORM\Column(name="correspondence_building", type="string", length=15, nullable=true)
     */
    public $correspondenceBuilding;

    /**
     * @var string
     *
     * @ORM\Column(name="correspondence_local", type="string", length=5, nullable=true)
     */
    public $correspondenceLocal;


    /**
     * @ORM\Column(name="enabled", type="boolean", nullable=false, options={"default":0})
     * @var boolean
     */
    public $enabled = 1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rules", type="text", nullable=true)
     */
    public $rules;

	public function __construct() {
        $this->clients = new ArrayCollection();
        $this->menteeWatchers = new ArrayCollection();
        $this->mentees = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $companyName
     *
     * @return Company
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * @return the $enabled
     */
    public function getEnabled() {
        return $this->enabled;
    }
    
    /**
     * @return the $rules
     */
    public function getRules() {
        return $this->rules;
    }
    
    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled) {
        $this->enabled = $enabled;
    }
    
    /**
     * @param string $rules
     */
    public function setRules($rules) {
        $this->rules = $rules;
    }
    
    /**
     * Get name
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set nip
     *
     * @param string $nip
     *
     * @return Company
     */
    public function setNip($nip)
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Get nip
     *
     * @return string
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->getAddressStreet() . ' ' . $this->getAddressBuilding() . ' ' . $this->getAddressLocal() . ' ' .
        $this->getAddressPostalCode() . ' ' . $this->getAddressCity();
    }

    /**
     * Get correspondenceAddress
     *
     * @return string
     */
    public function getCorrespondenceAddress()
    {
        return $this->getCorrespondenceStreet() . ' ' . $this->getCorrespondenceBuilding() . ' ' . $this->getCorrespondenceLocal() . ' ' .
        $this->getCorrespondencePostalCode() . ' ' . $this->getCorrespondenceCity();
    }
    
    /**
     * @return Collection
     */
    public function getClients() {
        return $this->clients;
    }
    
    /**
     * @return Collection
     */
    public function getMenteeWatchers() {
        return $this->menteeWatchers;
    }
    
    /**
     * @return \MentorBundle\Entity\Collection
     */
    public function getMentees() {
        return $this->mentees;
    }
    public function setAddressPostalCode($postalCode) {
        $this->addressPostalCode = $postalCode;
        return $this;
    }
    public function setAddressCity($city) {
        $this->addressCity = $city;
        return $this;
    }
    public function setAddressStreet($street) {
        $this->addressStreet = $street;
        return $this;
    }
    public function setAddressBuilding($building) {
        $this->addressBuilding = $building;
        return $this;
    }
    public function setAddressLocal($local) {
        $this->addressLocal = $local;
        return $this;
    }
    public function getAddressPostalCode() {
            return $this->addressPostalCode;
    }
    public function getAddressCity() {
        return $this->addressCity;
    }
    public function getAddressStreet() {
        return $this->addressStreet;
    }
    public function getAddressBuilding() {
        return $this->addressBuilding;
    }
    public function getAddressLocal() {
        return $this->addressLocal;
    }
    public function setCorrespondencePostalCode($postalCode) {
        $this->correspondencePostalCode = $postalCode;
        return $this;
    }
    public function setCorrespondenceCity($city) {
        $this->correspondenceCity = $city;
        return $this;
    }
    public function setCorrespondenceStreet($street) {
        $this->correspondenceStreet = $street;
        return $this;
    }
    public function setCorrespondenceBuilding($building) {
        $this->correspondenceBuilding = $building;
        return $this;
    }
    public function setCorrespondenceLocal($local) {
        $this->correspondenceLocal = $local;
        return $this;
    }
    public function getCorrespondencePostalCode() {
            return $this->correspondencePostalCode;
    }
    public function getCorrespondenceCity() {
        return $this->correspondenceCity;
    }
    public function getCorrespondenceStreet() {
        return $this->correspondenceStreet;
    }
    public function getCorrespondenceBuilding() {
        return $this->correspondenceBuilding;
    }
    public function getCorrespondenceLocal() {
        return $this->correspondenceLocal;
    }
}

