<?php

namespace MentorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MentorProcessSummary
 *
 * @ORM\Table(name="mentor_process_summary")
 * @ORM\Entity(repositoryClass="MentorBundle\Repository\MentorProcessSummaryRepository")
 */
class MentorProcessSummary
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="summary", type="text", nullable=true)
     */
    private $summary;

    /**
     * @var string
     *
     * @ORM\Column(name="informations", type="text", nullable=true)
     */
    private $informations;

    /**
     * @var string
     *
     * @ORM\Column(name="rating_first_goal_summary", type="text", nullable=true)
     */
    private $ratingFirstGoalSummary;

    /**
     * @var string
     *
     * @ORM\Column(name="rating_first_goal_comment", type="text", nullable=true)
     */
    private $ratingFirstGoalComment;

    /**
     * @var int
     *
     * @ORM\Column(name="rating_first_goal_rate", type="integer", nullable=true)
     */
    private $ratingFirstGoalRate;

    /**
     * @var string
     *
     * @ORM\Column(name="rating_second_goal_summary", type="text", nullable=true)
     */
    private $ratingSecondGoalSummary;

    /**
     * @var string
     *
     * @ORM\Column(name="rating_second_goal_comment", type="text", nullable=true)
     */
    private $ratingSecondGoalComment;

    /**
     * @var string
     *
     * @ORM\Column(name="rating_second_goal_rate", type="integer", nullable=true)
     */
    private $ratingSecondGoalRate;

    /**
     * @var string
     *
     * @ORM\Column(name="rating_third_goal_summary", type="text", nullable=true)
     */
    private $ratingThirdGoalSummary;

    /**
     * @var string
     *
     * @ORM\Column(name="rating_third_goal_comment", type="text", nullable=true)
     */
    private $ratingThirdGoalComment;

    /**
     * @var int
     *
     * @ORM\Column(name="rating_third_goal_rate", type="integer", nullable=true)
     */
    private $ratingThirdGoalRate;

    /**
     * @var string
     *
     * @ORM\Column(name="recommendations", type="text", nullable=true)
     */
    private $recommendations;

    /**
     * @var string
     *
     * @ORM\Column(name="information_for_superior", type="text", nullable=true)
     */
    private $informationForSuperior;

    /**
     * @var string
     *
     * @ORM\Column(name="additional_informations", type="text", nullable=true)
     */
    private $additionalInformations;

    /**
     * @ORM\ManyToOne(targetEntity="MentorBundle\Entity\MentorProcess", inversedBy="summary",cascade={"persist"})
     * @ORM\JoinColumn(name="id_process", referencedColumnName="id")
     */
    public $process;
    
    public function getProcess() 
    {
        return $this->process;
    }
    
    public function setProcess($process) 
    {
        $this->process = $process;
        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set summary
     *
     * @param string $summary
     *
     * @return MentorProcessSummary
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set informations
     *
     * @param string $informations
     *
     * @return MentorProcessSummary
     */
    public function setInformations($informations)
    {
        $this->informations = $informations;

        return $this;
    }

    /**
     * Get informations
     *
     * @return string
     */
    public function getInformations()
    {
        return $this->informations;
    }

    /**
     * Set ratingFirstGoalSummary
     *
     * @param string $ratingFirstGoalSummary
     *
     * @return MentorProcessSummary
     */
    public function setRatingFirstGoalSummary($ratingFirstGoalSummary)
    {
        $this->ratingFirstGoalSummary = $ratingFirstGoalSummary;

        return $this;
    }

    /**
     * Get ratingFirstGoalSummary
     *
     * @return string
     */
    public function getRatingFirstGoalSummary()
    {
        return $this->ratingFirstGoalSummary;
    }

    /**
     * Set ratingFirstGoalComment
     *
     * @param string $ratingFirstGoalComment
     *
     * @return MentorProcessSummary
     */
    public function setRatingFirstGoalComment($ratingFirstGoalComment)
    {
        $this->ratingFirstGoalComment = $ratingFirstGoalComment;

        return $this;
    }

    /**
     * Get ratingFirstGoalComment
     *
     * @return string
     */
    public function getRatingFirstGoalComment()
    {
        return $this->ratingFirstGoalComment;
    }

    /**
     * Set ratingFirstGoalRate
     *
     * @param integer $ratingFirstGoalRate
     *
     * @return MentorProcessSummary
     */
    public function setRatingFirstGoalRate($ratingFirstGoalRate)
    {
        $this->ratingFirstGoalRate = $ratingFirstGoalRate;

        return $this;
    }

    /**
     * Get ratingFirstGoalRate
     *
     * @return int
     */
    public function getRatingFirstGoalRate()
    {
        return $this->ratingFirstGoalRate;
    }

    /**
     * Set ratingSecondGoalSummary
     *
     * @param string $ratingSecondGoalSummary
     *
     * @return MentorProcessSummary
     */
    public function setRatingSecondGoalSummary($ratingSecondGoalSummary)
    {
        $this->ratingSecondGoalSummary = $ratingSecondGoalSummary;

        return $this;
    }

    /**
     * Get ratingSecondGoalSummary
     *
     * @return string
     */
    public function getRatingSecondGoalSummary()
    {
        return $this->ratingSecondGoalSummary;
    }

    /**
     * Set ratingSecondGoalComment
     *
     * @param string $ratingSecondGoalComment
     *
     * @return MentorProcessSummary
     */
    public function setRatingSecondGoalComment($ratingSecondGoalComment)
    {
        $this->ratingSecondGoalComment = $ratingSecondGoalComment;

        return $this;
    }

    /**
     * Get ratingSecondGoalComment
     *
     * @return string
     */
    public function getRatingSecondGoalComment()
    {
        return $this->ratingSecondGoalComment;
    }

    /**
     * Set ratingSecondGoalRate
     *
     * @param string $ratingSecondGoalRate
     *
     * @return MentorProcessSummary
     */
    public function setRatingSecondGoalRate($ratingSecondGoalRate)
    {
        $this->ratingSecondGoalRate = $ratingSecondGoalRate;

        return $this;
    }

    /**
     * Get ratingSecondGoalRate
     *
     * @return string
     */
    public function getRatingSecondGoalRate()
    {
        return $this->ratingSecondGoalRate;
    }

    /**
     * Set ratingThirdGoalSummary
     *
     * @param string $ratingThirdGoalSummary
     *
     * @return MentorProcessSummary
     */
    public function setRatingThirdGoalSummary($ratingThirdGoalSummary)
    {
        $this->ratingThirdGoalSummary = $ratingThirdGoalSummary;

        return $this;
    }

    /**
     * Get ratingThirdGoalSummary
     *
     * @return string
     */
    public function getRatingThirdGoalSummary()
    {
        return $this->ratingThirdGoalSummary;
    }

    /**
     * Set ratingThirdGoalComment
     *
     * @param string $ratingThirdGoalComment
     *
     * @return MentorProcessSummary
     */
    public function setRatingThirdGoalComment($ratingThirdGoalComment)
    {
        $this->ratingThirdGoalComment = $ratingThirdGoalComment;

        return $this;
    }

    /**
     * Get ratingThirdGoalComment
     *
     * @return string
     */
    public function getRatingThirdGoalComment()
    {
        return $this->ratingThirdGoalComment;
    }

    /**
     * Set ratingThirdGoalRate
     *
     * @param integer $ratingThirdGoalRate
     *
     * @return MentorProcessSummary
     */
    public function setRatingThirdGoalRate($ratingThirdGoalRate)
    {
        $this->ratingThirdGoalRate = $ratingThirdGoalRate;

        return $this;
    }

    /**
     * Get ratingThirdGoalRate
     *
     * @return int
     */
    public function getRatingThirdGoalRate()
    {
        return $this->ratingThirdGoalRate;
    }

    /**
     * Set recommendations
     *
     * @param string $recommendations
     *
     * @return MentorProcessSummary
     */
    public function setRecommendations($recommendations)
    {
        $this->recommendations = $recommendations;

        return $this;
    }

    /**
     * Get recommendations
     *
     * @return string
     */
    public function getRecommendations()
    {
        return $this->recommendations;
    }

    /**
     * Set informationForSuperior
     *
     * @param string $informationForSuperior
     *
     * @return MentorProcessSummary
     */
    public function setInformationForSuperior($informationForSuperior)
    {
        $this->informationForSuperior = $informationForSuperior;

        return $this;
    }

    /**
     * Get informationForSuperior
     *
     * @return string
     */
    public function getInformationForSuperior()
    {
        return $this->informationForSuperior;
    }

    /**
     * Set additionalInformations
     *
     * @param string $additionalInformations
     *
     * @return MentorProcessSummary
     */
    public function setAdditionalInformations($additionalInformations)
    {
        $this->additionalInformations = $additionalInformations;

        return $this;
    }

    /**
     * Get additionalInformations
     *
     * @return string
     */
    public function getAdditionalInformations()
    {
        return $this->additionalInformations;
    }
}

