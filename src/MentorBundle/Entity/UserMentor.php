<?php

namespace MentorBundle\Entity;

use Symfony\Component\Process\Process;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\User;
use MentorBundle\Entity\BusinessHours;
use MentorBundle\Entity\MentorProcess;
use MentorBundle\Entity\Industry;
use MentorBundle\Entity\SpecializationArea;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="user_mentor")
 * @UniqueEntity(fields = "username", targetClass = "AppBundle\Entity\User", message="fos_user.username.already_used")
 * @UniqueEntity(fields = "email", targetClass = "AppBundle\Entity\User", message="fos_user.email.already_used")
 */
class UserMentor extends User
{
    protected $entityType = 'user_mentor';
    protected $redirectPath = 'list_mentor';
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\OneToMany(targetEntity="MentorBundle\Entity\BusinessHours",
     *  mappedBy="mentor",
     *  fetch="EXTRA_LAZY",
     *  orphanRemoval=true,
     *  cascade={"persist"})
     */
    public $business_hours;

    /**
     * @ORM\ManyToMany(targetEntity="MentorBundle\Entity\Industry",
      *  inversedBy="mentor",
      *  fetch="EXTRA_LAZY",
      *  orphanRemoval=false,
      *  cascade={"persist"})
     * @ORM\JoinTable(name="mentor_industries",
     *     inverseJoinColumns={@ORM\JoinColumn(name="industry_id", referencedColumnName="id")},
     *     joinColumns={@ORM\JoinColumn(name="user_mentor_id", referencedColumnName="id")}
     * )
     */
    public $industries;

    /**
     * @ORM\ManyToMany(targetEntity="MentorBundle\Entity\SpecializationArea",
      *  inversedBy="mentor",
      *  fetch="EXTRA_LAZY",
      *  orphanRemoval=false,
      *  cascade={"persist"})
     * @ORM\JoinTable(name="mentor_specializations",
     *     inverseJoinColumns={@ORM\JoinColumn(name="specialization_id", referencedColumnName="id")},
     *     joinColumns={@ORM\JoinColumn(name="user_mentor_id", referencedColumnName="id")}
     * )
     */
    public $specializations;

    /**
    * @ORM\Column(type="integer", nullable=true)
     * @Assert\Regex(
     *     pattern="/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     match=true,
     *     message="Please insert only numbers"
     * )
    */
    protected $trenerExperience;
    /**
    * @ORM\Column(type="text", nullable=true)
    */
    protected $about;
    
    /**
    * @ORM\Column(type="text", nullable=true)
    */
    protected $professionalProfile;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */    
    protected $education;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $credentials;
    
    /**
     * @ORM\OneToMany(targetEntity="MentorBundle\Entity\MentorProcess",
     *  mappedBy="mentor",
     *  fetch="EXTRA_LAZY",
     *  orphanRemoval=true,
     *  cascade={"persist"})
     */
    protected $processes;
    
    public function __construct() {
        parent::__construct();
        $this->processes = new ArrayCollection();
        $this->business_hours = new ArrayCollection();
        $this->industries = new ArrayCollection();
        $this->specializations = new ArrayCollection();
    }

	/**
     * @return the $trenerExperience
     */
    public function getTrenerExperience() {
        return $this->trenerExperience;
    }

	/**
     * @return the $about
     */
    public function getAbout() {
        return $this->about;
    }

	/**
     * @return the $professionalProfile
     */
    public function getProfessionalProfile() {
        return $this->professionalProfile;
    }

	/**
     * @return the $education
     */
    public function getEducation() {
        return $this->education;
    }

	/**
     * @return the $credentials
     */
    public function getCredentials() {
        return $this->credentials;
    }

	/**
     * @param field_type $trenerExperience
     */
    public function setTrenerExperience($trenerExperience) {
        $this->trenerExperience = $trenerExperience;
    }

	/**
     * @param field_type $about
     */
    public function setAbout($about) {
        $this->about = $about;
    }

	/**
     * @param field_type $professionalProfile
     */
    public function setProfessionalProfile($professionalProfile) {
        $this->professionalProfile = $professionalProfile;
    }

	/**
     * @param field_type $education
     */
    public function setEducation($education) {
        $this->education = $education;
    }

	/**
     * @param field_type $credentials
     */
    public function setCredentials($credentials) {
        $this->credentials = $credentials;
    }

	/**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->addRole('ROLE_MENTOR');
    }
    
    public function addBusinessHour(BusinessHours $hour)
    {
        if ($this->business_hours->contains($hour)) {
            return;
        }
        $this->business_hours[] = $hour;
        $hour->setMentor($this);
    }
    
    public function removeBusinessHour(BusinessHours $hour)
    {
        if (!$this->business_hours->contains($hour)) {
            return;
        }
        $this->business_hours->removeElement($hour);
        $hour->setMentor(null);
    }

    public function addProcess(MentorProcess $process)
    {
        if ($this->processes->contains($process)) {
            return;
        }
        $this->processes[] = $process;
        $process->setMentor($this);
    }
    
    public function removeProcess(MentorProcess $process)
    {
        if (!$this->processes->contains($process)) {
            return;
        }
        $this->processes->removeElement($process);
        $process->setMentor(null);
    }

    public function addIndustry(Industry $industry = null)
    {
        if ($industry == null) {
            $this->industries = new ArrayCollection();
            return;
        }
        if ($this->industries->contains($industry)) {
            return;
        }
        $this->industries[] = $industry;
    }

    public function removeIndustry(Industry $industry)
    {
        if (!$this->industries->contains($industry)) {
            return;
        }
        $this->industries->removeElement($industry);
    }

    public function addSpecialization(SpecializationArea $area = null)
    {
        if ($area == null) {
            $this->specializations = new ArrayCollection();
            return;
        }
        if ($this->specializations->contains($area)) {
            return;
        }
        $this->specializations[] = $area;
    }

    public function removeSpecialization(SpecializationArea $area)
    {
        if (!$this->specializations->contains($area)) {
            return;
        }
        $this->specializations->removeElement($area);
    }
}