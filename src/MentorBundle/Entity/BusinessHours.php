<?php

namespace MentorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hours
 *
 * @ORM\Table(name="business_hours")
 * @ORM\Entity(repositoryClass="MentorBundle\Repository\BusinessHoursRepository")
 */
class BusinessHours
{   
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="MentorBundle\Entity\UserMentor", inversedBy="business_hours")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $mentor;

    /**
     * @var string
     *
     * @ORM\Column(name="open", type="datetime", length=5)
     */
    private $open;

    /**
     * @var string
     *
     * @ORM\Column(name="end", type="datetime", length=5)
     */
    private $end;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set open
     *
     * @param string $open
     *
     * @return BusinessHours
     */
    public function setOpen($open)
    {
        $this->open = $open;

        return $this;
    }

    /**
     * @return the $mentor
     */
    public function getMentor()
    {
        return $this->mentor;
    }

	/**
     * @param field_type $mentor
     * 
     * @return BusinessHours
     */
    public function setMentor($mentor)
    {
        $this->mentor = $mentor;
        
        return $this;
    }

	/**
     * Get open
     *
     * @return string
     */
    public function getOpen()
    {
        return $this->open;
    }

    /**
     * Set end
     *
     * @param string $end
     *
     * @return BusinessHours
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return string
     */
    public function getEnd()
    {
        return $this->end;
    }
}

