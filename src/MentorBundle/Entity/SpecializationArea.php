<?php

namespace MentorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SpecializationArea
 *
 * @ORM\Table(name="specialization_area")
 * @ORM\Entity(repositoryClass="MentorBundle\Repository\SpecializationAreaRepository")
 */
class SpecializationArea
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    public $name;

    /**
     * @ORM\ManyToMany(targetEntity="MentorBundle\Entity\UserMentor", mappedBy="specializations")
     */
    public $mentor;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SpecializationArea
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString() {
      return (string) $this->getName();
    }
}

