<?php

namespace MentorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\User;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="user_mentee_watcher")
 * @ORM\Entity(repositoryClass="MentorBundle\Repository\MentorWatcherRepository")
 * @UniqueEntity(fields = "username", targetClass = "AppBundle\Entity\User", message="fos_user.username.already_used")
 * @UniqueEntity(fields = "email", targetClass = "AppBundle\Entity\User", message="fos_user.email.already_used")
 */
class UserMenteeWatcher extends User
{
    protected $entityType = 'user_mentee_watcher';
    protected $redirectPath = 'list_mentee_watchers';
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\OneToMany(targetEntity="MentorBundle\Entity\UserMentee", mappedBy="menteeWatcher")
     */
    public $mentees;

    /**
     * @ORM\ManyToOne(targetEntity="MentorBundle\Entity\Company", inversedBy="menteeWatchers")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    public $company;
    
    public function __construct() {
        parent::__construct();
        $this->mentees = new ArrayCollection();
    }
    
    public function getCompany()
    {
        return $this->company;
    }
    
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->addRole('ROLE_MENTEE_WATCHER');
    }
}