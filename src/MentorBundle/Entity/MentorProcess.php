<?php

namespace MentorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * MentorProcess
 *
 * @ORM\Table(name="mentor_process")
 * @ORM\Entity(repositoryClass="MentorBundle\Repository\MentorProcessRepository")
 */
class MentorProcess
{
    const STATUS_INACTIVE = 0;
    const STATUS_UNREALIZED = 1;
    const STATUS_REALIZED = 2;
    const STATUS_CLOSED = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_mentee", type="integer")
     */
    private $idMentee;

    /**
     * @var int
     *
     * @ORM\Column(name="id_mentor", type="integer")
     */
    private $idMentor;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $endDate;

    /**
     * @var int
     *
     * @ORM\Column(name="sessions", type="integer", options={"default":0})
     */
    private $sessions = 0;

    /**
     * @ORM\Column(name="sessions_planned", type="integer", options={"default":0});
     * @var int
     */
    private $sessionsPlanned = 0;
    
    /**
     * @ORM\Column(name="sessions_realized", type="integer", options={"default":0});
     * @var int
     */
    private $sessionsRealized = 0;
    
    /**
     * @var int
     *
     * @ORM\Column(name="blocked_for_client", type="integer")
     */
    private $blockedForClient;

    /**
     * @var text
     * @ORM\Column(name="close_reason", type="text", nullable=true)
     */
    private $closeReason;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="closedProcesses",cascade={"persist"})
     * @ORM\JoinColumn(name="id_closing_user", referencedColumnName="id")
     */
    private $closingUser;

    /**
     * @ORM\OneToMany(targetEntity="MentorBundle\Entity\MentorSession", mappedBy="process",cascade={"persist"})
     */
    public $sessionsList;
    
    /**
     * @ORM\OneToMany(targetEntity="MentorBundle\Entity\MentorGoalCard", mappedBy="process",cascade={"persist"})
     */
    public $goalCards;
    

    /**
     * @ORM\OneToMany(targetEntity="MentorBundle\Entity\MentorProcessSummary", mappedBy="process",cascade={"persist"})
     */
    public $summary;
    
    /**
     * @ORM\ManyToOne(targetEntity="MentorBundle\Entity\UserMentor", inversedBy="processes")
     * @ORM\JoinColumn(name="id_mentor", referencedColumnName="id")
     */
    public $mentor;
    /**
     * @ORM\ManyToOne(targetEntity="MentorBundle\Entity\UserMentee", inversedBy="processes")
     * @ORM\JoinColumn(name="id_mentee", referencedColumnName="id")
     */
    public $mentee;
    
    public static $statuses = [
        null => 'process.status.all',
        self::STATUS_INACTIVE => 'process.status.inactive',
        self::STATUS_UNREALIZED => 'process.status.unrealized',
        self::STATUS_REALIZED => 'process.status.realized',
        self::STATUS_CLOSED => 'process.status.closed',
    ];
    
    public $statusesText = [
        null => 'process.status.all',
        self::STATUS_INACTIVE => 'process.status.inactive',
        self::STATUS_UNREALIZED => 'process.status.unrealized',
        self::STATUS_REALIZED => 'process.status.realized',
        self::STATUS_CLOSED => 'process.status.closed',
    ];

	public function __construct() {
        $this->sessionsList = new ArrayCollection();
        $this->goalCards = new ArrayCollection();
        $this->summary = new ArrayCollection();
    }

	/**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idMentee
     *
     * @param integer $idMentee
     *
     * @return MentorProcess
     */
    public function setIdMentee($idMentee)
    {
        $this->idMentee = $idMentee;

        return $this;
    }

    /**
     * Get idMentee
     *
     * @return int
     */
    public function getIdMentee()
    {
        return $this->idMentee;
    }

    /**
     * Set idMentor
     *
     * @param integer $idMentor
     *
     * @return MentorProcess
     */
    public function setIdMentor($idMentor)
    {
        $this->idMentor = $idMentor;

        return $this;
    }

    /**
     * @return the $sessionsList
     */
    public function getSessionsList() {
        return $this->sessionsList;
    }

	/**
     * @return the $goalCards
     */
    public function getGoalCards() {
        return $this->goalCards;
    }

	/**
     * @param Ambigous <\Doctrine\Common\Collections\ArrayCollection, MentorSession> $sessionsList
     */
    public function setSessionsList($sessionsList) {
        $this->sessionsList = $sessionsList;
    }

	/**
     * @param \Doctrine\Common\Collections\ArrayCollection $goalCards
     */
    public function setGoalCards($goalCards) {
        $this->goalCards = $goalCards;
    }

	/**
     * Get idMentor
     *
     * @return int
     */
    public function getIdMentor()
    {
        return $this->idMentor;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return MentorProcess
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return MentorProcess
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return MentorProcess
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set sessions
     *
     * @param integer $sessions
     *
     * @return MentorProcess
     */
    public function setSessions($sessions)
    {
        $this->sessions = $sessions;

        return $this;
    }

    /**
     * Get sessions
     *
     * @return int
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    public function getClosingUser()
    {
        return $this->closingUser;
    }

    public function setClosingUser($user)
    {
        $this->closingUser = $user;
    }

    public function setCloseReason($reason)
    {
        $this->closeReason = $reason;
    }

    public function getCloseReason()
    {
        return $this->closeReason;
    }
    
    public function setSessionsRealized($sessions) 
    {
        $this->sessionsRealized = $sessions;
        return $this;
    }
    
    public function getSessionsRealized()
    {
        return $this->sessionsRealized;
    }
    
    public function setSessionsPlanned($sessions)
    {
        $this->sessionsPlanned = $sessions;
        return $this;
    }
    
    public function getSessionsPlanned()
    {
        return $this->sessionsPlanned;
    }
    
    public function getSessionsLeft()
    {
        return $this->sessions - $this->sessionsRealized;
    }
    
    public function getSessionsLeftToPlanned() 
    {
        return $this->sessions - $this->sessionsPlanned;
    }

    public function isClosed() {
        return $this->status == self::STATUS_CLOSED;
    }

    /**
     * Set blockedForClient
     *
     * @param integer $blockedForClient
     *
     * @return MentorProcess
     */
    public function setBlockedForClient($blockedForClient)
    {
        $this->blockedForClient = $blockedForClient;

        return $this;
    }

    /**
     * Get blockedForClient
     *
     * @return int
     */
    public function getBlockedForClient()
    {
        return $this->blockedForClient;
    }

    public function getStatusText() {
        return $this->statusesText[$this->status];
    }

    public static function getAllStatuses() {
        return array_flip(self::$statuses);
    }

    /**
     * @return the $summary
     */
    public function getSummary() {
        return $this->summary;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $summary
     */
    public function setSummary($summary) {
        $this->summary = $summary;
    }

    /**
     * @return the $mentor
     */
    public function getMentor() {
        return $this->mentor;
    }

    /**
     * @return the $mentee
     */
    public function getMentee() {
        return $this->mentee;
    }

    /**
     * @param field_type $mentor
     */
    public function setMentor($mentor) {
        $this->mentor = $mentor;
    }

    /**
     * @param field_type $mentee
     */
    public function setMentee($mentee) {
        $this->mentee = $mentee;
    }
    
    public function addSession(MentorSession $session)
    {
        if ($this->sessionsList->contains($session)) {
            return;
        }
        $this->sessionsList[] = $session;
        $session->setProcess($this);
    }
    
    public function removeSession(MentorSession $session)
    {
        if (!$this->sessionsList->contains($session)) {
            return;
        }
        $this->sessionsList->removeElement($session);
        $session->setProcess(null);
    }
    
    public function addGoalCard(MentorGoalCard $goalCard)
    {
        if ($this->goalCards->contains($goalCard)) {
            return;
        }
        $this->goalCards[] = $goalCard;
        $goalCard->setProcess($this);
    }
    
    public function removeGoalCard(MentorGoalCard $goalCard)
    {
        if (!$this->goalCards->contains($goalCard)) {
            return;
        }
        $this->goalCards->removeElement($goalCard);
        $goalCard->setProcess(null);
    }
    
    public function addSummary(MentorProcessSummary $summary)
    {
        if ($this->summary->contains($summary)) {
            return;
        }
        $this->summary[] = $summary;
        $summary->setProcess($this);
    }

    public function removeSummary(MentorProcessSummary $summary)
    {
        if (!$this->summary->contains($summary)) {
            return;
        }
        $this->summary->removeElement($summary);
        $summary->setProcess(null);
    }
}

