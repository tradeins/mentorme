<?php

namespace MentorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="user_client")
 * @UniqueEntity(fields = "username", targetClass = "AppBundle\Entity\User", message="fos_user.username.already_used")
 * @UniqueEntity(fields = "email", targetClass = "AppBundle\Entity\User", message="fos_user.email.already_used")
 */
class UserClient extends User
{
    protected $entityType = 'user_client';
    protected $redirectPath = 'list_clients';
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    

    /**
     * @Assert\Regex(
     *     pattern="/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     match=true,
     *     message="Please insert only numbers"
     * )
     * @ORM\Column(type="integer", nullable=true, options={"default":0})
     */
    public $hoursLimit;
    
    /**
     * @Assert\Regex(
     *     pattern="/^[0-9]\d*$/",
     *     htmlPattern = "^[0-9]\d*$",
     *     match=true,
     *     message="Please insert only numbers"
     * )
     * @ORM\Column(type="integer", nullable=true, options={"default":0})
     */
    public $menteeLimit;
    
    /**
    * @ORM\Column(type="string", length=40, nullable=true)
    */
    public $personFunction;
    
    /**
     * @ORM\ManyToOne(targetEntity="MentorBundle\Entity\Company", inversedBy="clients")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    public $company; 
    
    /**
     * @ORM\OneToMany(targetEntity="MentorBundle\Entity\UserMentee", mappedBy="client")
     */
    public $mentees;
    
    public function __construct() {
        parent::__construct();
        $this->mentees = new ArrayCollection();
        $this->menteeWatchers = new ArrayCollection();
    }
    
    public function getCompany()
    {
        return $this->company;
    }
    
    public function getHoursLimit()
    {
        return $this->hoursLimit;
    }
    
    public function getMenteeLimit()
    {
        return $this->menteeLimit;
    }
    
    public function getPersonFunction()
    {
        return $this->personFunction;
    }
    
    public function getCompanyName() {
        if (empty($this->getCompany())) {
            return '';
        }
        return $this->getCompany()->getCompanyName();
    }

    /**
     * @return Collection
     */
    public function getMentees() {
        return $this->mentees;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->addRole('ROLE_CLIENT');
    }
}