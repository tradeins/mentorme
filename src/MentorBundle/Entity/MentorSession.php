<?php

namespace MentorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MentorSession
 *
 * @ORM\Table(name="mentor_session")
 * @ORM\Entity(repositoryClass="MentorBundle\Repository\MentorSessionRepository")
 */
class MentorSession
{
    const STATUS_TO_ACCEPT = 0;
    const STATUS_TO_MOVE = 1;
    const STATUS_TO_REALIZATION = 2;
    const STATUS_REALIZED = 3;
    const STATUS_TO_CONFIRM = 4;
    
    protected $statusText = [
        self::STATUS_TO_ACCEPT => 'session.status.to_accept',
        self::STATUS_TO_MOVE => 'session.status.to_move',
        self::STATUS_TO_REALIZATION => 'session.status.to_realization',
        self::STATUS_REALIZED => 'session.status.realized',
        self::STATUS_TO_CONFIRM => 'session.status.to_confirm',
    ];
    
    public static $statusTextChoices = [
        null => "session.status.all",
        self::STATUS_TO_ACCEPT => 'session.status.to_accept',
        self::STATUS_TO_MOVE => 'session.status.to_move',
        self::STATUS_TO_REALIZATION => 'session.status.to_realization',
        self::STATUS_REALIZED => 'session.status.realized',
        self::STATUS_TO_CONFIRM => 'session.status.to_confirm',
    ];
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_process", type="integer")
     */
    private $idProcess;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer",options={"default":0})
     */
    private $status = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $endDate;

    /**
     * @var int
     *
     * @ORM\Column(name="confirmed_mentee", type="integer", options={"default":0})
     */
    private $confirmedMentee = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="confirmed_mentor", type="integer",options={"default":0})
     */
    private $confirmedMentor = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime")
     */
    private $createdDate;

    /**
     * @ORM\Column(name="acceptToken", type="string",nullable=true)
     * @var string
     */
    private $acceptToken;
    
    
    /**
     * @ORM\Column(name="accept_mentee", type="integer",options={"default":0})
     * @var integer
     */
    private $acceptByMentee = 0;
    
    /**
     * @ORM\Column(name="accept_mentor", type="integer",options={"default":0})
     * @var integer
     */
    private $acceptByMentor = 0;
    
    /**
     * @ORM\ManyToOne(targetEntity="MentorBundle\Entity\MentorProcess", inversedBy="sessionsList",cascade={"persist"})
     * @ORM\JoinColumn(name="id_process", referencedColumnName="id")
     */
    public $process;

    /**
     * Action on form to accept
     * @var string
     */
    public $action;
    
    /**
     * Reason of session reject
     * @ORM\Column(name="reject_reason", type="string",nullable=true)
     * @var string
     */
    public $rejectReason = '';
    
    
    /**
     * @return the $action
     */
    public function getAction() {
        return $this->action;
    }

	/**
     * @return the $rejectReason
     */
    public function getRejectReason() {
        return $this->rejectReason;
    }

	/**
     * @param string $action
     */
    public function setAction($action) {
        $this->action = $action;
    }

	/**
     * @param string $rejectReason
     */
    public function setRejectReason($rejectReason) {
        $this->rejectReason = $rejectReason;
    }

	/**
     * @return the $acceptToken
     */
    public function getAcceptToken() {
        return $this->acceptToken;
    }

	/**
     * @return the $acceptByMentee
     */
    public function getAcceptByMentee() {
        return $this->acceptByMentee;
    }

	/**
     * @return the $acceptByMentor
     */
    public function getAcceptByMentor() {
        return $this->acceptByMentor;
    }

	/**
     * @param string $acceptToken
     */
    public function setAcceptToken($acceptToken) {
        $this->acceptToken = $acceptToken;
    }

	/**
     * @param number $acceptByMentee
     */
    public function setAcceptByMentee($acceptByMentee) {
        $this->acceptByMentee = $acceptByMentee;
    }

	/**
     * @param number $acceptByMentor
     */
    public function setAcceptByMentor($acceptByMentor) {
        $this->acceptByMentor = $acceptByMentor;
    }

	/**
     * @return the $process
     */
    public function getProcess() {
        return $this->process;
    }

	/**
     * @param field_type $process
     */
    public function setProcess($process) {
        $this->process = $process;
    }

	/**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idProcess
     *
     * @param integer $idProcess
     *
     * @return MentorSession
     */
    public function setIdProcess($idProcess)
    {
        $this->idProcess = $idProcess;

        return $this;
    }

    /**
     * Get idProcess
     *
     * @return int
     */
    public function getIdProcess()
    {
        return $this->idProcess;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return MentorSession
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return MentorSession
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return MentorSession
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set confirmedMentee
     *
     * @param integer $confirmedMentee
     *
     * @return MentorSession
     */
    public function setConfirmedMentee($confirmedMentee)
    {
        $this->confirmedMentee = $confirmedMentee;

        return $this;
    }

    /**
     * Get confirmedMentee
     *
     * @return int
     */
    public function getConfirmedMentee()
    {
        return $this->confirmedMentee;
    }

    /**
     * Set confirmedMentor
     *
     * @param integer $confirmedMentor
     *
     * @return MentorSession
     */
    public function setConfirmedMentor($confirmedMentor)
    {
        $this->confirmedMentor = $confirmedMentor;

        return $this;
    }

    /**
     * Get confirmedMentor
     *
     * @return int
     */
    public function getConfirmedMentor()
    {
        return $this->confirmedMentor;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return MentorSession
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }
    
    public function isBothConfirmed() 
    {
        return $this->confirmedMentor && $this->confirmedMentee;
    }
    public function isBothAccepted()
    {
        return $this->acceptByMentor && $this->acceptByMentee;
    }
    
    public function isAcceptedStatus() {
        return $this->status == self::STATUS_TO_ACCEPT;
    }
    public function isRealizedStatus() {
        return $this->status == self::STATUS_REALIZED;
    }
    public function isRejectedStatus() {
        return $this->status == self::STATUS_TO_MOVE;
    }
    public function isToConfirmStatus() {
        return $this->status == self::STATUS_TO_CONFIRM;
    }
    public function getStatusText() {
        return $this->statusText[$this->status];
    }
    public function isToRealizationStatus() {
        return $this->status == self::STATUS_TO_REALIZATION;
    }

    public function canShowProposeNewDateLink() {
        return !$this->isToRealizationStatus() && !$this->isBothConfirmed();
    }
}

