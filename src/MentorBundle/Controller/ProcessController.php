<?php

namespace MentorBundle\Controller;


use AppBundle\Entity\User;
use MentorBundle\Entity\UserMentor;
use MentorBundle\Entity\MentorProcess;
use MentorBundle\Entity\MentorSession;
use MentorBundle\Entity\MentorProcessSummary;
use MentorBundle\Entity\MentorGoalCard;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use MentorBundle\Form\Filter\Lists\MenteeProcessFilterType;
use MentorBundle\Response\PdfResponse;

/**
 * @Route("/process");
 * @author mgrabarczyk
 *
 */
class ProcessController extends Controller
{
    /**
     * @Route("/show_my/", name="process_show_my")
     */
    public function showMyProcessesAction(Request $request)
    {
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:MentorProcess')
        ->createQueryBuilder('e');
  
        $filterBuilder->where('e.idMentee = ' . $this->getUser()->getId())->orderBy('e.startDate', 'DESC');
        
        $form = $this->get('form.factory')->create(MenteeProcessFilterType::class);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);            
        }
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $filterBuilder->getQuery(),
                $request->query->getInt('page', 1),
                10
        );
        return $this->render('MentorBundle:Process:my_processes.html.twig', array(
           'pagination' => $pagination,
           'form' => $form->createView(),
           'printQueryString' => $request->getQueryString(),
        ));
    }

    /**
     * @Route("/pdf/process/", name="process_pdf_process_list")
     */
    public function printPdfProcess(Request $request) {
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:MentorProcess')
        ->createQueryBuilder('e');

        $filterBuilder->where('e.idMentee = ' . $this->getUser()->getId())->orderBy('e.startDate', 'DESC');

        $form = $this->get('form.factory')->create(MenteeProcessFilterType::class);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $filterBuilder->getQuery(),
                $request->query->getInt('page', 1),
                10
        );
        $data = $form->getData();
        $filters = [];
        if (!empty($data)) {
            $filters = [
                'startDate' => !empty($data['startDate']['left_date']) ? $data['startDate']['left_date']->format('Y-m-d') : null,
                'endDate' => !empty($data['endDate']['right_date']) ? $data['endDate']['right_date']->format('Y-m-d') : null,
            ];
        }
        $html = $this->renderView('MentorBundle:Process:pdf_list_process.html.twig', [
            'filters' => $filters,
            'pagination' => $pagination,
        ]);
        $response = new PdfResponse($this->get("white_october.tcpdf"));
        $response->generatePdfResponseFromHtml('processList', $html, 'My Process List', 'horizontal');
    }
    
    private function checkClientAccess(MentorProcess $process) 
    {
        if ($this->getUser()->getEntityType() == 'user_client' && $process->getBlockedForClient()) {
            return $this->redirectToRoute('clients_process_list');
        }
        return null;
    }

    /**
     * @Route("/close/{id}", name="process_close", requirements={"id"="\d+"})
     * @param MentorProcess $process
     */
    public function closeProcessAction(MentorProcess $process, Request $request)
    {
        $user = $this->getUser();
        if (($process->getMentee() != $user && $process->getMentor() != $user) || !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('dashboard');
        }

        $form = $this->createForm('MentorBundle\Form\MentorProcessCloseType', $process);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $reason = $data->getCloseReason();
            $process->setClosingUser($this->getUser());
            $process->setStatus(MentorProcess::STATUS_CLOSED);
            $em = $this->getDoctrine()->getManager();
            $em->persist($process);
            $em->flush();
            return $this->redirectToRoute('process_show', ['id' => $process->getId()]);
        }

        return $this->render('MentorBundle:Process:close_process.html.twig', [
            'form' => $form->createView(),
            'process' => $process,
         ]);
    }
    
    /**
     * @Route("/show/{id}", name="process_show", requirements={"id"="\d+"})
     * @param MentorProcess $process
     */
    public function showProcessAction(MentorProcess $process)
    {
        if ($response = $this->checkClientAccess($process)) {
            return $response;
        }
        return $this->render('MentorBundle:Process:show.html.twig', $this->prepareProcessData($process));
    }

    /**
     * @Route("/goal_card/{id}", name="process_goal_card_show", requirements={"id"="\d+"})
     * @param MentorProcess $process
     */
    public function showGoalCardAction(MentorProcess $process)
    {
        if ($response = $this->checkClientAccess($process)) {
            return $response;
        }
        return $this->render('MentorBundle:Process:goal_card.html.twig', $this->prepareProcessData($process));
    }

    /**
     * @Route("/goal_card/{id}/edit", name="process_goal_card_edit", requirements={"id"="\d+"})
     * @param MentorGoalCard $goalCard
     */
    public function editGoalCardAction(Request $request, MentorGoalCard $goalCard)
    {
        $editForm = $this->createForm('MentorBundle\Form\MentorGoalCardType', $goalCard);
        $editForm->handleRequest($request);
        
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('process_goal_card_show', array('id' => $goalCard->getId()));
        }
        
        return $this->render('MentorBundle:Process:edit_goal_card.html.twig', array(
            'goal_card' => $goalCard,
            'edit_form' => $editForm->createView(),
        ));
    }
    
    private function prepareProcessData(MentorProcess $process) 
    {
        $mentee = $process->getMentee();
        $sessionList = array_pad($process->getSessionsList()->getValues(), $process->getSessions(), null);
        
        return [
          'sessionRealizedStatus' => MentorSession::STATUS_REALIZED,
          'user' => $this->getUser(),
          'process' => $process,
          'mentor' => $process->getMentor(),
          'mentee' => $mentee,
          'client' => $mentee->getClient(),
          'company' => $mentee->getCompany(),
          'menteeWatcher' => $mentee->getMenteeWatcher(),
          'sessionList' => $sessionList,
          'summary' => $process->getSummary()->first(),
          'goalCard' => $process->getGoalCards()->first(),
        ];
    }

    /**
     * @Route("/summary/{id}/edit", name="process_summary_edit", requirements={"id"="\d+"})
     * @param MentorProcessSummary $process
     */
    public function editProcessSummaryAction(Request $request, MentorProcessSummary $summary) 
    {
        $editForm = $this->createForm('MentorBundle\Form\MentorProcessSummaryType', $summary);
        $editForm->handleRequest($request);
        
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        
            return $this->redirectToRoute('process_show', array('id' => $summary->getProcess()->getId()));
        }
        
        return $this->render('MentorBundle:Process:edit_summary.html.twig', array(
            'summary' => $summary,
            'edit_form' => $editForm->createView(),
        ));
    }
    
    /**
     * @Route("/block/{id}", name="process_block_for_client", requirements={"id"="\d+"})
     * @Method({"POST"})
     * @param MentorProcess $process
     */
    public function blockForClientAction(MentorProcess $process, Request $request) 
    {
        $process->setBlockedForClient(!$process->getBlockedForClient());
        $em = $this->getDoctrine()->getManager();
        $em->persist($process);
        $em->flush();
        return new JsonResponse(['processId' => $process->getId(), 'blocked' => $process->getBlockedForClient()]);
    }
    
    /**
     * @Route("/goal_card_accept/{id}", name="process_goal_card_accept", requirements={"id"="\d+"})
     * @Method({"POST"})
     * @param MentorGoalCard $goalCard
     */
    public function acceptGoalCardAction(MentorGoalCard $goalCard, Request $request)
    {
        $userType = $request->get('userType', null);
        if ($userType == 'mentee') {
            $goalCard->setAcceptedMentee(1);
        }
        if ($userType == 'mentor') {
            $goalCard->getAcceptedMentor(1);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($goalCard);
        $em->flush();
        return new JsonResponse(['msg' => 'Done']);
    }
    
    /**
     * @Route("/pdf/{id}", name="process_pdf", requirements={"id"="\d+"})
     * @param MentorProcess $process
     */
    public function generateProcessPdfAction(MentorProcess $process) {
        if ($response = $this->checkClientAccess($process)) {
            return $response;
        }
        $html = $this->renderView('MentorBundle:Process:pdf.html.twig', $this->prepareProcessData($process));
        $response = new PdfResponse($this->get("white_october.tcpdf"));
        $response->generatePdfResponseFromHtml('process_'.$process->getId(), $html, 'Process View', 'vertical');
    }
    
    /**
     * @Route("/pdf/goal/{id}", name="process_pdf_goal", requirements={"id"="\d+"})
     * @param MentorProcess $process
     */
    public function generateGoalCardPdfAction(MentorGoalCard $goalCard) {
        $process = $goalCard->getProcess();
        if ($response = $this->checkClientAccess($process)) {
            return $response;
        }
        $html = $this->renderView('MentorBundle:Process:pdf_goal_card.html.twig', $this->prepareProcessData($process));
        $response = new PdfResponse($this->get("white_october.tcpdf"));
        $response->generatePdfResponseFromHtml('process_goal_card_'.$process->getId(), $html, 'Process Goal Card', 'vertical');
    }
}