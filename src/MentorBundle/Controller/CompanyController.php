<?php

namespace MentorBundle\Controller;

use MentorBundle\Entity\Company;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use MentorBundle\Form\Filter\Lists\CompanyFilterType;

/**
 * Company controller.
 *
 * @Route("/company")
 */
class CompanyController extends Controller
{
    /**
     * Lists all company entities.
     *
     * @Route("/", name="company_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:Company')
        ->createQueryBuilder('e');
        
        $form = $this->get('form.factory')->create(CompanyFilterType::class);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }
        
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $filterBuilder->getQuery(),
                $request->query->getInt('page', 1),
                10
        );
        
        return $this->render('MentorBundle:Company:index.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new company entity.
     *
     * @Route("/new", name="company_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $company = new Company();
        $form = $this->createForm('MentorBundle\Form\CompanyType', $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($company);
            $em->flush();

            return $this->redirectToRoute('company_show', array('id' => $company->getId()));
        }

        return $this->render('MentorBundle:Company:new.html.twig', array(
            'company' => $company,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a company entity.
     *
     * @Route("/{id}", name="company_show")
     * @Method("GET")
     */
    public function showAction(Company $company)
    {
        return $this->render('MentorBundle:Company:show.html.twig', array(
            'company' => $company,
        ));
    }

    /**
     * Displays a form to edit an existing company entity.
     *
     * @Route("/{id}/edit", name="company_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Company $company)
    {
        $editForm = $this->createForm('MentorBundle\Form\CompanyType', $company);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('company_edit', array('id' => $company->getId()));
        }

        return $this->render('MentorBundle:Company:edit.html.twig', array(
            'company' => $company,
            'edit_form' => $editForm->createView(),
        ));
    }
    
    /**
     * @Route("/change_status/{id}", name="change_company_status", requirements={"id"="\d+"})
     * @param Request $request
     * @param Company $company
     */
    public function changeCompanyStatus(Request $request, Company $company)
    {
        $company->setEnabled(!$company->getEnabled());
        $em = $this->getDoctrine()->getManager();
        $em->persist($company);
        $em->flush();
        return $this->redirect( $request
                ->headers
                ->get('referer'));
    }
    
    /**
     * @Route("/terms/{id}", name="company_terms", requirements={"id"="\d+"})
     * @param Company $company
     */
    public function showTermsAction(Company $company)
    {
        return $this->render('MentorBundle:Company:terms.html.twig', [
            'company' => $company
        ]);
    }
}
