<?php

namespace MentorBundle\Controller;

use PUGX\MultiUserBundle\Model\UserDiscriminator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use MentorBundle\Controller\RegistrationController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use MentorBundle\Form\Multiuser\FormFactory;

class RegistrationManager
{
    /**
     * @var \PUGX\MultiUserBundle\Model\UserDiscriminator
     */
    protected $userDiscriminator;

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var \FOS\UserBundle\Controller\RegistrationController
     */
    protected $controller;

    /**
     * @var \PUGX\MultiUserBundle\Form\FormFactory
     */
    protected $formFactory;

    /**
     * @param \PUGX\MultiUserBundle\Model\UserDiscriminator             $userDiscriminator
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     * @param \FOS\UserBundle\Controller\RegistrationController         $controller
     * @param \PUGX\MultiUserBundle\Form\FormFactory                    $formFactory
     */
    public function __construct(UserDiscriminator $userDiscriminator,
            ContainerInterface $container,
            RegistrationController $controller,
            FormFactory $formFactory)
    {
        $this->userDiscriminator = $userDiscriminator;
        $this->container = $container;
        $this->controller = $controller;
        $this->formFactory = $formFactory;
    }
    
    /**
     * @param string $class
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function register($class)
    {
        $this->userDiscriminator->setClass($class);
        $this->controller->setContainer($this->container);
        $result = $this->controller->registerAction($this->getRequest());
        if ($result instanceof RedirectResponse) {
            return $result;
        }

        $template = $this->userDiscriminator->getTemplate('registration');
        if (is_null($template)) {
            $template = 'FOSUserBundle:Registration:register.html.twig';
        }
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $form = $this->formFactory->createForm(['user' => $user]);

        return $this->container->get('templating')->renderResponse($template, [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }
    /**
     * @return \Symfony\Component\HttpFoundation\Request;
     */
    private function getRequest()
    {
        return $this->container->get('request_stack')->getCurrentRequest();
    }
    
}
