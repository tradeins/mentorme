<?php

namespace MentorBundle\Controller;

use MentorBundle\Entity\UserMentor;
use MentorBundle\Entity\MentorProcess;
use MentorBundle\Entity\MentorSession;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/user")
 * @author mgrabarczyk
 *
 */
class UserController extends Controller
{
    /**
     * @Route("/profile/{id}", name="user_profile_view")
     */
    public function userProfileAction(User $user, Request $request)
    {
        $session = null;
        $company = null;
        $process = null;
        $plan = $request->get('plan', false);
        $sessionId = $request->get('sid', false);
        
        $loggedUser = $this->getUser();
        if ($loggedUser->getEntityType() == 'user_mentee') {
            $company = $loggedUser->getCompany();
            $process = $this->getProcess($user, $loggedUser);
        }
        
        if ($sessionId) {
            $session = $this->getSession($sessionId);
            $process = $session->getProcess();
            if ($process->getMentee() != $loggedUser && $process->getMentor() != $loggedUser) {
                return new RedirectResponse($this->generateUrl('dashboard'));
            }
        }
        
        $mentorProcess = new MentorProcess();
        $form = $this->createForm('MentorBundle\Form\MentorProcessType', $mentorProcess);
        
        return $this->render('FOSUserBundle:Profile:show.html.twig',
            [
               'user' => $user,
               'company' => $company,
               'businessHours' => $this->prepareBusinessHours($user, $this->get('doctrine.orm.entity_manager')),
               'eventForm' => $form->createView(),
               'userIsMentee' => ($this->getUser()->getEntityType() == 'user_mentee'),
               'process' => $process,
               'planDate' => ($plan && $process)? $process->getEndDate()->format('Y-m-d') : null,
               'sessionDate' => $session ? $session->getStartDate()->format('Y-m-d') : null,
               'session' => $session,
            ]
        );
    }

    /**
     * @Route("/business_hours/{id}", name="user_get_business", defaults={"id"=null})
     * @Method({"POST"})
     * @param User $user
     * @param Request $request
     * @return JsonResponse
     */
    public function getBusinessHoursUserAction(User $user, Request $request) {
        $startDate = $request->get('startDate', new \DateTime());
        $endDate = $request->get('endDate', new \DateTime());
        $businessHours = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:BusinessHours')
        ->findByMentor($user->getId(), $startDate, $endDate);
        $hours = [];
        foreach ($businessHours as $bh) {
            $hours[] = [
                'dow' => [$bh->getOpen()->format('N')],
                'start' => $bh->getOpen()->format('H:i'),
                'end' => $bh->getEnd()->format('H:i'),
            ];
        }
        if (empty($hours)) {
            $hours[] = [
                'dow' => [1,2,3,4,5],
                'start' => '00:00',
                'end' => '00:01'
            ];
        }

        return new JsonResponse(json_encode($hours));
    }
   
    public function prepareBusinessHours(User $user, $entityManager)
    {
        if ($user->getEntityType() != 'user_mentor') {
            return json_encode('');
        }

        $date = date('Y-m-d');
        $nbDay = date('N', strtotime($date));
        $monday = new \DateTime($date);
        $sunday = new \DateTime($date);
        $monday->modify('-'.($nbDay-1).' days');
        $sunday->modify('+'.(7-$nbDay).' days');

        $businessHours = $entityManager
        ->getRepository('MentorBundle:BusinessHours')
        ->findByMentor($user->getId(), $monday, $sunday);
        $hours = [];
        foreach ($businessHours as $bh) {
            $hours[] = [
                'dow' => [$bh->getOpen()->format('N')],
                'start' => $bh->getOpen()->format('H:i'),
                'end' => $bh->getEnd()->format('H:i'),
            ];
        }
        return json_encode($hours);
    }
    
    private function getProcess($mentor, $mentee) {
        $process = $this->getDoctrine()
        ->getRepository(MentorProcess::class)
        ->findOneBy([
                'mentee' => $mentee,
                'mentor' => $mentor,
                'status' => [MentorProcess::STATUS_INACTIVE, MentorProcess::STATUS_UNREALIZED],
                ],
                ['id' => 'DESC']
        );
        if (!$process || $process->getSessionsLeftToPlanned() == 0) {
            return null;
        }
        return $process;
    }
    
    private function getSession($id) {
        $session = $this->getDoctrine()
        ->getRepository(MentorSession::class)
        ->find($id);
        if (!$session) {
            return null;
        }
        return $session;
    }
    
}