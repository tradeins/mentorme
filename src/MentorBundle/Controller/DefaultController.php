<?php

namespace MentorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/main", name="")
     */
    public function indexAction()
    {
        $notifications = $this->getUser()->getNotifications();
        return $this->render('MentorBundle:Default:index.html.twig',[
                'notifications' => $notifications
        ]);
    }
}
