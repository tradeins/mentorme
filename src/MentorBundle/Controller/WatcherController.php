<?php

namespace MentorBundle\Controller;


use AppBundle\Entity\User;;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MentorBundle\Form\Filter\Lists\WatcherProcessFilterType;

/**
 * @Route("/watcher");
 * @author mgrabarczyk
 *
 */
class WatcherController extends Controller
{

    /**
     * @Route("/mentees/", name="watcher_mentee_list")
     */
    public function listMenteeAction(Request $request)
    {
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:UserMentee')
        ->createQueryBuilder('e');
        $filterBuilder->where('e.menteeWatcher = '.$this->getUser()->getId());
        
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $filterBuilder->getQuery(),
                $request->query->getInt('page', 1),
                10
        );
        return $this->render('MentorBundle:Watcher:list_mentee.html.twig', array(
            'pagination' => $pagination,
        ));
    }
    
    /**
     * @Route("/process/", name="watcher_process_list")
     */
    public function processMenteeAction(Request $request)
    {
        $userId = $this->getUser()->getId();
        
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:MentorProcess')
        ->findMyMenteeProcessesQuery($userId);
        
        $form = $this->get('form.factory')->create(WatcherProcessFilterType::class, null, ['watcherId' => $userId]);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }
        
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $filterBuilder->getQuery(),
                $request->query->getInt('page', 1),
                10
        );
        return $this->render('MentorBundle:Watcher:list_process.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView(),
        ));
    }
}