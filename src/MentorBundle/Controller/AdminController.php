<?php
namespace MentorBundle\Controller;

use MentorBundle\MentorBundle;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use MentorBundle\Response\PdfResponse;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use MentorBundle\Form\Filter\Lists\ClientProcessFilterType;
use MentorBundle\Form\Filter\Lists\UserFilterType;
use MentorBundle\Form\Filter\Lists\Sessions\AdminSessionFilterType;

/**
 * @Route("/adm")
 * @author mgrabarczyk
 *
 */
class AdminController extends Controller
{
    /**
     * @Route("/list_clients/", name="list_clients")
     */
    public function listClientsAction(Request $request)
    {
        return $this->render('MentorBundle:Admin:list_client.html.twig',
            $this->prepareListData('MentorBundle:UserClient', $request)
        );
    }
    
    /**
     * @Route("/list_mentor/", name="list_mentor")
     */
    public function listMentorAction(Request $request)
    {
        return $this->render('MentorBundle:Admin:list_mentor.html.twig',
            $this->prepareListData('MentorBundle:UserMentor', $request)
        );
    }
    
    /**
     * @Route("/list_mentee/", name="list_mentee")
     */
    public function listMenteeAction(Request $request)
    {
        return $this->render('MentorBundle:Admin:list_mentee.html.twig', 
            $this->prepareListData('MentorBundle:UserMentee', $request)
        );
    }
    
    /**
     * @Route("/list_mentee_watchers/", name="list_mentee_watchers")
     */
    public function listMentorWatchersAction(Request $request)
    {
        return $this->render('MentorBundle:Admin:list_mentee_watcher.html.twig',
            $this->prepareListData('MentorBundle:UserMenteeWatcher', $request)
        );
    }
    
    /**
     * @Route("/list_processes/", name="list_processes")
     */
    public function listProcessesAction(Request $request)
    {
        return $this->render('MentorBundle:Admin:list_processes.html.twig',
            $this->prepareListData('MentorBundle:MentorProcess', $request)
        );
    }
    
    /**
     * @Route("/list_sessions/", name="list_sessions")
     * @param Request $request
     */
    public function listSessionsAction(Request $request)
    {
        return $this->render('MentorBundle:Admin:list_sessions.html.twig',
                $this->prepareListData('MentorBundle:MentorSession', $request)
        );
    }
    
    private function getFormFilterType($entityType)
    {
        switch ($entityType) {
            case 'MentorBundle:MentorSession':
                return AdminSessionFilterType::class;
            case 'MentorBundle:MentorProcess':
                return ClientProcessFilterType::class;
            case 'MentorBundle:UserMenteeWatcher':
            case 'MentorBundle:UserMentee':
            case 'MentorBundle:UserMentor':
            case 'MentorBundle:UserClient':
                return UserFilterType::class;
        }
    } 

    private function prepareListData($entityType, Request $request) 
    {
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository($entityType)
        ->createQueryBuilder('e');
        
        $showCompanyFilter = ($entityType != 'MentorBundle:UserMentor');
        if ($entityType == 'MentorBundle:MentorSession') {
            $filterBuilder->leftJoin('e.process', 'cp');
            $filterBuilder->leftJoin('cp.mentee', 'ce');
        }
        
        $form = $this->get('form.factory')->create($this->getFormFilterType($entityType), null, ['show_company' => $showCompanyFilter]);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }

        $data = $form->getData();
        $filters = [];
        if (!empty($data)) {
            $filters = [
                'startDate' => !empty($data['startDate']['left_date']) ? $data['startDate']['left_date']->format('Y-m-d') : null,
                'endDate' => !empty($data['endDate']['right_date']) ? $data['endDate']['right_date']->format('Y-m-d') : null,
            ];
        }

        $paginator  = $this->get('knp_paginator');
        return [
            'pagination' => $paginator->paginate(
                $filterBuilder->getQuery(),
                $request->query->getInt('page', 1),
                10),
            'form' => $form->createView(),
            'filters' => $filters,
            'printQueryString' => $request->getQueryString(),
         ]; 
    }

    /**
     * @Route("/pdf/process/", name="adm_pdf_process_list")
     */
    public function printPdfProcess(Request $request) {
        $html = $this->renderView('MentorBundle:Watcher:pdf_list_process.html.twig', $this->prepareListData('MentorBundle:MentorProcess', $request));
        $response = new PdfResponse($this->get("white_october.tcpdf"));
        $response->generatePdfResponseFromHtml('processList', $html, 'My Process List', 'horizontal');
    }

    /**
     * @Route("/register_client/", name="register_client")
     */
    public function createClient(Request $request)
    {
        return $this->container
            ->get('pugx_multi_user.registration_manager')
            ->register('MentorBundle\Entity\UserClient');
    }
    
    /**
     * @Route("/register_mentor/", name="register_mentor")
     */
    public function createMentor(Request $request)
    {
        return $this->container
        ->get('pugx_multi_user.registration_manager')
        ->register('MentorBundle\Entity\UserMentor');
    }
    
    /**
     * @Route("/register_mentee/", name="register_mentee")
     */
    public function createMentee(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
            $userManager = $this->get('fos_user.user_manager');
            $count = $userManager->countActiveUsersByRole($this->getUser()->getId(), 'ROLE_MENTEE');
            if ($count[0]['co'] >= $this->getUser()->getMenteeLimit()) {
                $this->addFlash(
                        'warning',
                        'flash.exceeded_limit.mentee.add'
                );
                return $this->redirect($this->generateUrl('clients_mentee_watchers_list'));
            }
        }
        return $this->container
        ->get('pugx_multi_user.registration_manager')
        ->register('MentorBundle\Entity\UserMentee');
    }
    
    /**
     * @Route("/register_mentee_watcher/", name="register_mentee_watcher")
     */
    public function createMenteeWatcher(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
            $userManager = $this->get('fos_user.user_manager');
            $count = $userManager->countActiveUsersByRole($this->getUser()->getId(), 'ROLE_MENTEE_WATCHER');
            if ($count[0]['co'] >= $this->getUser()->getMenteeLimit()) {
                $this->addFlash(
                        'warning',
                        'flash.exceeded_limit.mentee_watcher.add'
                );
                return $this->redirect($this->generateUrl('clients_mentee_watchers_list'));
            }
        }
        return $this->container
        ->get('pugx_multi_user.registration_manager')
        ->register('MentorBundle\Entity\UserMenteeWatcher');
    }
        
    private function determineSuccessPath($clientPath, $adminPath){
        if ($this->getUser()->getEntityType() == 'user_base') {
            return $adminPath;
        }
        return $clientPath;
    }
    
    private function prepareEditAction($entityType, $path, $adminPath, $clientPath = null) 
    {
        $successPath = $adminPath;
        if (!empty($clientPath)) {
            $successPath = $this->determineSuccessPath($clientPath,$adminPath);
        }
        return $this->container
        ->get('pugx_multi_user.profile_manager')
        ->editByOtherUser($entityType, $path, $successPath);
    }
    
    /**
     * @Route("/mentee/{id}/edit", name="client_mentee_edit")
     * @Method({"GET", "POST"})
     */
    public function editMenteeAction(Request $request, User $user)
    {
        return $this->prepareEditAction('MentorBundle\Entity\UserMentee', 'client_mentee_edit', 'list_mentee', 'clients_mentee_list');
    }
    

    /**
     * @Route("/client/{id}/edit", name="user_client_edit")
     * @Method({"GET", "POST"})
     */
    public function editClientAction(Request $request, User $user)
    {
        return $this->prepareEditAction('MentorBundle\Entity\UserClient', 'user_client_edit', 'list_clients');
    }
    

    /**
     * @Route("/mentee_watcher/{id}/edit", name="client_mentee_watcher_edit")
     * @Method({"GET", "POST"})
     */
    public function editMenteeWatcherAction(Request $request, User $user)
    {
        return $this->prepareEditAction('MentorBundle\Entity\UserMenteeWatcher', 'client_mentee_watcher_edit', 'list_mentee_watchers', 'clients_mentee_watchers_list');
    }
    

    /**
     * @Route("/mentor/{id}/edit", name="client_mentor_edit")
     * @Method({"GET", "POST"})
     */
    public function editMentorAction(Request $request, User $user)
    {
        return $this->prepareEditAction('MentorBundle\Entity\UserMentor', 'client_mentor_edit', 'list_mentor');
    }
}