<?php
namespace MentorBundle\Controller;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Controller\RegistrationController as BaseRegistrationController;

class RegistrationController extends BaseRegistrationController
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function registerAction(Request $request)
    {
        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);
        $currentUser = $this->getUser();
        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }
        
        $form = $formFactory->createForm(['user' => $currentUser]);
        $form->setData($user);
        
        $em = $this->get('doctrine.orm.entity_manager');
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            if ($currentUser->getEntityType() == 'user_mentee') {
                $email = $form->get('email')->getData();
                $repository = $em->getRepository('MentorBundle:UserMenteeWatcher');
                $watcher = $repository->findBy(['email' => $email, 'company' => $currentUser->getCompany()->getId()]);
                if ($watcher) {
                    $currentUser->setMenteeWatcher($watcher[0]);
                    $em->persist($currentUser);
                    $em->flush();
                    $url = $this->generateUrl('mentee_show_my_watcher');
                    return new RedirectResponse($url);
                }
            }
            
            if ($form->isValid()) {
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('fos_user_registration_confirmed');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
                if ($currentUser->getEntityType() == 'user_mentee' && $user->getEntityType() == 'user_mentee_watcher') {
                    $currentUser->setMenteeWatcher($user);
                    $em->persist($currentUser);
                    $em->flush();
                    $url = $this->generateUrl('mentee_show_my_watcher');
                    $response = new RedirectResponse($url);
                }
                return $response;
            }

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);

            if (null !== $response = $event->getResponse()) {
                return $response;
            }
        }

        return $this->render('@FOSUser/Registration/register.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
