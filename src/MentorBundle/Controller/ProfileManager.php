<?php

namespace MentorBundle\Controller;

use MentorBundle\Controller\ProfileController;
use MentorBundle\Form\Multiuser\FormFactory;
use PUGX\MultiUserBundle\Model\UserDiscriminator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ProfileManager
{
    /**
     * @var UserDiscriminator
     */
    protected $userDiscriminator;
    
    /**
     * @var ContainerInterface
     */
    protected $container;
    
    /**
     * @var ProfileController
     */
    protected $controller;
    
    /**
     * @var FormFactory
     */
    protected $formFactory;
    
    /**
     * @param UserDiscriminator  $userDiscriminator
     * @param ContainerInterface $container
     * @param ProfileController  $controller
     * @param FormFactory        $formFactory
     */
    public function __construct(
            UserDiscriminator $userDiscriminator,
            ContainerInterface $container,
            ProfileController $controller,
            FormFactory $formFactory
    ) {
        $this->userDiscriminator = $userDiscriminator;
        $this->container = $container;
        $this->controller = $controller;
        $this->formFactory = $formFactory;
    }
    
    /**
     * @param string $class
     * @param string $path
     *
     * @return RedirectResponse
     */
    public function editByOtherUser($class, $path, $successPath)
    {
        $this->userDiscriminator->setClass($class);

        $this->controller->setContainer($this->container);
        $result = $this->controller->editByOtherUserAction($this->getRequest(), $successPath);
        if ($result instanceof RedirectResponse) {
            return $result;
        }

        $template = $this->userDiscriminator->getTemplate('profile');
        if (is_null($template)) {
            $template = 'FOSUserBundle:Profile:edit.html.twig';
        }

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $form = $this->formFactory->createForm(['user' => $user]);

        $type = substr($class, strrpos($class, "\\")+5);
        return $this->container->get('templating')->renderResponse($template, [
            'form' => $form->createView(),
            'path' => $path,
            'id' => $this->getRequest()->get('id', null),
            'userType' => $type
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Request;
     */
    private function getRequest()
    {
        return $this->container->get('request_stack')->getCurrentRequest();
    }
}
