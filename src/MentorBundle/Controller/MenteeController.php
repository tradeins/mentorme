<?php
namespace MentorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/mentee")
 * @author mgrabarczyk
 *
 */
class MenteeController extends Controller
{
    /**
     * @Route("/show_watcher/", name="mentee_show_my_watcher")
     */
    public function showMyWatcherAction()
    {
        $watcher = $this->getUser()->getMenteeWatcher();
        if ($watcher) {
            return $this->render('FOSUserBundle:Profile:show.html.twig', [
                'user' => $watcher
            ]);
        }
        return $this->container
            ->get('pugx_multi_user.registration_manager')
            ->register('MentorBundle\Entity\UserMenteeWatcher');
    }
}