<?php

namespace MentorBundle\Controller;

use MentorBundle\Entity\SpecializationArea;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Specializationarea controller.
 *
 * @Route("specializationarea")
 */
class SpecializationAreaController extends Controller
{
    /**
     * Lists all specializationArea entities.
     *
     * @Route("/", name="specializationarea_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:SpecializationArea')
        ->createQueryBuilder('e');

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
             $filterBuilder->getQuery(),
             $request->query->getInt('page', 1),
             10
        );

        return $this->render('MentorBundle:SpecializationArea:index.html.twig', array(
          'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new specializationArea entity.
     *
     * @Route("/new", name="specializationarea_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $specializationArea = new Specializationarea();
        $form = $this->createForm('MentorBundle\Form\SpecializationAreaType', $specializationArea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($specializationArea);
            $em->flush();

            return $this->redirectToRoute('specializationarea_show', array('id' => $specializationArea->getId()));
        }

        return $this->render('MentorBundle:SpecializationArea:new.html.twig', array(
            'specializationArea' => $specializationArea,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a specializationArea entity.
     *
     * @Route("/{id}", name="specializationarea_show")
     * @Method("GET")
     */
    public function showAction(SpecializationArea $specializationArea)
    {
        $deleteForm = $this->createDeleteForm($specializationArea);

        return $this->render('MentorBundle:SpecializationArea:show.html.twig', array(
            'specializationArea' => $specializationArea,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing specializationArea entity.
     *
     * @Route("/{id}/edit", name="specializationarea_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SpecializationArea $specializationArea)
    {
        $deleteForm = $this->createDeleteForm($specializationArea);
        $editForm = $this->createForm('MentorBundle\Form\SpecializationAreaType', $specializationArea);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('specializationarea_show', array('id' => $specializationArea->getId()));
        }

        return $this->render('MentorBundle:SpecializationArea:edit.html.twig', array(
            'specializationArea' => $specializationArea,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a specializationArea entity.
     *
     * @Route("/{id}", name="specializationarea_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, SpecializationArea $specializationArea)
    {
        $form = $this->createDeleteForm($specializationArea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($specializationArea);
            $em->flush();
        }

        return $this->redirectToRoute('specializationarea_index');
    }

    /**
     * Creates a form to delete a specializationArea entity.
     *
     * @param SpecializationArea $specializationArea The specializationArea entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SpecializationArea $specializationArea)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('specializationarea_delete', array('id' => $specializationArea->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
