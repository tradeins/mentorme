<?php

namespace MentorBundle\Controller;

use MentorBundle\Entity\Industry;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


/**
 * Industry controller.
 *
 * @Route("industry")
 */
class IndustryController extends Controller
{
    /**
     * Lists all industry entities.
     *
     * @Route("/", name="industry_index")
     * @Method("GET")
     * @param Request $request
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

       $filterBuilder = $this->get('doctrine.orm.entity_manager')
       ->getRepository('MentorBundle:Industry')
       ->createQueryBuilder('e');

//       $form = $this->get('form.factory')->create(CompanyFilterType::class);
//       if ($request->query->has($form->getName())) {
//           $form->submit($request->query->get($form->getName()));
//           $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
//       }

       $paginator  = $this->get('knp_paginator');
       $pagination = $paginator->paginate(
               $filterBuilder->getQuery(),
               $request->query->getInt('page', 1),
               10
       );

       return $this->render('MentorBundle:Industry:index.html.twig', array(
           'pagination' => $pagination,
//           'form' => $form->createView(),
       ));
    }

    /**
     * Creates a new industry entity.
     *
     * @Route("/new", name="industry_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $industry = new Industry();
        $form = $this->createForm('MentorBundle\Form\IndustryType', $industry);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($industry);
            $em->flush();

            return $this->redirectToRoute('industry_show', array('id' => $industry->getId()));
        }

        return $this->render('MentorBundle:Industry:new.html.twig', array(
            'industry' => $industry,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a industry entity.
     *
     * @Route("/{id}", name="industry_show")
     * @Method("GET")
     */
    public function showAction(Industry $industry)
    {
        $deleteForm = $this->createDeleteForm($industry);

        return $this->render('MentorBundle:Industry:show.html.twig', array(
            'industry' => $industry,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing industry entity.
     *
     * @Route("/{id}/edit", name="industry_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Industry $industry)
    {
        $deleteForm = $this->createDeleteForm($industry);
        $editForm = $this->createForm('MentorBundle\Form\IndustryType', $industry);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('industry_show', array('id' => $industry->getId()));
        }

        return $this->render('MentorBundle:Industry:edit.html.twig', array(
            'industry' => $industry,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a industry entity.
     *
     * @Route("/{id}", name="industry_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Industry $industry)
    {
        $form = $this->createDeleteForm($industry);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($industry);
            $em->flush();
        }

        return $this->redirectToRoute('industry_index');
    }

    /**
     * Creates a form to delete a industry entity.
     *
     * @param Industry $industry The industry entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Industry $industry)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('industry_delete', array('id' => $industry->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
