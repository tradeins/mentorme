<?php
namespace MentorBundle\Controller;

use MentorBundle\Entity\BusinessHours;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use MentorBundle\Form\Filter\MentorFilterType;
use MentorBundle\Form\Filter\Lists\MentorProcessFilterType;
use MentorBundle\Form\Filter\Lists\Sessions\MentorSessionFilterType;
use MentorBundle\Response\PdfResponse;

/**
 * @Route("/mentor")
 * @author mgrabarczyk
 *
 */
class MentorController extends Controller
{

    /**
     * @Route("/search/", name="search_mentor")
     */
    public function searchMentorAction(Request $request)
    {
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:UserMentor')
        ->createQueryBuilder('e')
        ->where('e.enabled = 1');
        
        $form = $this->get('form.factory')->create(MentorFilterType::class);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $filterBuilder->getQuery(),
                $request->query->getInt('page', 1),
                10
        );

        return $this->render('MentorBundle:Mentor:search.html.twig', array(
            'form' => $form->createView(),
            'pagination' => $pagination,
            'submited' => $form->isSubmitted()
        ));
    }
    
    /**
     * @Route("/sessions/", name="mentor_sessions")
     * @param Request $request
     */
    public function showSessionsAction(Request $request)
    {
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:MentorSession')
        ->createQueryBuilder('e');
        
        $filterBuilder->leftJoin('e.process', 'cp');
        $filterBuilder->andWhere('cp.mentor = :mentor')
        ->setParameter('mentor', $this->getUser());
        
        $form = $this->get('form.factory')->create(MentorSessionFilterType::class, null, ['mentor' => $this->getUser()]);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }
        
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $filterBuilder->getQuery(),
                $request->query->getInt('page', 1),
                10
        );
        
        return $this->render('MentorBundle:Mentor:list_sessions.html.twig', array(
          'pagination' => $pagination,
          'form' => $form->createView()
        ));
    }
    
    /**
     * @Route("/business/", name="mentor_business_hours")
     * @param Request $request
     */
    public function showBusinessHoursAction(Request $request) {
        return $this->render('MentorBundle:Mentor:business_hours.html.twig', [
          'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/business_create/", name="mentor_buisness_create")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function createBusinessHour(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        try {
            $hour = new BusinessHours();
            $startDate = new \DateTime($request->get('start'));
            $endDate = new \DateTime($request->get('end'));
            $hour->setOpen($startDate);
            $hour->setEnd($endDate);
            $hour->setMentor($this->getUser());
            $em->persist($hour);
            $em->flush();

            $url = $this->generateUrl('mentor_business_delete', ['id' => $hour->getId()]);
            $title = '<a href="'.$url.'" class="cal-del-link"> '.$this->get('translator')->trans('calendar.delete_business_hours').'</a>';
            return new JsonResponse([
              'title' => $title,
              'id' => $hour->getId(),
              'start' => $hour->getOpen(),
              'end' => $hour->getEnd(),
            ]);
        } catch (\Throwable $e) {
            return new JsonResponse(['msg' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
    
    /**
     * @Route("/business_all/", name="mentor_business_all")
     * @param Request $request
     * @return JsonResponse
     */
    public function getBusinessHours(Request $request) {
        $startDate = new \DateTime($request->get('start'));
        $startDate = $startDate->format('Y-m-d H:i:s');
        $endDate = new \DateTime($request->get('end'));
        $endDate = $endDate->format('Y-m-d H:i:s');
        $fetchedEvents = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:BusinessHours')
        ->findByMentor($this->getUser(), $startDate, $endDate);
        
        $events = [];
        foreach ($fetchedEvents as $eventData) {
            $url = $this->generateUrl('mentor_business_delete', ['id' => $eventData->getId()]);
            $title = '<a href="'.$url.'" class="cal-del-link"> '.$this->get('translator')->trans('calendar.delete_business_hours').'</a>';
            $events[] = [
                'id' => $eventData->getId(),
                'title' => $title,
                'start' => $eventData->getOpen()->format('Y-m-d H:i:s'),
                'end' => $eventData->getEnd()->format('Y-m-d H:i:s'),
                'stick' => true,
                'allDay' => false,
                'constraint' =>'businessHours',
                'backgroundColor'  => '#51c62d',
                'textColor' => 'black',
                'borderColor' => 'black',
                'overlap' => false,
                'editable' => true,
                'durationEditable' => true
            ];
        }
        
        return new JsonResponse($events);
    }

    /**
     * @Route("/business_change/{id}", name="mentor_business_change", defaults={"id"=null})
     * @Method({"POST"})
     * @param BusinessHours $hour
     * @param Request $request
     */
    public function changeBusinessTimeAction(BusinessHours $hour, Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $em->getConnection()->beginTransaction();
        try {
            $startDate = new \DateTime($request->get('startDate'));
            $endDate = new \DateTime($request->get('endDate'));
            $hour->setOpen($startDate);
            $hour->setEnd($endDate);
            $em->persist($hour);
            $em->flush();
            $em->getConnection()->commit();
            return new JsonResponse(['msg' => '']);//$this->get('translator')->trans('calendar.change.success')]);
        } catch (\Throwable $e) {
            $em->getConnection()->rollback();
            return new JsonResponse(['msg' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/business_delete/{id}", name="mentor_business_delete", defaults={"id"=null})
     * @Method({"GET"})
     * @param BusinessHours $hour
     * @return JsonResponse
     */
    public function deleteBusinessTime(BusinessHours $hour, Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $em->getConnection()->beginTransaction();
        try {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->remove($hour);
            $em->flush();
            $em->getConnection()->commit();
            return new JsonResponse(['msg' => 'deleted']);
        } catch (\Throwable $e) {
            $em->getConnection()->rollback();
            return new JsonResponse(['msg' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
    
    /**
     * @Route("/process/", name="mentor_process_list")
     */
    public function processMentorAction(Request $request)
    {
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:MentorProcess')
        ->createQueryBuilder('e')
        ->where('e.idMentor = '.$this->getUser()->getId());
    
        $form = $this->get('form.factory')->create(MentorProcessFilterType::class, null, ['mentor' => $this->getUser()]);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }
        
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $filterBuilder->getQuery(),
                $request->query->getInt('page', 1),
                10
        );
        return $this->render('MentorBundle:Mentor:list_process.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView(),
            'printQueryString' => $request->getQueryString(),
        ));
    }

    /**
     * @Route("/pdf/process/", name="mentor_pdf_process_list")
     */
    public function printPdfProcess(Request $request) {
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:MentorProcess')
        ->createQueryBuilder('e')
        ->where('e.idMentor = '.$this->getUser()->getId());

        $form = $this->get('form.factory')->create(MentorProcessFilterType::class, null, ['mentor' => $this->getUser()]);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $filterBuilder->getQuery(),
                $request->query->getInt('page', 1),
                10
        );
        $data = $form->getData();
        $filters = [];
        if (!empty($data)) {
            $filters = [
                'startDate' => !empty($data['startDate']['left_date']) ? $data['startDate']['left_date']->format('Y-m-d') : null,
                'endDate' => !empty($data['endDate']['right_date']) ? $data['endDate']['right_date']->format('Y-m-d') : null,
            ];
        }
        $html = $this->renderView('MentorBundle:Mentor:pdf_list_process.html.twig', [
            'pagination' => $pagination,
            'filters' => $filters,
        ]);
        $response = new PdfResponse($this->get("white_october.tcpdf"));
        $response->generatePdfResponseFromHtml('processList', $html, 'My Process List', 'horizontal');
    }
}