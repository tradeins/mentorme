<?php

namespace MentorBundle\Controller;

use MentorBundle\Entity\MentorProcessSummary;
use MentorBundle\Entity\MentorGoalCard;
use MentorBundle\Entity\UserMentor;
use MentorBundle\Entity\UserMentee;
use AppBundle\Entity\User;
use MentorBundle\Entity\MentorProcess;
use MentorBundle\Entity\MentorSession;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Notification;

/**
 * @Route("/event")
 * @author mgrabarczyk
 *
 */
class EventController extends Controller
{   
    /**
     * @Route("/create_appoitment/{sessionId}", name="event_create_appointment", defaults={"sessionId"=null})
     * @Method({"POST"})
     * @param Request $request
     */
    public function createAppointment(Request $request) 
    {
        $form = $this->createForm('MentorBundle\Form\MentorProcessType', new MentorProcess());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $client = $user->getClient();
            $em =  $this->get('doctrine.orm.entity_manager');
            $clientSessionLimit = $client->getHoursLimit();
            $countedSessions = $em->getRepository('MentorBundle:MentorProcess')
            ->countSessionsByClientId($client->getId());
            $data = $form->getData();
            
            if (($countedSessions + $data->getSessions()) >= $clientSessionLimit) {
                $msg = $this->get('translator')->trans('exception.session.reached.limit');
                return new JsonResponse(['msg' => $msg], Response::HTTP_BAD_REQUEST);
            }
            $em->getConnection()->beginTransaction();
            try {
                if (isset($sessionId)) {
                    $session = $em->getRepository('MentorBundle:MentorSession')->findById($sessionId);
                    if ($session->getId() == null) {
                        $session = new MentorSession();
                    }
                } else {
                    $session = new MentorSession();
                }

                $session->setStartDate($data->getStartDate());
                $session->setEndDate($data->getEndDate());
                $session->setCreatedDate(new \DateTime('now'));
                $session->setAcceptByMentee(1);
                
                $mentor = $this->getDoctrine()
                ->getRepository(UserMentor::class)
                ->find($data->getIdMentor());
                
                $mentorProcess = $this->getMentorProcess($user, $mentor);
                if ($mentorProcess->getId() == null){ 
                    $mentorProcess->setStartDate($data->getStartDate());
                    $mentorProcess->setEndDate($data->getEndDate());
                    $mentorProcess->setSessions($data->getSessions());
                } else {
                    $mentorProcess->setSessionsPlanned($mentorProcess->getSessionsPlanned()+1);
                }
                if ($mentorProcess->getEndDate() < $data->getEndDate()) {
                    $mentorProcess->setEndDate($data->getEndDate());
                }
                $mentorProcess->addSession($session);
                   
                $em->persist($mentorProcess);
                $em->flush();
                $renderedTemplate = $this->renderView('MentorBundle:Session:email_accept_session.txt.twig', array(
                  'user' => $mentor,
                  'acceptUrl' => $this->generateUrl('session_accept', [
                      'id' => $session->getId()
                  ], 0),
                  'mentee' => $this->getUser(),
                  'session' => $session,
                  'processUrl' => $this->generateUrl('process_show', ['id' => $mentorProcess->getId()], 0)
                ));
                
                $renderedLines = explode("\n", trim($renderedTemplate));
                $subject = array_shift($renderedLines);
                $body = implode("\n", $renderedLines);
                
                $message = (new \Swift_Message())
                ->setSubject($subject)
                ->setFrom('no_replay@mentorme.com.pl')
                ->setTo($mentor->getEmail())
                ->setBody($body);
                
                $this->get('mailer')->send($message);
                
                $notice = new Notification();
                $notice->setEntityType(Notification::TYPE_SESSION_ACCEPT)
                ->setEntityId($session->getId())
                ->setDescription('notice.session.to.accept')
                ->setUser($mentor);
                $em->persist($notice);
                $em->flush();
                $em->getConnection()->commit();
                return new JsonResponse([
                        'msg' => $this->get('translator')->trans('session.reservation.completed'),
                        'startDate' => $data->getStartDate(),
                        'endDate' => $data->getEndDate(),
                        'user' => $this->getUser()->getFullName()
                ]);
            } catch (\Throwable $e) {
                $em->getConnection()->rollBack();
                return new JsonResponse(['msg' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
            }
        }
        
        return new JsonResponse(['msg' => 'Form is invalid'], Response::HTTP_BAD_REQUEST);
    }
    
    private function getMentorProcess($user, $mentor) {
        $process = $this->getDoctrine()
        ->getRepository(MentorProcess::class)
        ->findOneBy([
             'mentee' => $user,
             'mentor' => $mentor,
             'status' => [MentorProcess::STATUS_INACTIVE, MentorProcess::STATUS_UNREALIZED],
           ],
           ['id' => 'DESC']
        );
        
        if (!$process) {
            $process = new MentorProcess();
            $process->addGoalCard(new MentorGoalCard());
            $process->addSummary(new MentorProcessSummary());
            $process->setMentor($mentor);
            $process->setMentee($user);
            $process->setStatus(MentorProcess::STATUS_INACTIVE);
            $process->setBlockedForClient(0);
            $process->setSessionsPlanned(1);
        } else if ($process->getSessionsLeftToPlanned() == 0 && $process->getSessionsLeft() > 0) {
            $msg = $this->get('translator')->trans('exception.process.not.finished');
            throw new \Exception($msg);
        }
        return $process;
    }

    /**
     * @Route("/check_exits/{userId},{mentorId}", name="event_check_process", defaults={"userId"=null,"mentorId"=null})
     * @Method({"POST"})
     * @param UserMentee $user
     * @param UserMentor $mentor
     */
    public function checkIfProcessExsists(UserMentee $userId, UserMentor $mentorId) {
        $process = $this->getDoctrine()
        ->getRepository(MentorProcess::class)
        ->findOneBy([
             'mentee' => $userId,
             'mentor' => $mentorId,
             'status' => [MentorProcess::STATUS_INACTIVE, MentorProcess::STATUS_UNREALIZED],
           ],
           ['id' => 'DESC']
        );
        return new JsonResponse(['status' => $process ? 'true' : 'false']);
    }

    /**
     * @Route("/get_all", name="event_get_all")
     * @param Request $request
     */
    public function getCalendarEvents(Request $request) 
    {
        $mentorId = $request->get('mentorId');
        $startDate = new \DateTime($request->get('start'));
        $startDate = $startDate->format('Y-m-d H:i:s');
        $endDate = new \DateTime($request->get('end'));
        $endDate = $endDate->format('Y-m-d H:i:s');
        $fetchedEvents = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:MentorSession')
        ->findBySessionsForUser($mentorId, $startDate, $endDate);
        
        $events = [];
        $loggedUser = $this->getUser();
        foreach ($fetchedEvents as $eventData) {
            $events[] = $this->processEvent($eventData, $loggedUser);
        }
        
        return new JsonResponse($events);
    }
    
    private function processEvent($eventData, $loggedUser) {
        $translator = $this->get('translator');
        if ($loggedUser->getId() != $eventData['idMentee'] && $loggedUser->getId() != $eventData['idMentor']) {
            $color = '#ccdfff';
            $editable = false;
            $title = $translator->trans('calendar.occupied');
        } else {
            $url = $this->generateUrl('process_show', ['id' => $eventData['idProcess']]);
            $color = '#92E88E';
            $showLinkToAccept = false;
            if ($loggedUser->getId() == $eventData['idMentee']) {
                $userName = $loggedUser->getFullName();
                if (!$eventData['acceptByMentee']) {
                    $showLinkToAccept = true;
                }
            } else if ($loggedUser->getId() == $eventData['idMentor']) {
                $mentee = $this->get('fos_user.user_manager')->findUserBy(['id' => $eventData['idMentee']]);
                $userName = $mentee->getFullName();
                if (!$eventData['acceptByMentor']) {
                    $showLinkToAccept = true;
                }
            }
            $title = $userName . '<br><a href="'.$url.'">'.$translator->trans('calendar.show_process').'</a>';
            if ($showLinkToAccept && ($eventData['status'] == MentorSession::STATUS_TO_ACCEPT)) {
               $color = '#EE575A';
               $title .= '<br><a href="'.$this->generateUrl('session_accept', ['id' => $eventData['id']]).'">'.$translator->trans('session.form_accept.accept').'</a>';
            }
            if ($eventData['status'] == MentorSession::STATUS_TO_MOVE) {
                $title = $translator->trans('session.calendar.move.title').'<br>' . $title .'<br>'.$translator->trans('session.calendar.move.annotation');
                $color = '#f49542';
            }
            $date = new \DateTime();
            $interval = $date->diff($eventData['startDate']);
            $editable = ($interval->d >= 2);
        }
        $event = [
          'id' => $eventData['id'],
          'title' => $title,
          'start' => $eventData['startDate']->format('Y-m-d H:i:s'),
          'end' => $eventData['endDate']->format('Y-m-d H:i:s'),
          'stick' => true,
          'allDay' => false,
          'constraint' =>'businessHours',
          'backgroundColor'  => $color,
          'textColor' => 'black',
          'borderColor' => 'black',
          'overlap' => false,
          'editable' => $editable,
          'durationEditable' => $editable
        ];
        
        return $event;
    }
    

    /**
     * @Route("/change_time/{id}", name="event_change_date", defaults={"id"=null})
     * @Method({"POST"})
     * @param MentorSession $session
     * @param Request $request
     */
    
    public function changeEventTimeAction(MentorSession $session, Request $request) {
        $em =  $this->get('doctrine.orm.entity_manager');
        $em->getConnection()->beginTransaction();
        try {
            $process = $session->getProcess();
            if ($process->isClosed()) {
                throw new Exception('Process already closed');
            }
            $startDate = new \DateTime($request->get('startDate'));
            $endDate = new \DateTime($request->get('endDate'));
            $session->setStartDate($startDate);
            $session->setEndDate($endDate);
            $session->setStatus(MentorSession::STATUS_TO_ACCEPT);

            $interval = $session->getEndDate()->diff($process->getEndDate());
            if ($interval->invert == 1) {
                $process->setEndDate($endDate);
            }
            $userType = $this->getUser()->getEntityType();
            switch ($userType) {
                case 'user_mentee':
                    $notifyUser = $session->getProcess()->getMentor();
                    $session->setAcceptByMentee(1);
                    $session->setAcceptByMentor(0);
                    break;
                case 'user_mentor':
                    $notifyUser = $session->getProcess()->getMentee();
                    $session->setAcceptByMentee(0);
                    $session->setAcceptByMentor(1);
                    break;
            }
            $notice = new Notification();
            $notice->setEntityType(Notification::TYPE_SESSION_ACCEPT)
                ->setEntityId($session->getId())
                ->setDescription('notice.session.changed.to.accept')
                ->setUser($notifyUser);
            $em->persist($notice);

            $emailData = [
                  'mentor' => $session->getProcess()->getMentor(),
                  'acceptUrl' => $this->generateUrl('session_accept', [
                      'id' => $session->getId()
                  ], 0),
                  'mentee' => $session->getProcess()->getMentee(),
                  'session' => $session,
                  'processUrl' => $this->generateUrl('process_show', ['id' => $session->getProcess()->getId()], 0)
            ];
            $renderedTemplate = $this->renderView('MentorBundle:Session:email_changed_date.html.twig', $emailData);
            $renderedLines = explode("\n", trim($renderedTemplate));
            $subject = array_shift($renderedLines);
            $body = implode("\n", $renderedLines);

            $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom('no_replay@mentorme.com.pl')
            ->setTo($notifyUser->getEmail())
            ->setBody($body, 'text/html');

            $this->get('mailer')->send($message);

            $em->persist($process);
            $em->persist($session);
            $em->flush();
            $em->getConnection()->commit();
            return new JsonResponse(['msg' => $this->get('translator')->trans('calendar.change.success')]);
        } catch (\Throwable $e) {
            $em->getConnection()->rollback();
            return new JsonResponse(['msg' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}