<?php

namespace MentorBundle\Controller;

use AppBundle\Entity\User;
use MentorBundle\MentorBundle;
use MentorBundle\Entity\UserMentor;
use MentorBundle\Entity\UserMentee;
use MentorBundle\Entity\MentorSession;
use MentorBundle\Entity\MentorProcess;
use AppBundle\Entity\Notification;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MentorBundle\Form\Filter\Lists\SessionRaportFilterType;
use MentorBundle\Response\PdfResponse;

/**
 * @Route("/session");
 * @author mgrabarczyk
 *
 */
class SessionController extends Controller
{
    
    /**
     * @Route("/acceptlink/{token}/{userType}/{userId}", name="session_accept_link",
         requirements={
           "userId":"\d+", 
           "userType":"ce|ch",
           "token":"\w{16}"
       })
     * @param MentorSession $session
     * @param Request $request
     */
    public function acceptSessionFromEmailAction(Request $request) {
        $userId = $request->get('userId', null);
        $userType = $request->get('userType', null);
        $token = $request->get('token', null);
        
        $session = $this->getDoctrine()
        ->getRepository(MentorSession::class)
        ->findByUserAndToken($userType, $userId, $token);
        
        $url = $this->generateUrl('dashboard');
        if (empty($session)) {
            $this->addFlash('error', 'session.accept.not.found');
            return new RedirectResponse($url);
        }
        switch ($userType) {
            case 'ce':
                $session->setAcceptByMentee(1);
                break;
            case 'ch':
                $session->setAcceptByMentor(1);
                break;
        }
        $session->setAcceptToken(null);
        $session->setStatus(MentorSession::STATUS_TO_REALIZATION);
        $em = $this->getDoctrine()->getManager();
        $em->persist($session);
        $em->flush();
        $this->addFlash('success', 'session.accept.success');
        return new RedirectResponse($url);
    }
    
    /**
     * @Route("/accept/{id}", name="session_accept",
         requirements={
           "id":"\d+"
       })
     * @param Request $request
     * @param MentorSession $session
     */
    public function acceptSessionAction(Request $request, MentorSession $session) {
        if ($session->getProcess()->isClosed()) {
            $this->addFlash(
                'warning',
                'flash.process.closed'
            );
            return $this->redirect($this->generateUrl('process_show', ['id' => $session->getProcess()->getId()]));
        }
        if ($session->getStatus() != MentorSession::STATUS_TO_ACCEPT) {
            $this->addFlash(
                'warning',
                'flash.session.already.accepted'
            );
            return $this->redirect($this->generateUrl('process_show', ['id' => $session->getProcess()->getId()]));
        }
        $form = $this->createForm('MentorBundle\Form\MentorSessionAcceptType', $session);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $action = $data->getAction(); 
            if ($action == 'reject') {
                $session->setStatus(MentorSession::STATUS_TO_MOVE);
                $session->setAcceptByMentor(0);
                $session->setAcceptByMentee(0);
                $session->setAcceptToken(null);
                $userType = $this->getUser()->getEntityType();
                switch ($userType) {
                    case 'user_mentee':
                        $notifyUser = $session->getProcess()->getMentor();
                        break;
                    case 'user_mentor':
                        $notifyUser = $session->getProcess()->getMentee();
                        break;
                }
                $em = $this->getDoctrine()->getManager();

                $notice = new Notification();
                $notice->setEntityType(Notification::TYPE_SESSION_ACCEPT)
                    ->setEntityId($session->getId())
                    ->setDescription('notice.session.changed.to.accept')
                    ->setUser($notifyUser);

                $emailData = [
                    'session' => $session,
                    'reason' => $data->getRejectReason(),
                    'mentor' => $session->getProcess()->getMentor(),
                    'mentee' => $session->getProcess()->getMentee(),
                    'processUrl' => $this->generateUrl('process_show', ['id' => $session->getProcess()->getId()], 0)
                ];
                $renderedTemplate = $this->renderView('MentorBundle:Session:email_rejected_session.html.twig', $emailData);
                $renderedLines = explode("\n", trim($renderedTemplate));
                $subject = array_shift($renderedLines);
                $body = implode("\n", $renderedLines);

                $message = (new \Swift_Message())
                ->setSubject($subject)
                ->setFrom('no_replay@mentorme.com.pl')
                ->setTo($notifyUser->getEmail())
                ->setBody($body, 'text/html');

                $this->get('mailer')->send($message);

                $em->persist($notice);
                $em->persist($session);
                $em->flush();
                $this->addFlash('info', 'flash.session.choose_new_date');
                return $this->redirectToRoute('user_profile_view', ['id' => $session->getProcess()->getMentor()->getId(), 'sid' => $session->getId()]);
            }
            if ($action == 'accept') {
                $userType = $this->getUser()->getEntityType();
                switch ($userType) {
                    case 'user_mentee': 
                        $session->setAcceptByMentee(1);
                        break;
                    case 'user_mentor':
                        $session->setAcceptByMentor(1);
                        break;
                }
                if ($session->isBothAccepted()) {
                    $session->setRejectReason(null);
                    $session->setStatus(MentorSession::STATUS_TO_REALIZATION);
                }
                $session->setAcceptToken(null);
                $em = $this->getDoctrine()->getManager();
                $em->persist($session);
                $em->flush();

                $notifications = $em->getRepository('AppBundle:Notification')->findBy([
                    'entityId' => $session->getId(),
                    'user' => $this->getUser(),
                    'entityType' => Notification::TYPE_SESSION_ACCEPT
                ]);
                if (!empty($notifications)) {
                    $notifications = is_array($notifications) ? $notifications : [$notifications];
                    foreach ($notifications as $notice) {
                        $em->remove($notice);
                        $em->flush();
                    }
                }
            }
        
            return $this->redirectToRoute('process_show', ['id' => $session->getProcess()->getId()]);
        }
        
        return $this->render('MentorBundle:Session:accept_form.html.twig', array(
          'form' => $form->createView(),
          'session' => $session,
        ));
    }
    
    /**
     * @Route("/confirm_realization/{id}", name="session_confirm_realization", defaults={"id"=null})
     * @Method({"POST"})
     * @param Request $request
     * @param MentorSession $session
     */
    public function confirmRealizationAction(Request $request, MentorSession $session)
    {
        try {
            $type = $request->get('type', null);
            $user = $this->getUser();
            $process = $session->getProcess();
            if ($process->isClosed()) {
                return new JsonResponse(['msg' => 'Process is already closed'], Response::HTTP_NOT_FOUND);
            }
            if ($user->getEntityType() != $type) {
                return new JsonResponse(['msg' => 'Fail usertype'], Response::HTTP_NOT_FOUND);
            }
            switch ($type) {
                case 'user_mentor':
                    if ($process->getMentor()->getId() != $user->getId()) {
                        return new JsonResponse(['msg' => 'Fail'], Response::HTTP_NOT_FOUND);
                    }
                    $session->setConfirmedMentor(true);
                    break;
                case 'user_mentee':
                    if ($process->getMentee()->getId() != $user->getId()) {
                        return new JsonResponse(['msg' => 'Fail'], Response::HTTP_NOT_FOUND);
                    }
                    $session->setConfirmedMentee(true);
                    break;
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($session);
            $em->flush();
            if ($session->isBothConfirmed()) {
                $process->setSessionsRealized($process->getSessionsRealized()+1);
                if ($process->getSessionsLeft() == 0) {
                    $process->setStatus(MentorProcess::STATUS_REALIZED);
                }
                $em->persist($process);
                $em->flush();
            }
            
            $notifications = $em->getRepository('AppBundle:Notification')->findBy([
                    'entityId' => $process->getId(), 
                    'user' => $user,
                    'entityType' => Notification::TYPE_SESSION_CONFIRM_REALIZED
            ]);
            $response = new JsonResponse([
                'msg' => 'Confirmed',
                'bothConfirmed' => $session->isBothConfirmed(),
            ]);
            if (empty($notifications)) {
                return $response;
            }
            $notifications = is_array($notifications) ? $notifications : [$notifications];
            foreach ($notifications as $notice) {
                $em->remove($notice);
                $em->flush();
            }
            return $response;
        } catch (\Exception $e) {
            return new JsonResponse(['msg' => 'Confirmed'], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/raport/", name="summary_raport")
     */
    public function generateSummaryRaportAction(Request $request)
    {
        $repository = $this->get('doctrine.orm.entity_manager')
                    ->getRepository('MentorBundle:MentorSession');
        $filterBuilder = $repository->getRaportQuery();

        $user = $this->getUser();
        $mentor = null;
        $showCompany = true;
        $showMentee = true;
        $client = null;
        switch ($user->getEntityType()) {
            case 'user_client':
                $filterBuilder->andWhere('mt.client = :client')
                    ->setParameter('client', $user->getId());
                $showCompany = false;
                $client = $user;
                break;
            case 'user_mentor':
                $filterBuilder->andWhere('mp.mentor = :mentor')
                    ->setParameter('mentor', $user->getId());
                $mentor = $user;
                break;
            case 'user_mentee':
                $filterBuilder->andWhere('mp.mentee = :mentee')
                    ->setParameter('mentee', $user->getId());
                $showCompany = false;
                $showMentee = false;
                break;
        }

        $form = $this->get('form.factory')->create(SessionRaportFilterType::class, null, [
            'mentor' => $mentor,
            'showCompany' => $showCompany,
            'showMentee' => $showMentee,
            'client' => $client,
        ]);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }

        $query = $filterBuilder->getQuery();
        $query->setHint('knp_paginator.count', $repository->getCountRaportQuery($query));

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('MentorBundle:Session:raport.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView(),
            'printQueryString' => $request->getQueryString(),
        ]);
    }

    /**
     * @Route("/pdf/raport/", name="sessions_pdf_raport")
     */
    public function generatePdfSummaryRaportAction(Request $request)
    {
        $repository = $this->get('doctrine.orm.entity_manager')
                ->getRepository('MentorBundle:MentorSession');
        $filterBuilder = $repository->getRaportQuery();

        $user = $this->getUser();
        $mentor = null;
        $showCompany = true;
        $showMentee = true;
        $client = null;
        switch ($user->getEntityType()) {
            case 'user_client':
                $filterBuilder->andWhere('mt.client = :client')
                    ->setParameter('client', $user->getId());
                $showCompany = false;
                $client = $user;
                break;
            case 'user_mentor':
                $filterBuilder->andWhere('mp.mentor = :mentor')
                    ->setParameter('mentor', $user->getId());
                $mentor = $user;
                break;
            case 'user_mentee':
                $filterBuilder->andWhere('mp.mentee = :mentee')
                    ->setParameter('mentee', $user->getId());
                $showCompany = false;
                $showMentee = false;
                break;
        }

        $form = $this->get('form.factory')->create(SessionRaportFilterType::class, null, [
            'mentor' => $mentor,
            'showCompany' => $showCompany,
            'showMentee' => $showMentee,
            'client' => $client,
        ]);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }

        $query = $filterBuilder->getQuery();
        $query->setHint('knp_paginator.count', $repository->getCountRaportQuery($query));

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );
        $data = $form->getData();
        $filters = [];
        if (!empty($data)) {
            $filters = [
                'startDate' => !empty($data['startDate']['left_date']) ? $data['startDate']['left_date']->format('Y-m-d') : null,
                'endDate' => !empty($data['endDate']['right_date']) ? $data['endDate']['right_date']->format('Y-m-d') : null,
            ];
        }
        $html = $this->renderView('MentorBundle:Session:pdf_raport.html.twig', [
            'pagination' => $pagination,
            'filters' => $filters,
        ]);
        $response = new PdfResponse($this->get("white_october.tcpdf"));
        $response->generatePdfResponseFromHtml('raportSessions', $html, 'Sessions Raport', 'horizontal');
    }

}