<?php

namespace MentorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use MentorBundle\Form\Filter\Lists\UserFilterType;
use MentorBundle\Form\Filter\Lists\ClientProcessFilterType;
use MentorBundle\Form\Filter\Lists\Sessions\ClientSessionFilterType;
use MentorBundle\Response\PdfResponse;


/**
 * @Route("/client")
 */
class ClientController extends Controller
{

    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('MentorBundle:Default:index.html.twig', array(
        ));
    }
    
    /**
     * @Route("/mentee_list/", name="clients_mentee_list")
     */
    public function showMyMenteeAction(Request $request) 
    {
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:UserMentee')
        ->createQueryBuilder('e');
        
        $filterBuilder->where('e.client = :client')
        ->setParameter('client', $this->getUser());
        
        $form = $this->get('form.factory')->create(UserFilterType::class, null, ['show_company' => false]);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }
        
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $filterBuilder->getQuery(),
                $request->query->getInt('page', 1),
                10
        );
        
        return $this->render('MentorBundle:Admin:list_mentee.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/process/", name="clients_process_list")
     */
    public function processMenteeAction(Request $request)
    {
        $userId = $this->getUser()->getId();
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:MentorProcess')
        ->findMyMenteeProcessesQuery($userId, 'client');
    
        $form = $this->get('form.factory')->create(ClientProcessFilterType::class, null, ['clientId' => $userId]);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }
        
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $filterBuilder->getQuery(),
                $request->query->getInt('page', 1),
                10
        );
        return $this->render('MentorBundle:Watcher:list_process.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView(),
            'printQueryString' => $request->getQueryString(),
        ));
    }

    /**
     * @Route("/pdf/process/", name="client_pdf_process_list")
     */
    public function printPdfProcess(Request $request) {
        $userId = $this->getUser()->getId();
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:MentorProcess')
        ->findMyMenteeProcessesQuery($userId, 'client');

        $form = $this->get('form.factory')->create(ClientProcessFilterType::class, null, ['clientId' => $userId]);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $filterBuilder->getQuery(),
                $request->query->getInt('page', 1),
                10
        );

        $data = $form->getData();
        $filters = [];
        if (!empty($data)) {
            $filters = [
                'startDate' => !empty($data['startDate']['left_date']) ? $data['startDate']['left_date']->format('Y-m-d') : null,
                'endDate' => !empty($data['endDate']['right_date']) ? $data['endDate']['right_date']->format('Y-m-d') : null,
            ];
        }

        $html = $this->renderView('MentorBundle:Watcher:pdf_list_process.html.twig', [
            'pagination' => $pagination,
            'filters' => $filters,
        ]);

        $response = new PdfResponse($this->get("white_october.tcpdf"));
        $response->generatePdfResponseFromHtml('processList', $html, 'My Process List', 'horizontal');
    }
    
    /**
     * @Route("/sessions/", name="clients_sessions")
     * @param Request $request
     */
    public function showMyMenteeSessionsAction(Request $request)
    {
        $filterBuilder = $this->get('doctrine.orm.entity_manager')
        ->getRepository('MentorBundle:MentorSession')
        ->createQueryBuilder('e');
        
        $filterBuilder->leftJoin('e.process', 'cp');
        $filterBuilder->leftJoin('cp.mentee', 'ce');
        $filterBuilder->andWhere('ce.client = :client')
        ->setParameter('client', $this->getUser());
        
        $form = $this->get('form.factory')->create(ClientSessionFilterType::class, null, ['client' => $this->getUser()]);
        if ($request->query->has($form->getName())) {
            $form->submit($request->query->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);
        }
        
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $filterBuilder->getQuery(),
                $request->query->getInt('page', 1),
                10
        );
        
        return $this->render('MentorBundle:Admin:list_sessions.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView()
        ));
    }
    
    /**
     * @Route("/change_status/{id}", name="change_status", requirements={"id"="\d+"})
     * @param Request $request
     * @param User $user
     */
    public function changeUserStatus(Request $request, User $user)
    {
        if (!$user->isEnabled() && in_array($user->getEntityType(), ['user_mentee', 'user_mentee_watcher'])) {
            $role = '';
            switch ($user->getEntityType()) {
                case 'user_mentee_watcher':
                    $userTxt = 'mentee_watcher';
                    $role = 'ROLE_MENTEE_WATCHER';
                    break;
                case 'user_mentee':
                    $userTxt = 'mentore';
                    $role = 'ROLE_MENTEE';
                    break;
            }
            if ($role != '' && $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
                $userManager = $this->get('fos_user.user_manager');
                $count = $userManager->countActiveUsersByRole($this->getUser()->getId(), $role);
                if ($count[0]['co'] >= $this->getUser()->getMenteeLimit()) {
                    $this->addFlash(
                            'warning',
                            'flash.exceeded_limit.'.$userTxt.'.activate'
                    );
                    return $this->redirect($request->headers->get('referer'));
                }
            }
        }
        
        $user->setEnabled(!$user->isEnabled());
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return $this->redirect( $request
                   ->headers
                   ->get('referer'));
    }
}
