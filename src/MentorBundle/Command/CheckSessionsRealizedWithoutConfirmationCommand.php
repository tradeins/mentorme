<?php 
namespace MentorBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Entity\Notification;

class CheckSessionsRealizedWithoutConfirmationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
        ->setName('mentor:session:check-realized-without-confirmation')
        ->setDescription('Checks sessions waiting to confirm')
        ->setHelp('This command checks sessions waiting to confirm and sending an notification to user');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new \DateTime();
        $endDate = $date->sub(new \DateInterval('P1D'))->format('Y-m-d H:i:s');
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getManager();
        $sessions = $em
        ->getRepository('MentorBundle:MentorSession')
        ->findRealizedWithoutConfirmation($endDate);
        
        foreach ($sessions as $session) {
            if ($session['confirmedMentee'] == 0) {
                $this->addNotification($session['idMentee'], $session['idProcess']);
            }
            if ($session['confirmedMentor'] == 0) {
                $this->addNotification($session['idMentor'], $session['idProcess']);
            }
        }
    }

    protected function addNotification($userId, $idProcess) {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getManager();
        $user = $container->get('fos_user.user_manager')->findUserBy(['id' => $userId]);
        $notification = $em->getRepository('AppBundle:Notification')->findOneBy([
            'entityId' => $idProcess,
            'user' => $user,
        ]);
        if ($notification) {
            return;
        }
        $notice = new Notification();
        $notice->setEntityType(Notification::TYPE_SESSION_CONFIRM_REALIZED)
        ->setEntityId($idProcess)
        ->setDescription('notice.session.to.confirm')
        ->setUser($user);
        $user->addNotification($notice);
        $em->persist($notice);
        $em->flush();
    }
}