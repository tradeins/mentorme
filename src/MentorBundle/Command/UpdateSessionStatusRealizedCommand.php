<?php 
namespace MentorBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Entity\Notification;
use MentorBundle\Entity\MentorSession;
use MentorBundle\Entity\MentorProcess;

class UpdateSessionStatusRealizedCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
        ->setName('mentor:session:update-status-realized')
        ->setDescription('Checks sessions that were realized')
        ->setHelp('This command checks sessions that end date was in past, and status is still waiting to realization and update status to realized');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getManager();
        $date = new \DateTime();
        $endDate = $date->sub(new \DateInterval('PT5M'))->format('Y-m-d H:i:s');
        
//        $qb = $em->createQueryBuilder();
//        $q = $qb->update('MentorBundle:MentorSession', 'cs')
//        ->set('cs.status', MentorSession::STATUS_REALIZED)
//        ->where('cs.status = :status and cs.endDate < :endDate')
//        ->setParameter('status', MentorSession::STATUS_TO_REALIZATION)
//        ->setParameter('endDate', $endDate)
//        ->getQuery();
//        echo $q->getSQL();
//        $p = $q->execute();

        $rawSql = "UPDATE mentor_session 
        INNER JOIN mentor_process ON mentor_process.id = mentor_session.id_process
        SET mentor_session.status = :statusUpdate
        WHERE mentor_session.status = :status 
            AND mentor_session.end_date < :endDate 
            AND mentor_process.status != :processStatus";
        $statement = $em->getConnection()->prepare($rawSql);
        $params = [
            ':statusUpdate' => MentorSession::STATUS_TO_CONFIRM,
            ':status' => MentorSession::STATUS_TO_REALIZATION,
            ':endDate' => $endDate,
            ':processStatus' => MentorProcess::STATUS_CLOSED,
        ];
        $statement->execute($params);

    }
}