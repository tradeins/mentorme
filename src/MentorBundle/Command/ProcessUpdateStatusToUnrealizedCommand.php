<?php 
namespace MentorBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Entity\Notification;
use MentorBundle\Entity\MentorProcess;

class ProcessUpdateStatusToUnrealizedCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
        ->setName('mentor:process:update-status-to-realization')
        ->setDescription('Checks process waiting to start realization')
        ->setHelp('This command checks process that are less then 2 days to realization');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getManager();
        $date = new \DateTime();
        $startDate = $date->add(new \DateInterval('P2D'))->format('Y-m-d');
        $startDateEnd = $date->add(new \DateInterval('P1D'))->format('Y-m-d');
        $qb = $em->createQueryBuilder();
        $q = $qb->update('MentorBundle:MentorProcess', 'cs')
        ->set('cs.status', MentorProcess::STATUS_UNREALIZED)
        ->where('cs.status = :status and cs.startDate > :startDate and cs.startDate < :startDateEnd')
        ->setParameters([
                'startDate' => $startDate,
                'startDateEnd' => $startDateEnd,
                'status' => MentorProcess::STATUS_INACTIVE
        ])
        ->getQuery();
        $p = $q->execute();
    }
}