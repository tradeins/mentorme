<?php 
namespace MentorBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Entity\Notification;
use MentorBundle\Entity\MentorSession;
use MentorBundle\Entity\MentorProcess;

class UpdateSessionStatusToRealizationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
        ->setName('mentor:session:update-status-to-realization')
        ->setDescription('Checks sessions waiting to confirm realization')
        ->setHelp('This command checks sessions that are less then 2 days to realization');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getManager();
        $date = new \DateTime();
        $startDate = $date->add(new \DateInterval('P2D'))->format('Y-m-d');
        $startDateEnd = $date->add(new \DateInterval('P1D'))->format('Y-m-d');
        // change status to realization for sessions that are less than 2 days to realization
//        $qb = $em->createQueryBuilder();
//        $q = $qb->update('MentorBundle:MentorSession', 'cs')
//        ->set('cs.status', MentorSession::STATUS_TO_REALIZATION)
//        ->where('cs.status = :status and cs.startDate > :startDate and cs.startDate < :startDateEnd')
//        ->setParameters([
//                'startDate' => $startDate,
//                'startDateEnd' => $startDateEnd,
//                'status' => MentorSession::STATUS_TO_ACCEPT,
//        ])
//        ->getQuery();
        $rawSql = "UPDATE mentor_session 
        INNER JOIN mentor_process ON mentor_process.id = mentor_session.id_process
        SET mentor_session.status = :statusUpdate
        WHERE mentor_session.status = :status 
            AND mentor_session.start_date > :startDate 
            AND mentor_session.start_date < :startDateEnd 
            AND mentor_process.status != :processStatus";
        $statement = $em->getConnection()->prepare($rawSql);
        $params = [
            ':statusUpdate' => MentorSession::STATUS_TO_REALIZATION,
            ':status' => MentorSession::STATUS_TO_ACCEPT,
            ':startDate' => $startDate,
            ':startDateEnd' => $startDateEnd,
            ':processStatus' => MentorProcess::STATUS_CLOSED,
        ];
        $statement->execute($params);
//        $p = $q->execute();
    }
}