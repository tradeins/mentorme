<?php

namespace MentorBundle\EventListener;

use FOS\UserBundle\EventListener\AuthenticationListener;
use FOS\UserBundle\FOSUserEvents;
class UserAuthenticationListener extends AuthenticationListener
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
        //             FOSUserEvents::REGISTRATION_COMPLETED => 'authenticate',
        //             FOSUserEvents::REGISTRATION_CONFIRMED => 'authenticate',
            FOSUserEvents::RESETTING_RESET_COMPLETED => 'authenticate',
        );
    }
}