<?php
namespace MentorBundle\EventListener;

use AppBundle\AppBundle;

use Avanzu\AdminThemeBundle\Event\ShowUserEvent;
use Avanzu\AdminThemeBundle\Model\NavBarUserLink;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserListener {

    private $container;
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
	public function onShowUser(ShowUserEvent $event) {

		$user = $this->getUser();
		$event->setUser($user);
		$event->setShowProfileLink(true);
	}

	protected function getUser() {
	    return $this->container->get('security.token_storage')->getToken()->getUser();
	}

}