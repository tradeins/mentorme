<?php
namespace MentorBundle\EventListener;

use Avanzu\AdminThemeBundle\Model\MenuItemModel;
use Avanzu\AdminThemeBundle\Event\SidebarMenuEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class MentorMenuListener {
    
    private $authChecker;
    
    public function __construct(AuthorizationCheckerInterface $authChecker)
    {
        $this->authChecker = $authChecker;
    }
    
    public function onSetupMenu(SidebarMenuEvent $event) {
    
        $request = $event->getRequest();
    
        foreach ($this->getMenu($request) as $item) {
            $event->addItem($item);
        }
    
    }
    
    protected function getMenu(Request $request) {
        $menuItems = [];
        if ($this->authChecker->isGranted('ROLE_CLIENT')) {
            $client = new MenuItemModel('ClientId', 'mentor.client', 'dashboard', [], 'iconclasses fa fa-user');
            $client->addChild(new MenuItemModel('ClientMentor', 'mentor.search.mentor', 'search_mentor', [], 'fa fa-search'));

            $p = new MenuItemModel('ClientMentee', 'mentor.manage.mentee', '/', [], 'fa fa-users');
            $p->addChild(new MenuItemModel('ClientMentee', 'mentor.list.mentee', 'clients_mentee_list', [], 'fa fa-users'));
            $p->addChild(new MenuItemModel('ClientMentee', 'mentor.add', 'register_mentee', [], 'fa fa-users'));
            $client->addChild($p);

            $client->addChild(new MenuItemModel('ClientProcesses', 'mentor.processes', 'clients_process_list', [], 'fa fa-calendar'));
            $client->addChild(new MenuItemModel('ClientSessions', 'mentor.list.sessions', 'clients_sessions', [], 'fa fa-calendar'));
            $client->addChild(new MenuItemModel('RaportSessions', 'raport.menu.sessions', 'summary_raport', [], 'fa fa-calendar'));
            $client->setIsActive(true);
            $menuItems[] = $client;
        }
        
        if ($this->authChecker->isGranted('ROLE_MENTEE')) {
            $mentee = new MenuItemModel('MenteeId', 'mentor.mentee', 'dashboard', [], 'iconclasses fa fa-group');
            $mentee->addChild(new MenuItemModel('MenteeMentor', 'mentor.search.mentor', 'search_mentor', [], 'fa fa-search'));
            $mentee->addChild(new MenuItemModel('MenteeProcesses', 'mentor.mine.processes', 'process_show_my', [], 'fa fa-calendar'));
            $mentee->addChild(new MenuItemModel('RaportSessions', 'raport.menu.sessions', 'summary_raport', [], 'fa fa-calendar'));
            //$mentee->addChild(new MenuItemModel('MenteeWatcher', 'mentee.mine.watcher', 'mentee_show_my_watcher', [], 'fa fa-plus'));
            
            $mentee->setIsActive(true);
            $menuItems[] = $mentee;
        }
        
        if ($this->authChecker->isGranted('ROLE_MENTEE_WATCHER')) {
            $watcher = new MenuItemModel('WatcherId', 'mentor.watcher', 'dashboard', [], 'iconclasses fa fa-group');
            $watcher->addChild(new MenuItemModel('WatcherMentee', 'mentor.manage.mentee', 'watcher_mentee_list', [], 'fa fa-search'));
            $watcher->addChild(new MenuItemModel('WatcherProcesses', 'mentor.list.processes', 'watcher_process_list', [], 'fa fa-calendar'));
            $watcher->setIsActive(true);
            $menuItems[] = $watcher;
        }

        if ($this->authChecker->isGranted('ROLE_MENTOR')) {
            $mentor = new MenuItemModel('MentorId', 'mentor.mentor', 'dashboard', [], 'iconclasses fa fa-graduation-cap');
            $mentor->addChild(new MenuItemModel('MentorCalendarView', 'mentor.show.calendar', 'fos_user_profile_show', [], 'fa fa-calendar'));
            $mentor->addChild(new MenuItemModel('MentorProcesses', 'mentor.list.processes', 'mentor_process_list', [], 'fa fa-calendar'));
            $mentor->addChild(new MenuItemModel('MentorSessions', 'mentor.list.sessions', 'mentor_sessions', [], 'fa fa-calendar'));
            $mentor->addChild(new MenuItemModel('ClientMentorBusinessHours', 'mentor.edit.hours', 'mentor_business_hours', [], 'fa fa-search'));
            $mentor->addChild(new MenuItemModel('RaportSessions', 'raport.menu.sessions', 'summary_raport', [], 'fa fa-calendar'));
            $mentor->setIsActive(true);
            $menuItems[] = $mentor;
        }
        
        if ($this->authChecker->isGranted('ROLE_ADMIN')) {
            $adminService = new MenuItemModel('AdminSearchMentorId', 'mentor.services', 'dashboard', [], 'iconclasses fa fa-graduation-cap');
            $adminService->addChild(new MenuItemModel('AdminSearchMentor', 'mentor.search.mentor', 'search_mentor', [], 'fa fa-search'));
            $adminService->addChild(new MenuItemModel('MentorProcesses', 'mentor.list.processes', 'list_processes', [], 'fa fa-calendar'));
            $adminService->addChild(new MenuItemModel('MentorSessions', 'mentor.list.sessions', 'list_sessions', [], 'fa fa-calendar'));
            $adminService->addChild(new MenuItemModel('RaportSessions', 'raport.menu.sessions', 'summary_raport', [], 'fa fa-calendar'));
            
            $menuItems[] = $adminService;
            
            $admin = new MenuItemModel('AdminId', 'mentor.admin', '/', [], 'iconclasses fa fa-user-secret');
            $a = new MenuItemModel('AdminClient', 'mentor.manage.clients', '/', [], 'fa fa-group');
            $a->addChild(new MenuItemModel('AdminClientList', 'mentor.list.clients', 'list_clients', [], 'fa fa-list'));
            $a->addChild(new MenuItemModel('AdminClientAdd', 'mentor.add', 'register_client', [], 'fa fa-plus'));
            $admin->addChild($a);
        
            $b = new MenuItemModel('AdminMentor', 'mentor.manage.mentor', '/', [], 'fa fa-group');
            $b->addChild(new MenuItemModel('AdminMentorList', 'mentor.list.mentor', 'list_mentor', [], 'fa fa-list'));
            $b->addChild(new MenuItemModel('AdminMentorAdd', 'mentor.add', 'register_mentor', [], 'fa fa-plus'));
            $admin->addChild($b);
        
            $c = new MenuItemModel('AdminMentee', 'mentor.manage.mentee', '/', [], 'fa fa-group');
            $c->addChild(new MenuItemModel('AdminMenteeList', 'mentor.list.mentee', 'list_mentee', [], 'fa fa-list'));
            $c->addChild(new MenuItemModel('AdminMenteeAdd', 'mentor.add', 'register_mentee', [], 'fa fa-plus'));
            $admin->addChild($c);
        
//            $d = new MenuItemModel('AdminMenteeWatcher', 'mentor.manage.mentee_watchers', 'list_mentee_watchers', [], 'fa fa-group');
//            $d->addChild(new MenuItemModel('AdminMenteeWatcherList', 'mentor.list.mentee_watchers', 'list_mentee_watchers', [], 'fa fa-list'));
//            $d->addChild(new MenuItemModel('AdminMenteeWatcherAdd', 'mentor.add', 'register_mentee_watcher', [], 'fa fa-plus'));
//            $admin->addChild($d);
        
            $e = new MenuItemModel('AdminCompany', 'mentor.manage.companies', '/', [], 'fa fa-group');
            $e->addChild(new MenuItemModel('AdminCompanyList', 'mentor.list.company', 'company_index', [], 'fa fa-list'));
            $e->addChild(new MenuItemModel('AdminCompanyAdd', 'companies.add', 'company_new', [], 'fa fa-plus'));
            $admin->addChild($e);
            $menuItems[] = $admin;

            $e = new MenuItemModel('AdminIndustry', 'mentor.manage.industry', '/', [], 'fa fa-group');
            $e->addChild(new MenuItemModel('AdminIndustryList', 'mentor.list.industry', 'industry_index', [], 'fa fa-list'));
            $e->addChild(new MenuItemModel('AdminIndustryAdd', 'industry.add', 'industry_new', [], 'fa fa-plus'));
            $admin->addChild($e);
            $menuItems[] = $admin;

            $e = new MenuItemModel('AdminSpecialization', 'mentor.manage.specializations', '/', [], 'fa fa-group');
            $e->addChild(new MenuItemModel('AdminSpecializationList', 'mentor.list.specialization', 'specializationarea_index', [], 'fa fa-list'));
            $e->addChild(new MenuItemModel('AdminSpecializationAdd', 'specialization.add', 'specializationarea_new', [], 'fa fa-plus'));
            $admin->addChild($e);
            $menuItems[] = $admin;
        }
        
        
        $profile = new MenuItemModel('ProfileId', 'user.profile.header', '/', [], 'iconclasses fa fa-graduation-cap');
        $profile->addChild(new MenuItemModel('ProfileMsg', 'user.profile.message', 'fos_message_inbox', [], 'fa fa-table'));
        $profile->addChild(new MenuItemModel('ProfileView', 'user.profile.view', 'fos_user_profile_show', [], 'fa fa-calendar'));
        $profile->addChild(new MenuItemModel('ProfileEdit', 'user.profile.edit', 'fos_user_profile_edit', [], 'fa fa-table'));
        $profile->addChild(new MenuItemModel('ProfileChangePassword', 'user.profile.change_password', 'fos_user_change_password', [], 'fa fa-table'));
        $profile->addChild(new MenuItemModel('ProfileLogout', 'user.profile.logout', 'fos_user_security_logout', [], 'fa fa-table'));
        $menuItems[] = $profile;
        
        
        return $this->activateByRoute($request->get('_route'), $menuItems);
    }
    
    protected function activateByRoute($route, $items) {
        foreach ($items as $item) {
            if (!$item->hasChildren()) {
                if ($item->getRoute() == $route) {
                    $item->setIsActive(true);
                }
                continue;
            }
            $this->activateByRoute($route, $item->getChildren());
        }
        return $items;
    }

}
