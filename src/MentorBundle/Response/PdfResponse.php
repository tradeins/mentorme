<?php

namespace MentorBundle\Response;

class PdfResponse
{
    private $pdfService;

    public function __construct($pdfService)
    {
        $this->pdfService = $pdfService;
    }

    public function generatePdfResponseFromHtml($filename, $html, $subject = 'Sessions List', $type = 'vertical')
    {
        $pdf = $this->pdfService->create($type, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->setHeaderFont(['dejavusans', '', 10, '', false]);
        $pdf->setFooterFont(['dejavusans', '', 8, '', false]);
        $pdf->SetAuthor('MentorMe');
        $pdf->SetTitle(('MentorMe: Raport'));
        $pdf->SetSubject('Sessions List');
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('dejavusans', '', 8, '', true);
        $pdf->AddPage(($type == 'horizontal') ? "L" : "P");
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->Output($filename.".pdf",'I');
    }
}