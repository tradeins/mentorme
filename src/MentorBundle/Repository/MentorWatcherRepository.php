<?php

namespace MentorBundle\Repository;
use Doctrine\ORM\EntityRepository;

/**
 * MentorGoalCardRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MentorWatcherRepository extends \Doctrine\ORM\EntityRepository
{
    public function findMenteeIdsByWatcherId($id) {
        $ids = [];
        $results = $this->getEntityManager()
        ->createQuery(
          'SELECT p.id FROM MentorBundle:UserMentee p WHERE p.menteeWatcher = '.$id
        )
        ->getResult();
        foreach ($results as $result) {
            $ids[] = $result['id'];
        }
        return $ids;
    }
    
}
