<?php

namespace MentorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\CallbackTransformer;
class MentorProcessType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('idMentor', HiddenType::class)
        ->add('startDate', HiddenType::class)
        ->add('endDate', HiddenType::class)
        ->add('sessions', ChoiceType::class, [
           'label' => 'How many sessions', 
           'choices' => array_combine($r = range(1, 12), $r),
           'choice_translation_domain' => false,
            
        ]);
        
        $builder->get('startDate')
          ->addModelTransformer(new CallbackTransformer(
            function ($time) {
                if ($time instanceof \DateTime) {
                    return $time->format('yyyy-MM-dd HH:ii:ss');
                }
                return '';
            },
            function ($time) {
                return new \DateTime($time);
            }
        ));
        $builder->get('endDate')
          ->addModelTransformer(new CallbackTransformer(
              function ($time) {
                if ($time instanceof \DateTime) {
                    return $time->format('yyyy-MM-dd HH:ii:ss');
                }
                return '';
              },
              function ($time) {
                  return new \DateTime($time);
              }
          ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MentorBundle\Entity\MentorProcess',
            'attr' => ['id' => 'modal-form']
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mb_mentorprocess';
    }


}
