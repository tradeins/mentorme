<?php

namespace MentorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationUserClientFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('email');
        $builder->add('company', EntityType::class, ['class' => 'MentorBundle:Company', 'choice_label' => 'company_name']);
        $builder->add('email');
        $builder->add('personFunction');
        $builder->add('name');
        $builder->add('surname');
        $builder->add('skype');
        $builder->add('telephone', null, [
                'attr' => [
                'data-inputmask' => "'mask': ['(+99) 999-999-999']",
                'data-mask' => null,
                ]
                ]);
        $builder->add('hoursLimit');
        $builder->add('menteeLimit');
        $builder->remove('username');
        $builder->remove('plainPassword');
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('user');
    }
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'fos_user_registration_client';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}