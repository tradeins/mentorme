<?php

namespace MentorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use MentorBundle\Form\IndustryType;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class ProfileUserMentorFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
        $builder->add('surname');
        $builder->add('skype');
        $builder->add('telephone', null, [
            'attr' => [
                'data-inputmask' => "'mask': ['(+99) 999-999-999']",
                'data-mask' => null,
            ]
        ]);
        $builder->add('trenerExperience');
        $builder->add('profilePic', FileType::class, ['data_class' => null,'required' => false, 'label' => 'Profile Pic (JPG file)']);
        
        $builder->add('about', TextareaType::class, ['required' => false, 'attr' => ['class' => 'editor']]);
        $builder->add('professionalProfile', TextareaType::class, ['required' => false, 'attr' => ['class' => 'editor']]);
        $builder->add('education', TextareaType::class, ['required' => false, 'attr' => ['class' => 'editor']]);
        $builder->add('credentials', TextareaType::class, ['required' => false, 'attr' => ['class' => 'editor']]);

        $builder->add('industries', CollectionType::class, [
                'entry_type' => EntityType::class,
                'entry_options' => [
                    'class' => 'MentorBundle:Industry',
                    'choice_label' => 'name',
                    'label' => 'Industry'
                ],
                'allow_add' => true,
                'allow_delete' => false,
                'by_reference' => false,
        ]);

        $builder->add('specializations', CollectionType::class, [
            'entry_type' => EntityType::class,
            'entry_options' => [
                'class' => 'MentorBundle:SpecializationArea',
                'choice_label' => 'name',
                'label' => 'Specialization Area'
            ],
            'allow_add' => true,
            'allow_delete' => false,
            'by_reference' => false,
        ]);

        $builder->remove('username');
        $builder->remove('current_password');
    }

    public function getParent()
    {
        return 'MentorBundle\Form\Type\ProfileFormType';
    }    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('user');
        $resolver->setDefined('companyId');
        $resolver->setDefaults(
            ['validation_groups' => ['Default', 'profile']]
        );
        
    }
    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}