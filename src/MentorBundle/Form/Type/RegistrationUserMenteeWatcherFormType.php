<?php

namespace MentorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Glifery\EntityHiddenTypeBundle\Form\Type\EntityHiddenType;


class RegistrationUserMenteeWatcherFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];
        
        $builder->add('name');
        $builder->add('surname');
        
        if (in_array($user->getEntityType(), ['user_base'])) {
            $builder->add('company',EntityType::class, [
                'label' => 'Company',
                'class' => 'MentorBundle:Company',
                'choice_label' => 'company_name',
                'placeholder' => 'Choose an option',
            ]);
        } else {
            $builder->add('company',EntityHiddenType::class, [
                'class' => 'MentorBundle:Company',
                'data' => $user->getCompany(),
            ]);
        }
        
        $builder->remove('username');
        $builder->remove('plainPassword');
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('user');
    }
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'fos_user_registration_mentee_watcher';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}