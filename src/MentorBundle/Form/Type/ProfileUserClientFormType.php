<?php

namespace MentorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ProfileUserClientFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];
        
        $builder->add('company', EntityType::class, ['class' => 'MentorBundle:Company', 'choice_label' => 'company_name']);
        $builder->add('personFunction');
        $builder->add('name');
        $builder->add('surname');
        $builder->add('skype');
        $builder->add('telephone', null, [
            'attr' => [
                'data-inputmask' => "'mask': ['(+99) 999-999-999']",
                'data-mask' => null,
            ]
        ]);
        $builder->add('profilePic', FileType::class, ['data_class' => null, 'required' => false,'label' => 'Profile Pic (JPG file)']);
        
        if ($user->getEntityType() == 'user_base') {
          $builder->add('menteeLimit');
          $builder->add('hoursLimit');
        }
        $builder->remove('username');
        $builder->remove('current_password');
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('user');
        $resolver->setDefined('companyId');
        $resolver->setDefaults(
            ['validation_groups' => ['Default', 'profile']]
        );
        
    }
    public function getParent()
    {
        return 'MentorBundle\Form\Type\ProfileFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}