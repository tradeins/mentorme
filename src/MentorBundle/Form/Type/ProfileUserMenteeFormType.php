<?php

namespace MentorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ProfileUserMenteeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];
        $companyId = null;
        if (isset($options['companyId'])) {
            $companyId = $options['companyId'];
        }
        $builder->add('name');
        $builder->add('surname');
        $builder->add('skype');
        $builder->add('telephone', null, [
            'attr' => [
                'data-inputmask' => "'mask': ['(+99) 999-999-999']",
                'data-mask' => null,
            ]
        ]);
        $builder->add('profilePic', FileType::class, ['data_class' => null, 'required' => false, 'label' => 'Profile Pic (JPG file)']);
        
        if (in_array($user->getEntityType(), ['user_base'])) {
            $builder->add('client', EntityType::class, [
                    'label' => 'client',
                    'class' => 'MentorBundle:UserClient',
                    'choice_label' => 'fullname',
                    'placeholder' => 'Choose an option',
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $er ) use ( $companyId ) {
                        $qb = $er->createQueryBuilder('w')
                                  ->orderBy('w.surname', 'ASC');
                        if (!empty($companyId)) {
                            $qb->where('w.company = :companyId')
                             ->setParameter('companyId', $companyId);
                        }
                        
                        return $qb;
                    }
                ]);
            $builder->add('menteeWatcher', EntityType::class, [
                'class' => 'MentorBundle:UserMenteeWatcher', 
                'choice_label' => 'fullname',
                'placeholder' => 'Choose an option',
                'required' => false,
                'query_builder' => function(\Doctrine\ORM\EntityRepository $er ) use ( $companyId ) {
                        return $er->createQueryBuilder('w')
                                  ->orderBy('w.surname', 'ASC')
                                  ->where('w.company = :companyId')
                                  ->setParameter('companyId', $companyId);
                }
            ]);
        }
        $builder->remove('username');
        $builder->remove('current_password');
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('user');
        $resolver->setDefined('companyId');
        $resolver->setDefaults(
            ['validation_groups' => ['Default', 'profile']]
        );
    }
    public function getParent()
    {
        return 'MentorBundle\Form\Type\ProfileFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}