<?php

namespace MentorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Glifery\EntityHiddenTypeBundle\Form\Type\EntityHiddenType;
class RegistrationUserMenteeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];
        
        $builder->add('name');
        $builder->add('surname');
        $builder->add('telephone', null, [
            'attr' => [
                'data-inputmask' => "'mask': ['(+99) 999-999-999']",
                'data-mask' => null,
            ]
        ]);
        
        if (in_array($user->getEntityType(), ['user_base'])) {
            $builder->add('company',EntityType::class, [
                'label' => 'Company',
                'class' => 'MentorBundle:Company',
                'choice_label' => 'company_name',
                'placeholder' => 'Choose an option',
                ]);
            $builder->add('client',EntityType::class, [
                'label' => 'client',
                'class' => 'MentorBundle:UserClient',
                'choice_label' => 'fullname',
                'placeholder' => 'Choose an option',
                ]);
//             $builder->add('menteeWatcher', EntityType::class, [
//                 'class' => 'MentorBundle:UserMenteeWatcher',
//                 'choice_label' => 'fullname',
//                 'placeholder' => 'Choose an option',
//                 'required' => false
//                 ]);
        } else {        
            $builder->add('company',EntityHiddenType::class, [
                    'class' => 'MentorBundle:Company',
                    'data' => $user->getCompany(),
            ]);
            $builder->add('client',EntityHiddenType::class, [
                   'class' => 'MentorBundle:UserClient',
                   'data' => $user,
            ]);
        }
        
        $builder->remove('username');
        $builder->remove('plainPassword');
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('user');
    }
    
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'fos_user_registration_mentee';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}