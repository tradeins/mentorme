<?php

namespace MentorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ProfileUserMenteeWatcherFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];
        
        $builder->add('name');
        $builder->add('surname');
        $builder->add('skype');
        $builder->add('telephone');
        $builder->add('profilePic', FileType::class, ['data_class' => null, 'required' => false,'label' => 'Profile Pic (JPG file)']);
        
        if (in_array($user->getEntityType(), ['user_base'])) {
            $builder->add('company',EntityType::class, [
                'label' => 'Company',
                'class' => 'MentorBundle:Company',
                'choice_label' => 'company_name',
                'placeholder' => 'Choose an option',
            ]);
        }
        
        $builder->remove('username');
        $builder->remove('current_password');
    }

    public function getParent()
    {
        return 'MentorBundle\Form\Type\ProfileFormType';
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('user');
        $resolver->setDefined('companyId');
        $resolver->setDefaults(
            ['validation_groups' => ['Default', 'profile']]
        );
        
    }
    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}