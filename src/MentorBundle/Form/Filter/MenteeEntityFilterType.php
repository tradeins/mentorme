<?php

namespace MentorBundle\Form\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Embed filter type.
 */
class MenteeEntityFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $clientId = isset($options['clientId']) ? $options['clientId'] : null;
        $watcherId = isset($options['watcherId']) ? $options['watcherId'] : null;
        $mentor = isset($options['mentor']) ? $options['mentor'] : null;
        
        $builder->add('id', Filters\EntityFilterType::class, [
                'label' => 'Mentee',
                'class' => 'MentorBundle:UserMentee',
                'choice_label' => 'fullname',
                'placeholder' => 'All',
                'multiple' => true,
                'query_builder' => function(\Doctrine\ORM\EntityRepository $er )  use ( $clientId, $watcherId, $mentor ) {
                    $qb = $er->createQueryBuilder('w')
                        ->orderBy('w.surname', 'ASC');
                        if (!empty($clientId)) {
                            $qb->andWhere('w.client = :client')
                            ->setParameter('client', $clientId);
                        }
                        if (!empty($watcherId)) {
                            $qb->andWhere('w.menteeWatcher = :watcher')
                            ->setParameter('watcher', $watcherId);
                        }
                        if (!empty($mentor)) {
                            $qb->leftJoin('w.processes', 'cp');
                            $qb->andWhere('cp.mentor = :mentor')
                            ->setParameter('mentor', $mentor);
                        }
                        
                        return $qb;
                }
        ]);
    }

    public function getBlockPrefix()
    {
        return 'cef';
    }
    

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('clientId');
        $resolver->setDefined('watcherId');
        $resolver->setDefined('mentor');
    }
}