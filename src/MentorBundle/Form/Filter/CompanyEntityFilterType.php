<?php

namespace MentorBundle\Form\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Embed filter type.
 */
class CompanyEntityFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mentor = isset($options['mentor']) ? $options['mentor'] : null;
        
        $builder->add('id', Filters\EntityFilterType::class, ['label' => 'Company',
                'class' => 'MentorBundle:Company',
                'choice_label' => 'companyName',
                'placeholder' => 'All',
                'query_builder' => function(\Doctrine\ORM\EntityRepository $er ) use ($mentor) {
                    $qb = $er->createQueryBuilder('w');
                    
                    if (!empty($mentor)) {
                        $qb->leftJoin('w.mentees', 'ce')
                        ->leftJoin('ce.processes', 'cp')
                        ->andWhere('cp.mentor = :mentor')
                        ->setParameter('mentor', $mentor);
                    }
                    
                    return $qb->orderBy('w.companyName', 'ASC');
                }
        ]);
    }

    public function getBlockPrefix()
    {
        return 'coef';
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
        'csrf_protection'   => false,
        'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
        $resolver->setDefined('mentor');
    }
}