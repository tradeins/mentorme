<?php
namespace MentorBundle\Form\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderExecuterInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use MentorBundle\Form\Filter\IndustryFilterType;
use MentorBundle\Form\Filter\SpecializationAreaFilterType;

class MentorFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('industries', Filters\CollectionAdapterFilterType::class, array(
           'entry_type' => IndustryFilterType::class,
           'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
               $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                   $filterBuilder->leftJoin($alias . '.industries', $joinAlias);
               };
               $qbe->addOnce($qbe->getAlias().'.industries', 'industries', $closure);
           },
       ));
        $builder->add('specializations', Filters\CollectionAdapterFilterType::class, array(
           'entry_type' => SpecializationAreaFilterType::class,
           'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
               $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                   $filterBuilder->leftJoin($alias . '.specializations', $joinAlias);
               };
               $qbe->addOnce($qbe->getAlias().'.specializations', 'spec', $closure);
           },
       ));

        $builder->add('name', Filters\TextFilterType::class);
        $builder->add('surname', Filters\TextFilterType::class);
        $builder->add('trenerExperience', Filters\NumberFilterType::class, [
            'condition_operator' => FilterOperands::OPERATOR_GREATER_THAN_EQUAL,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'cf';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}