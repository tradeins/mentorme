<?php
namespace MentorBundle\Form\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;

/**
 * Embed filter type.
 */
class SpecializationAreaFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', Filters\EntityFilterType::class, [
            'class' => 'MentorBundle:SpecializationArea',
            'label' => 'Specialization Area'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'pf';
    }
}