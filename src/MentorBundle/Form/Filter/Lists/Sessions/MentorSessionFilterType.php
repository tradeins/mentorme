<?php
namespace MentorBundle\Form\Filter\Lists\Sessions;

use MentorBundle\Entity\MentorSession;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderExecuterInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use MentorBundle\Form\Filter\MenteeEntityFilterType;
use MentorBundle\Form\Filter\MentorEntityFilterType;
use MentorBundle\Form\Filter\CompanyEntityFilterType;
use MentorBundle\Form\Filters\DateFromFilterType;
use MentorBundle\Form\Filters\DateToFilterType;

class MentorSessionFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $mentor = null;
        if (isset($options['mentor'])) {
            $mentor = $options['mentor'];
        }
        
        $builder->add('mentee', Filters\CollectionAdapterFilterType::class, array(
          'entry_type' => MenteeEntityFilterType::class,
          'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
            $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                $filterBuilder->leftJoin('cp.mentee', $joinAlias);
            };
            $qbe->addOnce('e.mentee', 'cee', $closure);
          },
          'entry_options' => ['mentor' => $mentor]
        ));

        $builder->add('company', Filters\CollectionAdapterFilterType::class, array(
            'entry_type' => CompanyEntityFilterType::class,
            'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
              $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                $filterBuilder->leftJoin('cp.mentee', 'ce');
                $filterBuilder->leftJoin('ce.company', $joinAlias);
              };
              $qbe->addOnce('e.company', 'comp', $closure);
            },
            'entry_options' => ['mentor' => $mentor]
        ));

        $builder->add('startDate', DateFromFilterType::class, ['label' => 'Start Date']);
        $builder->add('endDate', DateToFilterType::class, ['label' => 'End Date']);
        $builder->add('status', Filters\ChoiceFilterType::class, [
                'label' => 'Status',
                'placeholder' => 'Choose an option',
                'choices' => array_flip(MentorSession::$statusTextChoices)
        ]);
    }

    public function getBlockPrefix()
    {
        return 'clf';
    }

    public function configureOptions(OptionsResolver $resolver)
    {        
        $resolver->setDefined('mentor');
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}