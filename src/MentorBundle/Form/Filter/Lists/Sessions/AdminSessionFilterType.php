<?php
namespace MentorBundle\Form\Filter\Lists\Sessions;

use MentorBundle\Entity\MentorSession;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderExecuterInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use MentorBundle\Form\Filter\MenteeEntityFilterType;
use MentorBundle\Form\Filter\MentorEntityFilterType;
use MentorBundle\Form\Filter\CompanyEntityFilterType;
use MentorBundle\Form\Filters\DateFromFilterType;
use MentorBundle\Form\Filters\DateToFilterType;

class AdminSessionFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('mentee', Filters\CollectionAdapterFilterType::class, array(
          'entry_type' => MenteeEntityFilterType::class,
          'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
            $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                $filterBuilder->leftJoin('cp.mentee', $joinAlias);
            };
            $qbe->addOnce('e.mentee', 'cee', $closure);
          },
        ));
        
        $builder->add('company', Filters\CollectionAdapterFilterType::class, array(
            'entry_type' => CompanyEntityFilterType::class,
            'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
                $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                    $filterBuilder->leftJoin('ce.company', $joinAlias);
                };
                $qbe->addOnce('e.company', 'comp', $closure);
            },
        ));
        
        $builder->add('mentor', Filters\CollectionAdapterFilterType::class, array(
            'entry_type' => MentorEntityFilterType::class,
            'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
                $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                    $filterBuilder->leftJoin('cp.mentor', $joinAlias);
                };
                $qbe->addOnce('e.mentor', 'chc', $closure);
            },
        ));
        
        $builder->add('startDate', DateFromFilterType::class, ['label' => 'Start Date']);
        $builder->add('endDate', DateToFilterType::class, ['label' => 'End Date']);
        $builder->add('status', Filters\ChoiceFilterType::class, [
                'label' => 'Status',
                'placeholder' => 'Choose an option',
                'choices' => array_flip(MentorSession::$statusTextChoices)
        ]);
    }

    public function getBlockPrefix()
    {
        return 'clf';
    }

    public function configureOptions(OptionsResolver $resolver)
    {        
        $resolver->setDefined('client');
        $resolver->setDefined('show_company');
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}