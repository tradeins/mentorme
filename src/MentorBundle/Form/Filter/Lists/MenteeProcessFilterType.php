<?php
namespace MentorBundle\Form\Filter\Lists;

use MentorBundle\Entity\MentorProcess;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderExecuterInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use MentorBundle\Form\Filter\MentorEntityFilterType;
use MentorBundle\Form\Filters\DateFromFilterType;
use MentorBundle\Form\Filters\DateToFilterType;

class MenteeProcessFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('mentor', Filters\CollectionAdapterFilterType::class, array(
            'entry_type' => MentorEntityFilterType::class,
            'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
                $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                    $filterBuilder->leftJoin($alias . '.mentor', $joinAlias);
                };
                $qbe->addOnce($qbe->getAlias().'.mentor', 'spec', $closure);
            },
        ));
        $builder->add('startDate', DateFromFilterType::class, ['label' => 'Start Date']);
        $builder->add('endDate', DateToFilterType::class, ['label' => 'End Date']);
        $builder->add('status', Filters\ChoiceFilterType::class, [
                'label' => 'form.session.status',
                'choices' => MentorProcess::getAllStatuses(),
                'placeholder' => 'Choose an option'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'cepf';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}