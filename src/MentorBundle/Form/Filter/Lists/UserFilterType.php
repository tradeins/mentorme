<?php
namespace MentorBundle\Form\Filter\Lists;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderExecuterInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use MentorBundle\Form\Filter\CompanyEntityFilterType;

class UserFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', Filters\TextFilterType::class);
        $builder->add('name', Filters\TextFilterType::class);
        $builder->add('surname', Filters\TextFilterType::class);
        
        if (isset($options['show_company']) && $options['show_company']) {
            $builder->add('company', Filters\CollectionAdapterFilterType::class, array(
                'entry_type' => CompanyEntityFilterType::class,
                'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
                    $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                        $filterBuilder->leftJoin($alias . '.company', $joinAlias);
                    };
                    $qbe->addOnce($qbe->getAlias().'.company', 'spec', $closure);
                },
            ));
        }
        $builder->add('skype', Filters\TextFilterType::class);
        $builder->add('telephone', Filters\TextFilterType::class);
        $builder->add('enabled', Filters\ChoiceFilterType::class, [
                'label' => 'Status',
                'placeholder' => 'Choose an option',
                'choices' => [
                   'Inactive' => 0,
                   'Active' => 1,
                ],
        ]);
    }

    public function getBlockPrefix()
    {
        return 'uf';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('show_company');
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'show_company' => true,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}