<?php
namespace MentorBundle\Form\Filter\Lists;

use MentorBundle\Entity\MentorProcess;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderExecuterInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use MentorBundle\Form\Filter\MentorEntityFilterType;
use MentorBundle\Form\Filter\MenteeEntityFilterType;
use MentorBundle\Form\Filter\CompanyEntityFilterType;
use MentorBundle\Form\Filters\DateFromFilterType;
use MentorBundle\Form\Filters\DateToFilterType;

class ClientProcessFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $clientId = null;
        if (isset($options['clientId'])) {
            $clientId = $options['clientId'];
        }
        
        $builder->add('mentee', Filters\CollectionAdapterFilterType::class, array(
          'entry_type' => MenteeEntityFilterType::class,
          'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
            $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                $filterBuilder->leftJoin($alias . '.mentee', $joinAlias);
            };
            $qbe->addOnce($qbe->getAlias().'.mentee', 'spece', $closure);
          },
          'entry_options' => ['clientId' => $clientId]
        ));
        
        if (isset($options['show_company']) && $options['show_company']) {
            $builder->add('company', Filters\CollectionAdapterFilterType::class, array(
            'entry_type' => CompanyEntityFilterType::class,
            'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
                $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                    $filterBuilder->leftJoin($alias .'.mentee', 'ce');
                    $filterBuilder->leftJoin('ce.company', $joinAlias);
                };
                $qbe->addOnce($qbe->getAlias().'.company', 'comp', $closure);
            },
            ));
        }
        
        $builder->add('mentor', Filters\CollectionAdapterFilterType::class, array(
            'entry_type' => MentorEntityFilterType::class,
            'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
                $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                    $filterBuilder->leftJoin($alias . '.mentor', $joinAlias);
                };
                $qbe->addOnce($qbe->getAlias().'.mentor', 'spec', $closure);
            },
        ));
        $builder->add('startDate', DateFromFilterType::class, ['label' => 'Start Date']);
        $builder->add('endDate', DateToFilterType::class, ['label' => 'End Date']);
        $builder->add('status', Filters\ChoiceFilterType::class, [
                'label' => 'form.session.status',
                'choices' => MentorProcess::getAllStatuses(),
                'placeholder' => 'Choose an option'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'cpf';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('clientId');
        $resolver->setDefined('show_company');
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}