<?php
namespace MentorBundle\Form\Filter\Lists;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderExecuterInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;

class CompanyFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('companyName', Filters\TextFilterType::class);
        $builder->add('nip', Filters\TextFilterType::class);
        $builder->add('enabled', Filters\ChoiceFilterType::class, [
                'label' => 'Status',
                'placeholder' => 'Choose an option',
                'choices' => [
                   'Inactive' => 0,
                   'Active' => 1,
                ],
        ]);
    }

    public function getBlockPrefix()
    {
        return 'clf';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}