<?php
namespace MentorBundle\Form\Filter\Lists;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderExecuterInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use MentorBundle\Form\Filter\CompanyEntityFilterType;
use MentorBundle\Form\Filter\MenteeEntityFilterType;
use MentorBundle\Form\Filter\MentorEntityFilterType;
use MentorBundle\Form\Filters\DateFromFilterType;
use MentorBundle\Form\Filters\DateToFilterType;

class SessionRaportFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['showCompany']) {
            $builder->add('company', Filters\CollectionAdapterFilterType::class, array(
                'entry_type' => CompanyEntityFilterType::class,
                'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
                  $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                    $filterBuilder->leftJoin('mt.company', $joinAlias);
                  };
                  $qbe->addOnce('ms.company', 'f_company', $closure);
                },
                'entry_options' => [
                    'mentor' => $options['mentor']
                ]
            ));
        }

        if ($options['showMentee']) {
            $builder->add('mentee', Filters\CollectionAdapterFilterType::class, array(
                'entry_type' => MenteeEntityFilterType::class,
                'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
                  $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                    $filterBuilder->leftJoin('mp.mentee', $joinAlias);
                  };
                  $qbe->addOnce('ms.mentee', 'f_mentee', $closure);
                },
                'entry_options' => [
                    'mentor' => $options['mentor'],
                    'clientId' => $options['client']
                ]
            ));
        }

        $builder->add('mentor', Filters\CollectionAdapterFilterType::class, array(
            'entry_type' => MentorEntityFilterType::class,
            'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {
                $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                    $filterBuilder->leftJoin('mp.mentor', $joinAlias);
                };
                $qbe->addOnce('ms.mentor', 'f_mentor', $closure);
            }
        ));

        $builder->add('startDate', DateFromFilterType::class, ['label' => 'Start Date']);
        $builder->add('endDate', DateToFilterType::class, ['label' => 'End Date']);
    }

    public function getBlockPrefix()
    {
        return 'uf';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('mentor');
        $resolver->setDefined('showMentee');
        $resolver->setDefined('showCompany');
        $resolver->setDefined('client');
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'showCompany' => true,
            'showMentee' => true,
            'client' => null,
            'mentor' => null,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}