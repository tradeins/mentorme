<?php

namespace MentorBundle\Form\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;

/**
 * Embed filter type.
 */
class MentorEntityFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', Filters\EntityFilterType::class, ['label' => 'Mentor',
                'class' => 'MentorBundle:UserMentor',
                'choice_label' => 'fullname',
                'placeholder' => 'All',
                'multiple' => true,
                'query_builder' => function(\Doctrine\ORM\EntityRepository $er ){
                    return $er->createQueryBuilder('w')
                    ->orderBy('w.surname', 'ASC');
                }
        ]);
    }

    public function getBlockPrefix()
    {
        return 'chf';
    }
}