<?php

namespace MentorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class CompanyType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('company_name')
        ->add('nip')
        ->add('addressStreet')
        ->add('addressBuilding')
        ->add('addressLocal')
        ->add('addressPostalCode')
        ->add('addressCity')
        ->add('correspondenceStreet')
        ->add('correspondenceBuilding')
        ->add('correspondenceLocal')
        ->add('correspondencePostalCode')
        ->add('correspondenceCity')
        ->add('rules', TextareaType::class, ['attr' => ['class' => 'editor'], 'required' => false]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MentorBundle\Entity\Company'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mb_company';
    }


}
