<?php

namespace MentorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MentorGoalCardType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('firstGoal', TextareaType::class, ['attr' => ['class' => 'editor']])
        ->add('firstKpi', TextareaType::class, ['attr' => ['class' => 'editor']])
        ->add('secondGoal', TextareaType::class, ['attr' => ['class' => 'editor']])
        ->add('secondKpi', TextareaType::class, ['attr' => ['class' => 'editor']])
        ->add('thirdGoal', TextareaType::class, ['attr' => ['class' => 'editor']])
        ->add('thirdKpi', TextareaType::class, ['attr' => ['class' => 'editor']]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MentorBundle\Entity\MentorGoalCard'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mb_mentorgc';
    }


}
