<?php
namespace MentorBundle\Form\Filters;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\DateFilterType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


/**
 * Filter type for date range field.
 *
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class DateToFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $rightOptions = ['widget' => 'single_text', 'html5' => false, 'attr' => ['class' => 'js-datepicker']];
        $builder->add('left_date', HiddenType::class);
        $builder->add('right_date', DateFilterType::class, $rightOptions);
        $builder->setAttribute('filter_value_keys', array(
            'left_date'  => [],
            'right_date' => $rightOptions,
        ));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(array(
                'required'               => false,
                'data_extraction_method' => 'value_keys',
            ))
            ->setAllowedValues('data_extraction_method', array('value_keys'))
        ;
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'filter_date_range';
    }
}