<?php
namespace MentorBundle\Form\Filters;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\DateFilterType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


/**
 * Filter type for date range field.
 *
 * @author Cédric Girard <c.girard@lexik.fr>
 */
class DateFromFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $leftOptions = ['widget' => 'single_text', 'html5' => false, 'attr' => ['class' => 'js-datepicker']];
        $builder->add('left_date', DateFilterType::class, $leftOptions);
        $builder->add('right_date', HiddenType::class);
        $builder->setAttribute('filter_value_keys', array(
            'left_date'  => $leftOptions,
            'right_date' => [],
        ));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(array(
                'required'               => false,
                'data_extraction_method' => 'value_keys',
            ))
            ->setAllowedValues('data_extraction_method', array('value_keys'))
        ;
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'filter_date_range';
    }
}