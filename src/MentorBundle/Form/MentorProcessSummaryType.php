<?php

namespace MentorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
class MentorProcessSummaryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sliderOptions = [
            'class' => 'slider',
            'data-slider-id' => 'blue',
            'data-provide' => 'slider',
            'data-slider-min' => '0',
            'data-slider-max' => '100',
            'data-slider-step' => '10',
            'data-slider-tooltip' => 'show'
        ];
        
        $builder->add('summary', null, ['label' => 'raport.summary', 'attr' => ['class' => 'editor']])
        ->add('informations', null, ['label' => 'raport.informations', 'attr' => ['class' => 'editor']])
        ->add('ratingFirstGoalSummary', null, ['label' => 'raport.goal.first.summary', 'attr' => ['class' => 'editor']])
        ->add('ratingFirstGoalComment', null, ['label' => 'raport.goal.first.comment', 'attr' => ['class' => 'editor']])
        ->add('ratingFirstGoalRate', TextType::class, ['attr' => array_merge($sliderOptions, ['data-slider-value' => 50])])
        ->add('ratingSecondGoalSummary', null, ['label' => 'raport.goal.second.summary', 'attr' => ['class' => 'editor']])
        ->add('ratingSecondGoalComment', null, ['label' => 'raport.goal.second.comment', 'attr' => ['class' => 'editor']])
        ->add('ratingSecondGoalRate', TextType::class, ['attr' => array_merge($sliderOptions, ['data-slider-value' => 50])])
        ->add('ratingThirdGoalSummary', null, ['label' => 'raport.goal.third.summary', 'attr' => ['class' => 'editor']])
        ->add('ratingThirdGoalComment', null, ['label' => 'raport.goal.third.comment', 'attr' => ['class' => 'editor']])
        ->add('ratingThirdGoalRate', TextType::class, ['attr' => array_merge($sliderOptions, ['data-slider-value' => 50])])
        ->add('recommendations', null, ['label' => 'raport.recommendations', 'attr' => ['class' => 'editor']])
        ->add('informationForSuperior', null, ['label' => 'raport.information_for_mentee_superior', 'attr' => ['class' => 'editor']])
        ->add('additionalInformations', null, ['label' => 'raport.additional', 'attr' => ['class' => 'editor']]);
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($sliderOptions) {
            $form = $event->getForm();
            $entity = $event->getData();
            $form->add('ratingFirstGoalRate', TextType::class, ['label' => 'raport.goal.first.rate', 'attr' => array_merge($sliderOptions, ['data-slider-value' => $entity->getRatingFirstGoalRate()])])
            ->add('ratingSecondGoalRate', TextType::class, ['label' => 'raport.goal.second.rate', 'attr' => array_merge($sliderOptions, ['data-slider-value' => $entity->getRatingSecondGoalRate()])])
            ->add('ratingThirdGoalRate', TextType::class, ['label' => 'raport.goal.third.rate', 'attr' => array_merge($sliderOptions, ['data-slider-value' => $entity->getRatingThirdGoalRate()])]);
        });
        
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MentorBundle\Entity\MentorProcessSummary'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mb_mentorprocesssummary';
    }


}
