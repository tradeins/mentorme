<?php 

namespace AppBundle\Security;

use AppBundle\Entity\User as AppUser;
use AppBundle\Exception\AccountCompanyBlockedException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) {
            return;
        }

        if (in_array($user->getEntityType(), ['user_base', 'user_mentor'])) {
            return;
        }
        
        $company = $user->getCompany();
        if (is_null($company) || !$company->getEnabled()) {
            throw new AccountCompanyBlockedException("Company is blocked");
        }
    }

    public function checkPostAuth(UserInterface $user)
    {
        return;
    }
}