<?php
namespace AppBundle\Model;

use Avanzu\AdminThemeBundle\Model\MessageInterface as ThemeMessage;

class Message implements  ThemeMessage {

    private $id;
    private $from;
    private $sentAt;
    private $subject;
    
    public function setId($id) {
        $this->id = $id;
    }
    
    public function setFrom($from) {
        $this->from = $from;
    }
    
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;
    }
    
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }
    
    /**
     * Returns the sender
     *
     * @return mixed
     */
    public function getFrom() {
        return $this->from;
    }
    
    /**
     * Returns the sentAt date
     *
     * @return \DateTime
    */
    public function getSentAt() {
        return $this->sentAt;
    }
    
    /**
     * Returns the subject
     *
     * @return string
    */
    public function getSubject() {
        return $this->subject;
    }
    
    /**
     * Returns the unique identifier of this message
     *
     * @return mixed
    */
    public function getIdentifier() {
        return $this->id;
    }
}