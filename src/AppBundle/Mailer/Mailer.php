<?php
namespace AppBundle\Mailer;

use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Translation\Translator;

class Mailer implements MailerInterface 
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;
    
    /**
     * @var UrlGeneratorInterface
     */
    protected $router;
    
    /**
     * @var EngineInterface
     */
    protected $templating;
    
    /**
     * @var array
     */
    protected $parameters;
    
    /**
     * @var TokenStorage
     */
    protected $tokenStorage;
    
    /**
     * 
     * @var Translator
     */
    protected $translator;
    
    /**
     * Mailer constructor.
     *
     * @param \Swift_Mailer         $mailer
     * @param UrlGeneratorInterface $router
     * @param EngineInterface       $templating
     * @param array                 $parameters
     */
    public function __construct($mailer, UrlGeneratorInterface  $router, EngineInterface $templating, array $parameters, TokenStorage $tokenStorage, Translator $translator)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->templating = $templating;
        $this->parameters = $parameters;
        $this->tokenStorage = $tokenStorage;
        $this->translator = $translator;
    }
    
    /**
     * {@inheritdoc}
     */
    public function sendConfirmationEmailMessage(UserInterface $user)
    {
        $registeringUser = $this->tokenStorage->getToken()->getUser();
        $templateData = $this->getEmailRegisterTemplate($registeringUser->getEntityType(), $user->getEntityType());
        $template = $templateData['templateName'];
        $url = $this->router->generate('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
        $rendered = $this->templating->render($template, array(
            'user' => $user,
            'confirmationUrl' => $url,
            'termsUrl' => $this->router->generate('main_terms', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'privacyUrl' => $this->router->generate('main_privacy', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'manager' => ($registeringUser->getEntityType() == 'user_client') ? $registeringUser->getFullName() : '',
            'managerRole' => ($registeringUser->getEntityType() == 'user_client') ? $registeringUser->getPersonFunction() : '',
            'processUrl' => $this->router->generate('register_mentee', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'infoUrl' => $this->router->generate('main', ['_fragment' => 'opis-aplikacji'], UrlGeneratorInterface::ABSOLUTE_URL),
            'searchUrl' => $this->router->generate('search_mentor', [], UrlGeneratorInterface::ABSOLUTE_URL),
        ));
        $this->sendEmailMessage($rendered, $this->parameters['from_email']['confirmation'], (string) $user->getEmail(), $templateData['isHtml']);
    }

    private function getTermsUrl($user) {
        if (in_array($user->getEntityType(), ['user_mentee', 'user_client']) && $user->getCompany()) {
            return $this->router->generate('company_terms', ['id' => $user->getCompany()->getId()] , UrlGeneratorInterface::ABSOLUTE_URL);
        }
        return null;
    }
    
    private function getEmailRegisterTemplate($currentUserEntity, $registerUserEntity) {
        $template =  $this->parameters['confirmation.template'];
        $isHtml = false;
        if ($currentUserEntity == 'user_base') {
            switch ($registerUserEntity) {
              case 'user_client':
                $template = 'MentorBundle:Emails:register.client.by.admin.html.twig';
                $isHtml = true;
                break;
              case 'user_mentee':
                $isHtml = true;
                $template = 'MentorBundle:Emails:register.mentee.by.admin.html.twig';
                break;
              case 'user_mentor':
                $isHtml = true;
                $template = 'MentorBundle:Emails:register.mentor.by.admin.html.twig';
                break;
            }
//         } else if ($currentUserEntity == 'user_mentee' && $registerUserEntity == 'user_watcher') {
//             $template = 'MentorBundle:Emails:register.watcher.by.mentee.txt.twig';
        } else if ($currentUserEntity == 'user_client' && $registerUserEntity == 'user_mentee') {
            $isHtml = true;
            $template = 'MentorBundle:Emails:register.mentee.by.client.html.twig';
        }
        return ['templateName' => $template, 'isHtml' => $isHtml];
    }
    
    /**
     * {@inheritdoc}
     */
    public function sendResettingEmailMessage(UserInterface $user)
    {
        $template = $this->parameters['resetting.template'];
        $url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
        $rendered = $this->templating->render($template, array(
            'user' => $user,
            'confirmationUrl' => $url,
        ));
        $this->sendEmailMessage($rendered, $this->parameters['from_email']['resetting'], (string) $user->getEmail());
    }

    /**
     * @param string       $renderedTemplate
     * @param array|string $fromEmail
     * @param array|string $toEmail
     */
    protected function sendEmailMessage($renderedTemplate, $fromEmail, $toEmail, $isHtml = false)
    {
        if ($isHtml) {
            $subject = $this->translator->trans('registration.email.subject');
            $body = $renderedTemplate;
        } else {
            $renderedLines = explode("\n", trim($renderedTemplate));
            $subject = array_shift($renderedLines);
            $body = implode("\n", $renderedLines);
        }
        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail)
            ->setBody($body, $isHtml ? 'text/html' : 'text/plain');

        $this->mailer->send($message);
    }
}