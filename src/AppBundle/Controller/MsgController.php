<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\MessageBundle\Controller\MessageController;
use AppBundle\Entity\User;

class MsgController extends MessageController
{

    /**
     * @Route("/msg/{id}/new", name="fos_message_user_new")
     */
    public function newThreadUserAction(User $user)
    {
        $form = $this->container->get('fos_message.new_thread_form.factory')->create();
        $formHandler = $this->container->get('fos_message.new_thread_form.handler');
        
        if ($message = $formHandler->process($form)) {
            return new RedirectResponse($this->container->get('router')->generate('fos_message_thread_view', array(
            'threadId' => $message->getThread()->getId(),
            )));
        }
        $field = $form->get('recipient');
        $field->setData($user);
        return $this->container->get('templating')->renderResponse('FOSMessageBundle:Message:newThread.html.twig', array(
            'form' => $form->createView(),
            'data' => $form->getData(),
            'user' => $user,
        ));
    }
}