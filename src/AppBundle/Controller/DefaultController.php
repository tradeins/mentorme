<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/v2", name="v2")
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm('AppBundle\Form\ContactType', null, ['method' => 'POST']);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid() && $this->captchaverify($request->get('g-recaptcha-response'))) {
            try {
                $data = $form->getData();
                $body = "Został wypełniony formularz kontaktowy:\n\nImię:".$data['name']."\nTelefon:".$data['telephone'];
                $body .= "\n".$data['email']."\n\nTreść:".$data['message'];
                $message = (new \Swift_Message())
                    ->setSubject('[mentor_me] formularz kontaktowy')
                    ->setFrom($data['email'])
                    ->setTo('contact@glan.d2.pl')
                    ->setBody($body);
                $this->get('mailer')->send($message);
            } catch (\Throwable $e) {
            }
        }
        
        return $this->render('main/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'user' => $this->getUser(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route ("/kontakt", name="main_contact")
     */
    public function contactAction(Request $request) {

        $form = $this->createForm('AppBundle\Form\ContactType', null, ['method' => 'POST']);
        $form->handleRequest($request);
        $successSentMessege = false;
        if ($form->isSubmitted() && $form->isValid() && $this->captchaverify($request->get('g-recaptcha-response'))) {
            try {
                $data = $form->getData();
                $body = "Został wypełniony formularz kontaktowy: \n\nImię: ".$data['name']."\nTelefon: ".$data['telephone'];
                $body .= "\n Email: ".$data['email']."\n\nTreść: ".$data['message'];
                $message = (new \Swift_Message())
                    ->setSubject('[MentorMe] Formularz Kontaktowy: '.$data['subject'])
                    ->setFrom($data['email'])
                    ->setTo('contact@mentorme.com.pl')
                    ->setBody($body);
                $this->get('mailer')->send($message);
                $successSentMessege = true;
            } catch (\Throwable $e) {
            }
        }

        return $this->render('v2/contact.html.twig', [
            'form' => $form->createView(),
            'successSentMessage' => $successSentMessege,
        ]);
    }

    /**
     * @Route("/", name="main")
     */
    public function v2Action() {
        return $this->render('v2/index.html.twig', []);
    }

    /**
     * @Route("/regulamin", name="main_terms")
     */
    public function termsAction() {
        return $this->render('v2/terms.html.twig', []);
    }

    /**
     * @Route("/polityka-prywatnosci", name="main_privacy")
     */
    public function privacyAction() {
        return $this->render('v2/privacy.html.twig', []);
    }
    
    /**
     * @Route("/dashboard/", name="dashboard")
     */
    public function dashboardAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $repo = $em->getRepository('AppBundle:Notification');
        $notifications = $repo->findBy(['user' => $this->getUser()->getId()]);
        return $this->render('MentorBundle:Default:index.html.twig', [
            'notifications' => $notifications
        ]);
    }
    
    # get success response from recaptcha and return it to controller
    public function captchaverify($recaptcha){
        $url = "https://www.google.com/recaptcha/api/siteverify";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
        "secret"=>"6LfsnGEUAAAAAGjN4SiYtgWaw_vOLcbi9UG3ljye","response"=>$recaptcha));
        $response = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($response);
    
        return $data->success;
    }
}
