<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'attr' => ['placeholder' => 'Imię'],
            'constraints' => [
                new NotBlank(["message" => "Prosimy wprowadzić imię"]),
            ]
        ])
        ->add('subject', TextType::class, [
            'attr' => ['placeholder' => 'Temat'],
            'constraints' => [
                new NotBlank(["message" => "Prosimy wprowadzić temat"]),
            ]
        ])
        ->add('email', TextType::class, [
            'attr' => ['placeholder' => 'E-mail'],
            'constraints' => [
                new NotBlank(["message" => "Prosimy wprowadzić adres email"]),
                new Email(["message" => "Prosimy wprowadzić prawidłowy adres email"]),
            ]
        ])
        ->add('telephone', TextType::class, ['required' => false, 'attr' => ['placeholder' => 'Telefon']])
        ->add('message', TextareaType::class, [
            'attr' => ['placeholder' => 'Wpisz swoją wiadomość...', 'rows' => 9],
            'constraints' => [
                new NotBlank(["message" => "Prosimy wprowadzić wiadomość"]),
            ]
        ]);
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'error_bubbling' => true
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'cf';
    }


}
