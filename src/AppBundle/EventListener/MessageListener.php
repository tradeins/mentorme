<?php
namespace AppBundle\EventListener;

use Avanzu\AdminThemeBundle\Event\MessageListEvent;
use AppBundle\Model\Message;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MessageListener {

    private $container;
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
	public function onListMessages(MessageListEvent $event) {
		foreach($this->getMessages() as $message) {
			$event->addMessage($message);
		}
	}

	protected function getMessages() {
	    $provider = $this->container->get('fos_message.provider');
	    $threads = $provider->getInboxThreads();
	    $messages = [];
	    foreach ($threads as $thread) {
	        if ($thread->isReadByParticipant($this->getUser())) {
	            continue;
	        }
	        $allMsg = $thread->getMessages();
	        $lastMessage = $allMsg->offsetGet($allMsg->count()-1);
	         
	        $msg = new Message();
	        $msg->setId($thread->getId());
	        $msg->setSubject($thread->getSubject());
	        $msg->setSentAt($lastMessage->getCreatedAt());
	        $msg->setFrom($lastMessage->getSender());
	        $messages[] = $msg;
	    }
	    return $messages;
	}
	
	protected function getUser() {
	    return $this->container->get('security.token_storage')->getToken()->getUser();
	}

}