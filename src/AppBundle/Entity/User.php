<?php
namespace AppBundle\Entity;

use FOS\MessageBundle\Model\ParticipantInterface;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Avanzu\AdminThemeBundle\Model\UserInterface as ThemeUser;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * 
 * @ORM\DiscriminatorMap({"user_base" = "MentorBundle\Entity\UserBase", "user_client" = "MentorBundle\Entity\UserClient", "user_mentor" = "MentorBundle\Entity\UserMentor", "user_mentee" = "MentorBundle\Entity\UserMentee", "user_mentee_watcher" = "MentorBundle\Entity\UserMenteeWatcher"})
 */
abstract class User extends BaseUser  implements ParticipantInterface, ThemeUser
{
    protected $entityType;
    protected $redirectUrlPath = '';
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    public $name;
    
    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    public $surname;
    
    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    public $skype;
    
    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * @Assert\Regex(
     *     pattern="/(\((?:\B\+|\b00)?48\)|(?:\B\+|\b00)?48)?([ -])?(?(1)|\b)\d{3}[ -]?\d{3}[ -]?\d{3}\b/",
     *     htmlPattern = "(\((?:\B\+|\b00)?48\)|(?:\B\+|\b00)?48)?([ -])?(?(1)|\b)\d{3}[ -]?\d{3}[ -]?\d{3}\b",
     *     match=true,
     *     message="Wrong telephone format"
     * )
     */
    public $telephone;
        
    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\File(mimeTypes={ "image/jpg", "image/jpeg" }, groups={"Profile"})
     * @Assert\Image(
     *     minWidth = 100,
     *     maxWidth = 1400,
     *     minHeight = 100,
     *     maxHeight = 1900,
     *     groups={"Profile"}
     * );
     */
    public $profilePic;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Notification", mappedBy="user", cascade={"persist"})
     */
    public $notifications;

    /**
     * @ORM\OneToMany(targetEntity="MentorBundle\Entity\MentorProcess", mappedBy="closingUser", cascade={"persist"})
     */
    public $closedProcesses;

    public function __construct()
    {
        parent::__construct();
        $this->notifications = new ArrayCollection();
        $this->closedProcesses = new ArrayCollection();
    }
    
    public function getName()
    {
        return $this->name;
    }
    public function getSkype()
    {
        return $this->skype;
    }
    public function getTelephone()
    {
        return $this->telephone;
    }
    public function getSurname()
    {
        return $this->surname;
    }
    public function getFullName()
    {
        return ucwords($this->name.' '.$this->surname);
    }
    
    public function getEntityType()
    {
        return $this->entityType;
    }
    
    public function getRedirectListPath() {
        return $this->redirectPath;
    }
    
    public function setEmail($email)
    {
        $email = is_null($email) ? '' : $email;
        parent::setEmail($email);
        $this->setUsername($email);
        if (empty($this->getPassword())) {
            $this->setPlainPassword(substr(sha1(random_bytes(8)), 0, 10));
        }
        return $this;
    }
    
    public function getProfilePic() {
        return $this->profilePic;
    }
    
    public function setProfilePic($profilePic) {
        $this->profilePic = $profilePic;
        return $this;
    }
    
    public function addNotification(Notification $notification)
    {
        if ($this->notifications->contains($notification)) {
            return;
        }
        $this->notifications[] = $notification;
        $notification->setUser($this);
    }
    
    public function removeNotification(Notification $notification)
    {
        if (!$this->notifications->contains($notification)) {
            return;
        }
        $this->notifications->removeElement($notification);
        $notification->setUser(null);
    }
    
    public function getNotifications()
    {
        return $this->notifications;
    }

    public function getClosedProcesses()
    {
        return $this->closedProcesses;
    }
    
    /** ThemeUser **/
    public function getAvatar() { return $this->profilePic; }
    
    public function getMemberSince(){ return null;}
    
    public function isOnline(){ return null;}
    
    public function getIdentifier(){ return null;}
    
    public function getTitle(){ return null;}
    
    
    
}