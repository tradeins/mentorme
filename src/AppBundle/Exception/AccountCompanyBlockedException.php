<?php

namespace AppBundle\Exception;

use Symfony\Component\Security\Core\Exception\AccountStatusException;

class AccountCompanyBlockedException extends AccountStatusException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
        return 'Your company is blocked.';
    }
}
