<?php
namespace AppBundle\UserManager;

use PUGX\MultiUserBundle\Doctrine\UserManager;
use Doctrine\ORM\Query\ResultSetMapping;


class ABUserManager extends UserManager
{
    public function findByRole($role)
    {
        $qb = $this->getRepository()->createQueryBuilder('findByRole');
        $qb->select('u')
        ->from("AppBundle\Entity\User", 'u')
        ->where('u.roles LIKE :roles')
        ->setParameter('roles', '%"'.$role.'"%');
        return $qb->getQuery()->getResult();
    }
    
    public function countActiveUsersByRole($userId, $role) {
        $rsm = new ResultSetMapping;
        $rsm->addScalarResult('counted', 'co');
        $table = '';
        switch ($role) {
            case 'ROLE_MENTEE':
                $table = 'user_mentee';
                break;
            case 'ROLE_MENTEE_WATCHER':
                $table = 'user_mentee_watcher';
                break;
        }
        if (empty($table)) {
            return 0;
        }
        
        $sql = "select count(1) as counted from fos_user as fu 
                join $table as uc on uc.id = fu.id
                where fu.enabled = 1 and uc.id_parent_user = ?;";
        
        $query = $this->objectManager->createNativeQuery($sql, $rsm);
        $query->setParameter(1, $userId);
        return $query->getResult();
    }
}