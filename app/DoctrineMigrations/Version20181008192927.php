<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181008192927 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE mentor_process ADD id_closing_user INT DEFAULT NULL, ADD close_reason LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE mentor_process ADD CONSTRAINT FK_1F16E3B3A39C4B72 FOREIGN KEY (id_closing_user) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_1F16E3B3A39C4B72 ON mentor_process (id_closing_user)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE mentor_process DROP FOREIGN KEY FK_1F16E3B3A39C4B72');
        $this->addSql('DROP INDEX IDX_1F16E3B3A39C4B72 ON mentor_process');
        $this->addSql('ALTER TABLE mentor_process DROP id_closing_user, DROP close_reason');
    }
}
