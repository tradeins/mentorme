<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180717151736 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE professional');
        $this->addSql('DROP TABLE specialization');
        $this->addSql('ALTER TABLE mentor_industries DROP FOREIGN KEY FK_386637E72B19A734');
        $this->addSql('ALTER TABLE mentor_industries DROP FOREIGN KEY FK_386637E7DBA0BDA0');
        $this->addSql('ALTER TABLE mentor_industries ADD CONSTRAINT FK_386637E72B19A734 FOREIGN KEY (industry_id) REFERENCES industry (id)');
        $this->addSql('ALTER TABLE mentor_industries ADD CONSTRAINT FK_386637E7DBA0BDA0 FOREIGN KEY (user_mentor_id) REFERENCES user_mentor (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE professional (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX UNIQ_B3B573AA5E237E06 (name), INDEX IDX_B3B573AAA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE specialization (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX UNIQ_9ED9F26A5E237E06 (name), INDEX IDX_9ED9F26AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE professional ADD CONSTRAINT FK_B3B573AAA76ED395 FOREIGN KEY (user_id) REFERENCES user_mentor (id)');
        $this->addSql('ALTER TABLE specialization ADD CONSTRAINT FK_9ED9F26AA76ED395 FOREIGN KEY (user_id) REFERENCES user_mentor (id)');
        $this->addSql('ALTER TABLE mentor_industries DROP FOREIGN KEY FK_386637E7DBA0BDA0');
        $this->addSql('ALTER TABLE mentor_industries DROP FOREIGN KEY FK_386637E72B19A734');
        $this->addSql('ALTER TABLE mentor_industries ADD CONSTRAINT FK_386637E7DBA0BDA0 FOREIGN KEY (user_mentor_id) REFERENCES user_mentor (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mentor_industries ADD CONSTRAINT FK_386637E72B19A734 FOREIGN KEY (industry_id) REFERENCES industry (id) ON DELETE CASCADE');
    }
}
