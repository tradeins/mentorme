<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180627203254 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE audit_associations (id INT AUTO_INCREMENT NOT NULL, typ VARCHAR(128) NOT NULL, tbl VARCHAR(128) NOT NULL, `label` VARCHAR(255) DEFAULT NULL, fk VARCHAR(255) NOT NULL, class VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE audit_logs (id INT AUTO_INCREMENT NOT NULL, source_id INT NOT NULL, target_id INT DEFAULT NULL, blame_id INT DEFAULT NULL, action VARCHAR(12) NOT NULL, tbl VARCHAR(128) NOT NULL, diff LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', logged_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_D62F2858953C1C61 (source_id), UNIQUE INDEX UNIQ_D62F2858158E0B66 (target_id), UNIQUE INDEX UNIQ_D62F28588C082A2E (blame_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, thread_id INT DEFAULT NULL, sender_id INT DEFAULT NULL, body LONGTEXT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_B6BD307FE2904019 (thread_id), INDEX IDX_B6BD307FF624B39D (sender_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message_metadata (id INT AUTO_INCREMENT NOT NULL, message_id INT DEFAULT NULL, participant_id INT DEFAULT NULL, is_read TINYINT(1) NOT NULL, INDEX IDX_4632F005537A1329 (message_id), INDEX IDX_4632F0059D1C3019 (participant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, id_user INT DEFAULT NULL, entity_type VARCHAR(20) NOT NULL, entity_id INT NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_BF5476CA6B3CA4B (id_user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE thread (id INT AUTO_INCREMENT NOT NULL, created_by_id INT DEFAULT NULL, subject VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, is_spam TINYINT(1) NOT NULL, INDEX IDX_31204C83B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE thread_metadata (id INT AUTO_INCREMENT NOT NULL, thread_id INT DEFAULT NULL, participant_id INT DEFAULT NULL, is_deleted TINYINT(1) NOT NULL, last_participant_message_date DATETIME DEFAULT NULL, last_message_date DATETIME DEFAULT NULL, INDEX IDX_40A577C8E2904019 (thread_id), INDEX IDX_40A577C89D1C3019 (participant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', name VARCHAR(100) DEFAULT NULL, surname VARCHAR(200) DEFAULT NULL, skype VARCHAR(80) DEFAULT NULL, telephone VARCHAR(30) DEFAULT NULL, profile_pic VARCHAR(255) DEFAULT NULL, type VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_957A647992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_957A6479A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_957A6479C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE business_hours (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, open DATETIME NOT NULL, end DATETIME NOT NULL, INDEX IDX_F4FB5A32A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company (id INT AUTO_INCREMENT NOT NULL, company_name VARCHAR(255) NOT NULL, nip VARCHAR(20) DEFAULT NULL, address_postal_code VARCHAR(10) DEFAULT NULL, address_city VARCHAR(255) DEFAULT NULL, address_street VARCHAR(255) DEFAULT NULL, address_building VARCHAR(15) DEFAULT NULL, address_local VARCHAR(5) DEFAULT NULL, correspondence_postal_code VARCHAR(10) DEFAULT NULL, correspondence_city VARCHAR(255) DEFAULT NULL, correspondence_street VARCHAR(255) DEFAULT NULL, correspondence_building VARCHAR(15) DEFAULT NULL, correspondence_local VARCHAR(5) DEFAULT NULL, enabled TINYINT(1) DEFAULT \'0\' NOT NULL, rules LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_4FBF094F1D4E64E8 (company_name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, file VARCHAR(255) NOT NULL, type INT NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mentor_goal_card (id INT AUTO_INCREMENT NOT NULL, id_process INT NOT NULL, accepted_mentee INT NOT NULL, accepted_mentor INT NOT NULL, first_goal LONGTEXT DEFAULT NULL, first_kpi LONGTEXT DEFAULT NULL, second_goal LONGTEXT DEFAULT NULL, second_kpi LONGTEXT DEFAULT NULL, third_goal LONGTEXT DEFAULT NULL, third_kpi LONGTEXT DEFAULT NULL, INDEX IDX_CAB8BF095880B1E6 (id_process), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mentor_process (id INT AUTO_INCREMENT NOT NULL, id_mentor INT NOT NULL, id_mentee INT NOT NULL, status INT NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, sessions INT DEFAULT 0 NOT NULL, sessions_planned INT DEFAULT 0 NOT NULL, sessions_realized INT DEFAULT 0 NOT NULL, blocked_for_client INT NOT NULL, INDEX IDX_1F16E3B3A622D733 (id_mentor), INDEX IDX_1F16E3B3DF1EBA7E (id_mentee), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mentor_process_summary (id INT AUTO_INCREMENT NOT NULL, id_process INT DEFAULT NULL, summary LONGTEXT DEFAULT NULL, informations LONGTEXT DEFAULT NULL, rating_first_goal_summary LONGTEXT DEFAULT NULL, rating_first_goal_comment LONGTEXT DEFAULT NULL, rating_first_goal_rate INT DEFAULT NULL, rating_second_goal_summary LONGTEXT DEFAULT NULL, rating_second_goal_comment LONGTEXT DEFAULT NULL, rating_second_goal_rate INT DEFAULT NULL, rating_third_goal_summary LONGTEXT DEFAULT NULL, rating_third_goal_comment LONGTEXT DEFAULT NULL, rating_third_goal_rate INT DEFAULT NULL, recommendations LONGTEXT DEFAULT NULL, information_for_superior LONGTEXT DEFAULT NULL, additional_informations LONGTEXT DEFAULT NULL, INDEX IDX_E76E0B055880B1E6 (id_process), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mentor_session (id INT AUTO_INCREMENT NOT NULL, id_process INT NOT NULL, status INT DEFAULT 0 NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, confirmed_mentee INT DEFAULT 0 NOT NULL, confirmed_mentor INT DEFAULT 0 NOT NULL, created_date DATETIME NOT NULL, acceptToken VARCHAR(255) DEFAULT NULL, accept_mentee INT DEFAULT 0 NOT NULL, accept_mentor INT DEFAULT 0 NOT NULL, reject_reason VARCHAR(255) DEFAULT NULL, INDEX IDX_494F2EF15880B1E6 (id_process), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE professional (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_B3B573AA5E237E06 (name), INDEX IDX_B3B573AAA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE specialization (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_9ED9F26A5E237E06 (name), INDEX IDX_9ED9F26AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_base (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_client (id INT NOT NULL, company_id INT DEFAULT NULL, hours_limit INT DEFAULT 0, mentee_limit INT DEFAULT 0, person_function VARCHAR(40) DEFAULT NULL, INDEX IDX_A2161F68979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_mentee (id INT NOT NULL, id_parent_user INT DEFAULT NULL, id_mentee_trainer INT DEFAULT NULL, company_id INT DEFAULT NULL, INDEX IDX_9C7B14AEA8622D0A (id_parent_user), INDEX IDX_9C7B14AEBEFBF172 (id_mentee_trainer), INDEX IDX_9C7B14AE979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_mentee_watcher (id INT NOT NULL, company_id INT DEFAULT NULL, INDEX IDX_9F827DA1979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_mentor (id INT NOT NULL, mentoring_experience INT DEFAULT NULL, trener_experience INT DEFAULT NULL, about LONGTEXT DEFAULT NULL, professional_profile LONGTEXT DEFAULT NULL, education LONGTEXT DEFAULT NULL, credentials LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE audit_logs ADD CONSTRAINT FK_D62F2858953C1C61 FOREIGN KEY (source_id) REFERENCES audit_associations (id)');
        $this->addSql('ALTER TABLE audit_logs ADD CONSTRAINT FK_D62F2858158E0B66 FOREIGN KEY (target_id) REFERENCES audit_associations (id)');
        $this->addSql('ALTER TABLE audit_logs ADD CONSTRAINT FK_D62F28588C082A2E FOREIGN KEY (blame_id) REFERENCES audit_associations (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FE2904019 FOREIGN KEY (thread_id) REFERENCES thread (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FF624B39D FOREIGN KEY (sender_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE message_metadata ADD CONSTRAINT FK_4632F005537A1329 FOREIGN KEY (message_id) REFERENCES message (id)');
        $this->addSql('ALTER TABLE message_metadata ADD CONSTRAINT FK_4632F0059D1C3019 FOREIGN KEY (participant_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA6B3CA4B FOREIGN KEY (id_user) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE thread ADD CONSTRAINT FK_31204C83B03A8386 FOREIGN KEY (created_by_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE thread_metadata ADD CONSTRAINT FK_40A577C8E2904019 FOREIGN KEY (thread_id) REFERENCES thread (id)');
        $this->addSql('ALTER TABLE thread_metadata ADD CONSTRAINT FK_40A577C89D1C3019 FOREIGN KEY (participant_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE business_hours ADD CONSTRAINT FK_F4FB5A32A76ED395 FOREIGN KEY (user_id) REFERENCES user_mentor (id)');
        $this->addSql('ALTER TABLE mentor_goal_card ADD CONSTRAINT FK_CAB8BF095880B1E6 FOREIGN KEY (id_process) REFERENCES mentor_process (id)');
        $this->addSql('ALTER TABLE mentor_process ADD CONSTRAINT FK_1F16E3B3A622D733 FOREIGN KEY (id_mentor) REFERENCES user_mentor (id)');
        $this->addSql('ALTER TABLE mentor_process ADD CONSTRAINT FK_1F16E3B3DF1EBA7E FOREIGN KEY (id_mentee) REFERENCES user_mentee (id)');
        $this->addSql('ALTER TABLE mentor_process_summary ADD CONSTRAINT FK_E76E0B055880B1E6 FOREIGN KEY (id_process) REFERENCES mentor_process (id)');
        $this->addSql('ALTER TABLE mentor_session ADD CONSTRAINT FK_494F2EF15880B1E6 FOREIGN KEY (id_process) REFERENCES mentor_process (id)');
        $this->addSql('ALTER TABLE professional ADD CONSTRAINT FK_B3B573AAA76ED395 FOREIGN KEY (user_id) REFERENCES user_mentor (id)');
        $this->addSql('ALTER TABLE specialization ADD CONSTRAINT FK_9ED9F26AA76ED395 FOREIGN KEY (user_id) REFERENCES user_mentor (id)');
        $this->addSql('ALTER TABLE user_base ADD CONSTRAINT FK_BA35B2A8BF396750 FOREIGN KEY (id) REFERENCES fos_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_client ADD CONSTRAINT FK_A2161F68979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE user_client ADD CONSTRAINT FK_A2161F68BF396750 FOREIGN KEY (id) REFERENCES fos_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_mentee ADD CONSTRAINT FK_9C7B14AEA8622D0A FOREIGN KEY (id_parent_user) REFERENCES user_client (id)');
        $this->addSql('ALTER TABLE user_mentee ADD CONSTRAINT FK_9C7B14AEBEFBF172 FOREIGN KEY (id_mentee_trainer) REFERENCES user_mentee_watcher (id)');
        $this->addSql('ALTER TABLE user_mentee ADD CONSTRAINT FK_9C7B14AE979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE user_mentee ADD CONSTRAINT FK_9C7B14AEBF396750 FOREIGN KEY (id) REFERENCES fos_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_mentee_watcher ADD CONSTRAINT FK_9F827DA1979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE user_mentee_watcher ADD CONSTRAINT FK_9F827DA1BF396750 FOREIGN KEY (id) REFERENCES fos_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_mentor ADD CONSTRAINT FK_E54779E3BF396750 FOREIGN KEY (id) REFERENCES fos_user (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE audit_logs DROP FOREIGN KEY FK_D62F2858953C1C61');
        $this->addSql('ALTER TABLE audit_logs DROP FOREIGN KEY FK_D62F2858158E0B66');
        $this->addSql('ALTER TABLE audit_logs DROP FOREIGN KEY FK_D62F28588C082A2E');
        $this->addSql('ALTER TABLE message_metadata DROP FOREIGN KEY FK_4632F005537A1329');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FE2904019');
        $this->addSql('ALTER TABLE thread_metadata DROP FOREIGN KEY FK_40A577C8E2904019');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FF624B39D');
        $this->addSql('ALTER TABLE message_metadata DROP FOREIGN KEY FK_4632F0059D1C3019');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA6B3CA4B');
        $this->addSql('ALTER TABLE thread DROP FOREIGN KEY FK_31204C83B03A8386');
        $this->addSql('ALTER TABLE thread_metadata DROP FOREIGN KEY FK_40A577C89D1C3019');
        $this->addSql('ALTER TABLE user_base DROP FOREIGN KEY FK_BA35B2A8BF396750');
        $this->addSql('ALTER TABLE user_client DROP FOREIGN KEY FK_A2161F68BF396750');
        $this->addSql('ALTER TABLE user_mentee DROP FOREIGN KEY FK_9C7B14AEBF396750');
        $this->addSql('ALTER TABLE user_mentee_watcher DROP FOREIGN KEY FK_9F827DA1BF396750');
        $this->addSql('ALTER TABLE user_mentor DROP FOREIGN KEY FK_E54779E3BF396750');
        $this->addSql('ALTER TABLE user_client DROP FOREIGN KEY FK_A2161F68979B1AD6');
        $this->addSql('ALTER TABLE user_mentee DROP FOREIGN KEY FK_9C7B14AE979B1AD6');
        $this->addSql('ALTER TABLE user_mentee_watcher DROP FOREIGN KEY FK_9F827DA1979B1AD6');
        $this->addSql('ALTER TABLE mentor_goal_card DROP FOREIGN KEY FK_CAB8BF095880B1E6');
        $this->addSql('ALTER TABLE mentor_process_summary DROP FOREIGN KEY FK_E76E0B055880B1E6');
        $this->addSql('ALTER TABLE mentor_session DROP FOREIGN KEY FK_494F2EF15880B1E6');
        $this->addSql('ALTER TABLE user_mentee DROP FOREIGN KEY FK_9C7B14AEA8622D0A');
        $this->addSql('ALTER TABLE mentor_process DROP FOREIGN KEY FK_1F16E3B3DF1EBA7E');
        $this->addSql('ALTER TABLE user_mentee DROP FOREIGN KEY FK_9C7B14AEBEFBF172');
        $this->addSql('ALTER TABLE business_hours DROP FOREIGN KEY FK_F4FB5A32A76ED395');
        $this->addSql('ALTER TABLE mentor_process DROP FOREIGN KEY FK_1F16E3B3A622D733');
        $this->addSql('ALTER TABLE professional DROP FOREIGN KEY FK_B3B573AAA76ED395');
        $this->addSql('ALTER TABLE specialization DROP FOREIGN KEY FK_9ED9F26AA76ED395');
        $this->addSql('DROP TABLE audit_associations');
        $this->addSql('DROP TABLE audit_logs');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE message_metadata');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE thread');
        $this->addSql('DROP TABLE thread_metadata');
        $this->addSql('DROP TABLE fos_user');
        $this->addSql('DROP TABLE business_hours');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE file');
        $this->addSql('DROP TABLE mentor_goal_card');
        $this->addSql('DROP TABLE mentor_process');
        $this->addSql('DROP TABLE mentor_process_summary');
        $this->addSql('DROP TABLE mentor_session');
        $this->addSql('DROP TABLE professional');
        $this->addSql('DROP TABLE specialization');
        $this->addSql('DROP TABLE user_base');
        $this->addSql('DROP TABLE user_client');
        $this->addSql('DROP TABLE user_mentee');
        $this->addSql('DROP TABLE user_mentee_watcher');
        $this->addSql('DROP TABLE user_mentor');
    }
}
