<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180702191446 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE mentor_industries (user_mentor_id INT NOT NULL, industry_id INT NOT NULL, INDEX IDX_386637E7DBA0BDA0 (user_mentor_id), INDEX IDX_386637E72B19A734 (industry_id), PRIMARY KEY(user_mentor_id, industry_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mentor_industries ADD CONSTRAINT FK_386637E7DBA0BDA0 FOREIGN KEY (user_mentor_id) REFERENCES user_mentor (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mentor_industries ADD CONSTRAINT FK_386637E72B19A734 FOREIGN KEY (industry_id) REFERENCES industry (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE mentor_industries');
    }
}
