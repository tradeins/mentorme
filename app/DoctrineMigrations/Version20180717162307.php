<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180717162307 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE mentor_specializations (user_mentor_id INT NOT NULL, specialization_id INT NOT NULL, INDEX IDX_D7C9290CDBA0BDA0 (user_mentor_id), INDEX IDX_D7C9290CFA846217 (specialization_id), PRIMARY KEY(user_mentor_id, specialization_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mentor_specializations ADD CONSTRAINT FK_D7C9290CDBA0BDA0 FOREIGN KEY (user_mentor_id) REFERENCES user_mentor (id)');
        $this->addSql('ALTER TABLE mentor_specializations ADD CONSTRAINT FK_D7C9290CFA846217 FOREIGN KEY (specialization_id) REFERENCES specialization_area (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE mentor_specializations');
    }
}
