# Mentorme


## 1. Postawienie projeku

1. klonujemy projekt do interesującego nas katalogu
2. uruchamiamy composer install
3. zmieniamy dane dostępowe dla bazy danych w pliku
 ```php
 app/config/parameters.yml (jeśli pliku nie ma, kopiujemy app/config/parameters.yml.dist)
 ```
 
4. uruchamiamy migracje bazy danych (Uwaga! Baza powstaje cała z migracji!)
```bash
php bin/console doctrine:migration:migrate
```

5. dodajemy użytkownika admina
```bash
php bin/console fos:user:create
```

6. nadajemy użytkownikowi role admina
```bash
php bin/console fos:user:promote
```
podając jako role ROLE_ADMIN

i uruchamiamy projekt :)

## 2. Najważniejsze paczki w projekcie

### 2.1 Użytkownicy:
#### 2.1.1 FosUserBundle
#### 2.1.2. PUGX\MultiUserBundle\PUGXMultiUserBundle
### 2.2 Wygląd:
Avanzu\AdminThemeBundle\AvanzuAdminThemeBundle https://github.com/avanzu/AdminThemeBundle

demo i możliwości: https://adminlte.io

### 2.2 Pozostałe:
#### 2.2.1 Wiadomości: FOS\MessageBundle\FOSMessageBundle
#### 2.2.2 Paginacja: Knp\Bundle\PaginatorBundle\KnpPaginatorBundle,
#### 2.2.3 Filtry: Lexik\Bundle\FormFilterBundle\LexikFormFilterBundle,
#### 2.2.4 Zapis zmian encji: DataDog\AuditBundle\DataDogAuditBundle,
#### 2.2.5 Generowanie PDF WhiteOctober\TCPDFBundle\WhiteOctoberTCPDFBundle,
#### 2.2.6 Manipulacja obrazami: Liip\ImagineBundle\LiipImagineBundle,
#### 2.2.7 Migracje: Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle,

## 3. Co poprawić/zrobić lepiej?
### 3.1 przenieść ACL na Voters: https://symfony.com/doc/current/security/voters.html
### 3.2 Usunąć duplikacje paru fragmentów kodu
### 3.3 Uspójnić wywołanie tłumaczeń

## 4. Crontab
Należy ustawić 4 akcje chodzące w tle:
```bash
*/5 * * * * php ../mentorme/bin/console mentor:process:update-status-to-realization >/dev/null 2>&1
*/1 * * * * php ../mentorme/bin/console mentor:session:update-status-realized >/dev/null 2>&1
* 1 * * * php ../mentorme/bin/console mentor:session:check-realized-without-confirmation >/dev/null 2>&1
*/1 * * * * php ../mentorme/bin/console mentor:session:update-status-to-realization >/dev/null 2>&1
```

